/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const packageJSON = require("./package.json");

module.exports = {
  sections: [
    {
      name: "Forms",
      components: "src/forms/*/index.tsx",
    },
    {
      name: "Inputs",
      components: "src/inputs/*/index.tsx",
    },
    {
      name: "Hooks",
      content: "src/hooks/Readme.md",
      components: "src/hooks/(?!index).*.tsx?",
    },
  ],
  propsParser: require("react-docgen-typescript").withDefaultConfig().parse,
  getComponentPathLine(componentPath) {
    const [name] = path.dirname(componentPath).split("/").reverse();
    return `import { ${name} } from '${packageJSON.name}';`;
  },
  moduleAliases: {
    [packageJSON.name]: path.resolve(__dirname, "src"),
  },
  webpackConfig: {
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          loader: "ts-loader",
        },
        {
          test: /\.s[ac]ss$/i,
          exclude: /node_modules/,
          use: ["style-loader", "css-loader", "sass-loader"],
        },
        {
          test: /\.svg$/,
          exclude: /node_modules/,
          loader: "svg-inline-loader",
        },
      ],
    },
  },
};
