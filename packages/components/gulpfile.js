/* eslint-disable @typescript-eslint/no-var-requires */
const gulp = require("gulp");
const svgstore = require("gulp-svgstore");
const svgmin = require("gulp-svgmin");
const cheerio = require("gulp-cheerio");
const filelist = require("gulp-filelist");
const htmlParser = require("node-html-parser");
const cx = require("classnames");
const path = require("path");
const fs = require("fs");

const getFileName = (file) => path.basename(file, path.extname(file));
const getIconKey = (file) => file.replace(/[^\d\w]+/g, "_").toUpperCase();

const svgMinFn = svgmin((file) => ({
  plugins: [
    { cleanupIDs: { prefix: getFileName(file.relative) + "-", minify: true } },
  ],
}));

const generateIconViewsFn = filelist("views.ts", {
  destRowTemplate: (file) => {
    const fileName = getFileName(file);
    const name = getIconKey(fileName);
    return `\rexport const ${name} = '${fileName}'\r\n`;
  },
});

const generateIconDefaultStylesFn = filelist("views.module.scss", {
  destRowTemplate: (file) => {
    const fileName = getFileName(file);
    const fileContent = fs.readFileSync(file).toString();
    const html = htmlParser.parse(fileContent);
    const svg = html.querySelector("svg");
    const width = svg?.getAttribute("width");
    const height = svg?.getAttribute("height");
    const fill = html.querySelector("[fill]")?.getAttribute("fill");
    const stroke = html.querySelector("[stroke]")?.getAttribute("stroke");
    return `.${fileName} { ${cx({
      [`width: ${width}px;`]: width,
      [`height: ${height}px;`]: height,
      [`fill: ${fill};`]: fill,
      [`stroke: ${stroke};`]: stroke,
    })} }\r\n`;
  },
});

const cleanAttributesFn = cheerio({
  run: ($) => {
    const stroke = $("[stroke]");
    stroke.length === 1 && stroke.removeAttr("stroke");

    const fill = $("[fill]");
    fill.length === 1 && fill.removeAttr("fill");

    $("[width]").removeAttr("width");
    $("[height]").removeAttr("height");
  },
  parserOptions: { xmlMode: true },
});

gulp.task("svgstore:svgs", () => {
  return gulp
    .src("icons/*.svg")
    .pipe(svgMinFn)
    .pipe(cleanAttributesFn)
    .pipe(
      svgstore({
        inlineSvg: true,
      })
    )
    .pipe(gulp.dest("src/forms/Icon"));
});

gulp.task("svgstore:views", () => {
  return gulp
    .src("icons/*.svg")
    .pipe(generateIconViewsFn)
    .pipe(gulp.dest("src/forms/Icon"));
});

gulp.task("svgstore:styles", () => {
  return gulp
    .src("icons/*.svg")
    .pipe(generateIconDefaultStylesFn)
    .pipe(gulp.dest("src/forms/Icon"));
});

gulp.task(
  "svgstore",
  gulp.series("svgstore:svgs", "svgstore:styles", "svgstore:views")
);
