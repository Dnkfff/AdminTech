```tsx
<Radio
  name="radio-name"
  value="radio1"
  valueKey="radio1"
  disabled={false}
  onChange={(e) => setState(e.target.value)}
/>
```

```tsx
<Radio
  name="radio-name"
  value="radio1"
  valueKey="radio1"
  disabled={false}
  onChange={(e) => setState(e.target.value)}
/>
```

```tsx
<Radio
  name="radio-name"
  value="radio1"
  valueKey="radio1"
  disabled={true}
  onChange={(e) => setState(e.target.value)}
/>
```
