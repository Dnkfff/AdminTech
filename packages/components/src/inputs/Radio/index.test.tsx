import "@testing-library/jest-dom";
import * as React from "react";
import { fireEvent, render } from "@testing-library/react";
import Radio from "./index";

const values = {
  valid: "valid",
  invalid: "invalid",
};

it("is rendered in document body", () => {
  const { getByTestId } = render(
    <Radio value={values.valid} valueKey={values.invalid} />
  );
  expect(getByTestId("container")).toBeInTheDocument();
});

it("is checked", () => {
  const { getByTestId } = render(
    <Radio value={values.valid} valueKey={values.valid} />
  );

  const node = getByTestId("radio");
  expect(node).toBeChecked();
});

it("is html contains required params", () => {
  const name = "name";
  const disabled = true;
  const { getByTestId } = render(
    <Radio
      value={values.valid}
      valueKey={values.valid}
      name={name}
      disabled={disabled}
    />
  );

  const node = getByTestId("radio");
  expect(node).toBeDisabled();
  expect(node).toHaveAttribute("name", name);
  expect(node).toHaveAttribute("value", values.valid);
});

it("is checked one from group", () => {
  const { getAllByTestId } = render(
    <>
      <Radio valueKey={values.valid} value={values.valid} />
      <Radio valueKey={values.invalid} value={values.valid} />
    </>
  );

  const [node1, node2] = getAllByTestId("radio");

  expect(node1).toBeChecked();
  expect(node2).not.toBeChecked();
});

it("is checked all from group", () => {
  const { getAllByTestId } = render(
    <>
      <Radio valueKey={values.valid} value={values.valid} />
      <Radio valueKey={values.invalid} value={values.invalid} />
    </>
  );

  const [node1, node2] = getAllByTestId("radio");

  expect(node1).toBeChecked();
  expect(node2).toBeChecked();
});

it("is checked on click", () => {
  const name = "component_key";
  const fn = jest.fn(({ target: { name, value, checked } }) => ({
    name,
    value,
    checked,
  }));

  const { getByTestId } = render(
    <Radio
      valueKey={values.valid}
      value={values.invalid}
      onChange={fn}
      name={name}
    />
  );

  const node = getByTestId("radio") as HTMLInputElement;
  fireEvent.click(node);

  const { value, name: _name, checked } = fn.mock.results[0].value;
  expect(fn).toBeCalledTimes(1);
  expect(value).toBe(values.valid);
  expect(checked).toBeTruthy();
  expect(_name).toEqual(name);
});

it("is cannot changed on click when disabled", () => {
  const fn = jest.fn();
  const { getByTestId } = render(
    <Radio
      valueKey={values.valid}
      value={values.invalid}
      onChange={fn}
      disabled
    />
  );

  const node = getByTestId("container") as HTMLInputElement;
  fireEvent.click(node);

  expect(fn).toBeCalledTimes(0);
});
