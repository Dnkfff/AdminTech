import "@testing-library/jest-dom";
import * as React from "react";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { always } from "../../utils";
import Datepicker from "./index";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import moment from "moment";

const intersectionObserverMock = () => ({
  observe: () => null,
  disconnect: () => null,
});
window.IntersectionObserver = jest
  .fn()
  .mockImplementation(intersectionObserverMock);

describe("Datepicker", () => {
  it("is rendered in document body", () => {
    const { getByTestId } = render(
      <Datepicker value="" onChange={always.EMPTY_FUNC} />
    );
    expect(getByTestId("input-container")).toBeInTheDocument();
  });

  it("is value display", () => {
    const value = moment();
    const dateFormat = "DD.MM.YYYY";
    const { getByTestId } = render(
      <Datepicker
        value={value.toISOString()}
        onChange={always.EMPTY_FUNC}
        dateFormat={dateFormat}
      />
    );
    expect(getByTestId("input")).toHaveValue(value.format(dateFormat));
  });

  it("is apply required params", () => {
    const value = moment();
    const disabled = true;
    const placeholder = "Placeholder";
    const className = "cx";
    const name = "name";

    const { getByTestId } = render(
      <Datepicker
        placeholder={placeholder}
        className={className}
        value={value.toISOString()}
        name={name}
        disabled={disabled}
      />
    );

    expect(getByTestId("input-container")).toHaveClass(className);
    expect(getByTestId("input")).toHaveAttribute("placeholder", placeholder);
    expect(getByTestId("input")).toHaveAttribute("name", name);
    expect(getByTestId("input")).toBeDisabled();
  });

  it("is opened picker on click field", async () => {
    const value = moment();
    const fn = jest.fn();

    const { getByTestId } = render(
      <Datepicker value={value.toISOString()} onChange={fn} />
    );

    await userEvent.click(getByTestId("input"));

    expect(getByTestId("picker-container")).toBeInTheDocument();
  });

  it("is opened picker on click field", async () => {
    const value = moment();
    const fn = jest.fn();

    const { getByTestId } = render(
      <Datepicker value={value.toISOString()} onChange={fn} />
    );

    await userEvent.click(getByTestId("input"));

    expect(getByTestId("picker-container")).toBeInTheDocument();
  });

  it("is correct month and year displayed", async () => {
    const value = moment();
    const fn = jest.fn();

    const { getByTestId } = render(
      <Datepicker value={value.toISOString()} onChange={fn} />
    );

    await userEvent.click(getByTestId("input"));

    expect(getByTestId("month")).toHaveTextContent(
      moment.months()[value.month()]
    );
    expect(getByTestId("year")).toHaveTextContent(value.year().toString());
  });

  it("is all days displayed in picker", async () => {
    const value = moment();
    const fn = jest.fn();

    const { getByTestId } = render(
      <Datepicker value={value.toISOString()} onChange={fn} />
    );

    await userEvent.click(getByTestId("input"));

    const pickerContainer = getByTestId("picker-container");
    const days = pickerContainer.querySelectorAll("[data-testid=day]");
    expect(days.length).toBe(value.daysInMonth());
  });

  it("is validate value on input", async () => {
    const fn = jest.fn(({ target: { name, value } }) => ({ value, name }));
    const name = "name";
    const format = "DD.MM.YYYY";

    const { getByTestId } = render(
      <Datepicker value="" onChange={fn} name={name} dateFormat={format} />
    );

    const inputValid = "23.02.2022";
    await userEvent.type(getByTestId("input"), inputValid);

    expect(fn.mock.lastCall[0].target.name).toBe(name);
    expect(fn.mock.lastCall[0].target.value).toBe(
      moment.utc(inputValid, format).toISOString()
    );

    await userEvent.clear(getByTestId("input"));

    expect(fn.mock.lastCall[0].target.value).toBe(null);
  });

  it("is change picker period on year and month change", async () => {
    const fn = jest.fn(({ target: { name, value } }) => ({ value, name }));
    const name = "name";
    const format = "DD.MM.YYYY";
    const value = moment();

    const { getByTestId } = render(
      <Datepicker
        value={value.toISOString()}
        onChange={fn}
        name={name}
        dateFormat={format}
      />
    );

    await userEvent.click(getByTestId("input"));

    const monthBack = getByTestId("month").querySelector(
      "button:first-child"
    ) as Element;
    await userEvent.click(monthBack);
    await userEvent.click(monthBack);
    await userEvent.click(
      getByTestId("days").querySelector("[data-testid=day]") as Element
    );

    expect(fn.mock.lastCall[0].target.value).toBe(
      moment.utc(value.add(-2, "month").format(format), format).toISOString()
    );

    const monthForward = getByTestId("month").querySelector(
      "button:last-child"
    ) as Element;
    await userEvent.click(monthForward);
    await userEvent.click(
      getByTestId("days").querySelector("[data-testid=day]") as Element
    );

    expect(fn.mock.lastCall[0].target.value).toBe(
      moment.utc(value.add(+1, "month").format(format), format).toISOString()
    );

    const yearBack = getByTestId("year").querySelector(
      "button:first-child"
    ) as Element;
    await userEvent.click(yearBack);
    await userEvent.click(yearBack);
    await userEvent.click(
      getByTestId("days").querySelector("[data-testid=day]") as Element
    );

    expect(fn.mock.lastCall[0].target.value).toBe(
      moment.utc(value.add(-2, "year").format(format), format).toISOString()
    );

    const yearForward = getByTestId("year").querySelector(
      "button:last-child"
    ) as Element;
    await userEvent.click(yearForward);
    await userEvent.click(yearForward);
    await userEvent.click(
      getByTestId("days").querySelector("[data-testid=day]") as Element
    );

    expect(fn.mock.lastCall[0].target.value).toBe(
      moment.utc(value.add(+2, "year").format(format), format).toISOString()
    );
  });
});
