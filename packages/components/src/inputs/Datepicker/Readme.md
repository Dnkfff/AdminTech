##### Datepicker

```tsx
import React, {useState} from "react";

const [date, setDate] = useState("");

<>
    {date}
    <Datepicker
        placeholder="Start date"
        title="Start date"
        value={date}
        dateFormat="DD.MM.YYYY"
        onChange={({target: {value}}) => {
            setDate(value);
        }}
    />
</>
```
