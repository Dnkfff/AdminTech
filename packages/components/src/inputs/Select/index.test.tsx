import "@testing-library/jest-dom";
import * as React from "react";
import { render, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { always } from "../../utils";
import Select from "./index";

const intersectionObserverMock = () => ({
  observe: () => null,
  disconnect: () => null,
});
window.IntersectionObserver = jest
  .fn()
  .mockImplementation(intersectionObserverMock);

const options = Array.from({ length: 30 }, (_, index) => ({
  id: String(index),
  label: Math.random().toString(36).slice(2, 7),
}));

describe("Select", () => {
  it("is rendered in document body", () => {
    const { getByTestId } = render(
      <Select value="" onChange={always.EMPTY_FUNC} options={options} />
    );
    expect(getByTestId("select")).toBeInTheDocument();
  });

  it("is value selected if defined", () => {
    const index = "3";
    const { getByTestId } = render(
      <Select value={index} onChange={always.EMPTY_FUNC} options={options} />
    );
    expect(getByTestId("input")).toHaveValue(options[index].label);
  });

  it("is opened options list on click", async () => {
    const { getByTestId } = render(
      <Select value="" onChange={always.EMPTY_FUNC} options={options} />
    );

    fireEvent.click(getByTestId("button.icon"));

    expect(getByTestId("options-container")).toBeInTheDocument();
    expect(document.body).toContainElement(getByTestId("options-container"));
  });

  it("is filter work, filterSelector accessible", async () => {
    const input = "w";
    const filteredOptions = options.filter(({ label }) =>
      label.includes(input)
    );

    const filterSelector = jest
      .fn()
      .mockImplementation((option, query) => option?.label.includes(query));

    const { getByTestId } = render(
      <Select
        value=""
        onChange={always.EMPTY_FUNC}
        options={options}
        filterSelector={filterSelector}
      />
    );

    await userEvent.type(getByTestId("input"), input);

    expect(getByTestId("input")).toHaveValue(input);
    expect(getByTestId("options-container").childNodes.length).toBe(
      filteredOptions.length
    );
    expect(filterSelector).toBeCalled();
  });

  it("is valueSelector, hiddenSelector, idSelector work", async () => {
    const index = "3";
    const valueSelector = (option: any) => option?.id + "-" + option?.label;
    const idSelector = (option: any) => option?.id + "-" + option?.label;

    const showOptions = 10;
    const hiddenSelector = (option: any) => option?.id < showOptions;

    const { getByTestId } = render(
      <Select
        value={index}
        onChange={always.EMPTY_FUNC}
        options={options}
        valueSelector={valueSelector}
        idSelector={idSelector}
        hiddenSelector={hiddenSelector}
      />
    );

    await userEvent.click(getByTestId("input"));

    const optionsNode = getByTestId("options-container");
    // idSelector check
    expect(optionsNode.childNodes.item(0)).toHaveValue(idSelector(options[0]));

    // valueSelector check
    expect(optionsNode).toHaveTextContent(valueSelector(options[0]));

    // hiddenSelector check
    expect(getByTestId("options-container").childNodes.length).toBe(
      showOptions
    );
  });

  it("is changed on value select", async () => {
    const index = "3";
    const name = "any_name";
    const fnChange = jest.fn(({ target: { name, value } }) => ({
      name,
      value,
    }));

    const { getByTestId } = render(
      <Select name={name} value={index} onChange={fnChange} options={options} />
    );

    await userEvent.click(getByTestId("input"));
    const optionsNode = getByTestId("options-container");
    const optionToClick = optionsNode.childNodes.item(0) as Element;
    await userEvent.click(optionToClick);

    expect(fnChange).toHaveBeenCalledTimes(1);
    expect(fnChange.mock.lastCall[0].target.value).toBe(
      optionToClick.getAttribute("value")
    );
    expect(fnChange.mock.lastCall[0].target.name).toBe(name);
  });

  it("is locked if disabled", async () => {
    const index = "3";
    const fnChange = jest.fn();

    const { getByTestId, queryByTestId } = render(
      <Select value={index} onChange={fnChange} options={options} disabled />
    );

    await userEvent.click(getByTestId("input"));
    const optionsNode = queryByTestId("options-container");

    expect(optionsNode).not.toBeInTheDocument();
    expect(getByTestId("input")).toBeDisabled();

    await userEvent.type(getByTestId("input"), "test");
    expect(fnChange).toHaveBeenCalledTimes(0);
  });

  it("is set required attributes", async () => {
    const name = "name";
    const value = "5";
    const placeholder = "placeholder";
    const fnChange = jest.fn();

    const inputText = "text";
    const fnInputChange = jest.fn();

    const { getByTestId } = render(
      <Select
        name={name}
        value={value}
        options={options}
        placeholder={placeholder}
        onChange={fnChange}
        onInputChange={fnInputChange}
      />
    );

    expect(getByTestId("input")).toHaveAttribute("name", name);
    expect(getByTestId("input")).toHaveAttribute("placeholder", placeholder);

    await userEvent.type(getByTestId("input"), inputText);
    expect(fnInputChange).toHaveBeenCalledTimes(inputText.length);
  });
});

describe("Select.Multi", () => {
  it("is value selected if defined", () => {
    const index = ["3", "4", "5"];
    const { getAllByTestId, getByTestId } = render(
      <Select.Multi
        value={index}
        onChange={always.EMPTY_FUNC}
        options={options}
      />
    );
    expect(getAllByTestId("select-tag").length).toBe(3);
    expect(getByTestId("input-holder")).toHaveTextContent(
      options[Number(index[0]) as number].label
    );
  });

  it("is changed on change", async () => {
    const index = ["3", "4", "5"];
    const onChange = jest.fn(({ target: { value, name } }) => ({
      value,
      name,
    }));

    const { getByTestId } = render(
      <Select.Multi value={index} onChange={onChange} options={options} />
    );

    await userEvent.click(getByTestId("input"));
    await userEvent.click(
      getByTestId("options-container").childNodes.item(
        Number(index[0])
      ) as Element
    );

    expect(onChange.mock.lastCall[0].target.value).toMatchObject(["4", "5"]);

    const new_item = "7";
    await userEvent.click(getByTestId("input"));
    await userEvent.click(
      getByTestId("options-container").childNodes.item(
        Number(new_item)
      ) as Element
    );

    expect(onChange.mock.lastCall[0].target.value).toMatchObject([
      ...index,
      new_item,
    ]);
  });

  it("is not changed on change if disabled", async () => {
    const index = ["3", "4", "5"];
    const onInputChange = jest.fn();

    const { getByTestId, queryByTestId } = render(
      <Select.Multi
        value={index}
        onChange={onInputChange}
        options={options}
        onInputChange={onInputChange}
        disabled
      />
    );

    await userEvent.click(getByTestId("input"));
    expect(queryByTestId("options-container")).not.toBeInTheDocument();

    await userEvent.type(getByTestId("input"), "text");
    expect(onInputChange).not.toBeCalled();
  });
});
