#### Select

```tsx
import React, { useCallback, useState, useMemo } from "react";

const data = useMemo(
  () => [
    { id: 0, label: "English" },
    { id: 1, label: "French" },
    { id: 2, label: "Ukrainian" },
    { id: 3, label: "Italian" },
    { id: 4, label: "Spain" },
    { id: 5, label: "German" },
    { id: 6, label: "Polish" },
    { id: 7, label: "Portugal" },
  ],
  []
);

const [value, setValue] = useState(null);

const onChange = useCallback(
  ({ target: { value } }) => setValue(value),
  [setValue]
);

<Select
  options={data}
  placeholder="Select..."
  value={value}
  onChange={onChange}
/>;
```

#### SelectMulti

```tsx
import React, { useCallback, useState, useMemo } from "react";
import { Icon } from "../../forms";

const data = useMemo(
  () => [
    { id: 0, label: "English" },
    { id: 1, label: "French" },
    { id: 2, label: "German" },
    { id: 3, label: "Italian" },
    { id: 4, label: "Spain" },
    { id: 5, label: "Ukrainian" },
    { id: 6, label: "Italian" },
  ],
  []
);

const [value, setValue] = useState([]);

const onChange = useCallback(
  (item) => setValue(item.target.value),
  [value, setValue]
);

<Select.Multi
  options={data}
  placeholder="Select..."
  value={value}
  onChange={onChange}
  tagCount={3}
/>;
```
