import "@testing-library/jest-dom";
import * as React from "react";
import { fireEvent, render } from "@testing-library/react";
import { always } from "../../utils";
import Switch from "./index";

it("is rendered in document body", () => {
  const { getByTestId } = render(<Switch value onChange={always.EMPTY_FUNC} />);
  expect(getByTestId("container")).toBeInTheDocument();
});

it("is checked", () => {
  const value = true;
  const { getByTestId } = render(
    <Switch value={value} onChange={always.EMPTY_FUNC} />
  );

  const node = getByTestId("switch");
  expect(node).toBeChecked();
});

it("is html contains required params", () => {
  const name = "name";
  const disabled = true;
  const value = true;
  const { getByTestId } = render(
    <Switch
      value={value}
      name={name}
      onChange={always.EMPTY_FUNC}
      disabled={disabled}
    />
  );

  const node = getByTestId("switch");
  expect(node).toBeDisabled();
  expect(node).toBeChecked();
  expect(node).toHaveAttribute("name", name);
});

it("is checked on click", () => {
  const name = "component_key";
  const fn = jest.fn(({ target: { name, value, checked } }) => ({
    name,
    value,
    checked,
  }));
  const { getByTestId } = render(<Switch value onChange={fn} name={name} />);

  const node = getByTestId("switch") as HTMLInputElement;
  fireEvent.click(node);

  const { value: _value, name: _name, checked } = fn.mock.results[0].value;
  expect(fn).toBeCalledTimes(1);
  expect(_value).toBeFalsy();
  expect(checked).toBeFalsy();
  expect(_name).toEqual(name);
});

it("is cannot changed on click when disabled", () => {
  const fn = jest.fn();
  const { getByTestId } = render(<Switch value onChange={fn} disabled />);

  const node = getByTestId("container") as HTMLInputElement;
  fireEvent.click(node);

  expect(fn).toBeCalledTimes(0);
});
