```tsx
const [state, setState] = React.useState(false)
;<Switch value={state} onChange={ e => (setState(e.target.checked))}  />
```
```tsx
const [state, setState] = React.useState(false)
;<Switch value={state} onChange={ e => (setState(e.target.checked))} disabled={true}  />
```
