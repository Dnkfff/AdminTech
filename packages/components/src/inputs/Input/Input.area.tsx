import React, { FC, RefObject } from "react";
import { IInputProps } from "./Input";

import styles from "./input.area.module.scss";

export type ITextareaProps = Omit<IInputProps, "type" | "ref"> & {
  ref?: RefObject<HTMLTextAreaElement>;
  maxLength?: HTMLTextAreaElement["maxLength"];
  minLength?: HTMLTextAreaElement["minLength"];
};

const InputTextarea: FC<ITextareaProps> = ({
  maxLength,
  minLength,
  ...props
}: ITextareaProps) => {
  return (
    <textarea
      data-testid="input-area"
      className={styles.inputArea}
      maxLength={maxLength}
      minLength={minLength}
      {...props}
    />
  );
};

InputTextarea.displayName = "Input.Area";

export default InputTextarea;
