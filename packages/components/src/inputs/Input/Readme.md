```tsx
import React, { useState } from "react";

const [text, setText] = useState("");
<Input
  value={text}
  label="hello"
  type="text"
  placeholder="Enter text here..."
  onChange={(e) => setText(e.target.value)}
/>;
```

##### Input.Number

###### Added some special props:

- min - minimum value
- max - maximum value
- precision - integers after dot ( decimals )

```tsx
import React, { useState } from "react";

const [number, setNumber] = useState("");
<Input.Number
  value={number}
  placeholder="Enter number here..."
  onChange={({ target: { value } }) => setNumber(value)}
  min={-100}
  max={100}
  precision={4}
/>;
```

##### Input.Area

```tsx
import React, { useState } from "react";

const [number, setNumber] = useState("");
<Input.Area
  value={number}
  placeholder="Enter text here..."
  onChange={(e) => setNumber(e.target.value)}
/>;
```
