import "@testing-library/jest-dom";
import * as React from "react";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { always } from "../../utils";
import Input from "./index";
import { act } from "react-dom/test-utils";

describe("Input", () => {
  it("is rendered in document body", () => {
    const { getByTestId } = render(
      <Input value="" onChange={always.EMPTY_FUNC} />
    );
    expect(getByTestId("input")).toBeInTheDocument();
  });

  it("is required properties assign", () => {
    const className = "class_name";
    const value = "value";
    const disabled = true;
    const placeholder = "placeholder";
    const type = "number";

    const { getByTestId } = render(
      <Input
        className={className}
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        type={type}
        onChange={always.EMPTY_FUNC}
      />
    );

    const node = getByTestId("input");
    expect(node).toHaveAttribute("value", value);
    expect(node).toHaveAttribute("disabled");
    expect(node).toHaveAttribute("placeholder", placeholder);
    expect(node.classList.contains(className)).toBeTruthy();
    expect(node).toHaveAttribute("type", type);
  });

  it("is call fn change on input", () => {
    const name = "name";
    const value = "";
    const input = "1";
    const fnChange = jest.fn(({ target: { name, value } }) => ({
      name,
      value,
    }));

    const { getByTestId } = render(
      <Input name={name} value={value} onChange={fnChange} />
    );

    userEvent.type(getByTestId("input"), input);

    expect(fnChange).toBeCalledTimes(input.length);
    expect(fnChange.mock.results[0].value).toMatchObject({
      value: input,
      name,
    });
  });

  it("is not call fn change on input if disabled", () => {
    const name = "name";
    const value = "";
    const input = "10";
    const fnChange = jest.fn();

    const { getByTestId } = render(
      <Input name={name} value={value} onChange={fnChange} disabled />
    );

    userEvent.type(getByTestId("input"), input);
    expect(fnChange).toBeCalledTimes(0);
  });
});

describe("Input.Number", () => {
  it("is required properties assign", () => {
    const className = "class_name";
    const value = "value";
    const disabled = true;
    const placeholder = "placeholder";

    const { getByTestId } = render(
      <Input.Number
        className={className}
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        onChange={always.EMPTY_FUNC}
      />
    );

    const node = getByTestId("input");
    expect(node).toHaveAttribute("value", value);
    expect(node).toHaveAttribute("disabled");
    expect(node).toHaveAttribute("placeholder", placeholder);
    expect(node.classList.contains(className)).toBeTruthy();
  });

  it("is value number type onChange event", () => {
    const name = "name";
    const value = "";
    const input = "145.34";
    let inputValue;
    const fnChange = jest.fn(({ target: { name, value } }) => {
      inputValue = value;
      return {
        name,
        value,
      };
    });

    const { getByTestId } = render(
      <Input.Number name={name} value={value} onChange={fnChange} />
    );

    act(() => {
      userEvent.type(getByTestId("input"), input);
    });

    expect(fnChange).toBeCalledTimes(input.length);
    expect(fnChange.mock.results[0].value).toMatchObject({
      value: Number(input.substring(0, 1)),
      name,
    });
    expect(typeof inputValue).toBe("number");
    expect(inputValue === Number(input)).toBeTruthy();
  });

  it("is value null if number invalid | empty onChange", () => {
    const name = "name";
    const value = "";
    const input = "145.34";
    const fnChange = jest.fn(({ target: { name, value } }) => ({
      name,
      value,
    }));

    const { getByTestId } = render(
      <Input.Number name={name} value={value} onChange={fnChange} />
    );

    act(() => {
      userEvent.type(getByTestId("input"), input);
      userEvent.clear(getByTestId("input"));
    });

    expect(fnChange.mock.lastCall[0].target).toMatchObject({
      name,
      value: null,
    });
  });

  it("is set min and max when value more or less", () => {
    const name = "name";
    const value = "";
    const max = 5.45;
    const min = 0;
    const inputMax = "10";
    const inputMin = "-111";
    const fnChange = jest.fn(({ target: { name, value } }) => ({
      name,
      value,
    }));

    const { getByTestId } = render(
      <Input.Number
        name={name}
        value={value}
        max={max}
        min={min}
        onChange={fnChange}
      />
    );

    act(() => {
      userEvent.type(getByTestId("input"), inputMax);
    });

    expect(fnChange.mock.lastCall[0].target.value).toBe(Number(max));

    act(() => {
      userEvent.clear(getByTestId("input"));
      userEvent.type(getByTestId("input"), inputMin);
    });

    expect(fnChange.mock.lastCall[0].target.value).toBe(Number(min));
  });

  it("is not called change if disabled", () => {
    const name = "name";
    const value = "";
    const fnChange = jest.fn();

    const { getByTestId } = render(
      <Input.Number name={name} value={value} onChange={fnChange} disabled />
    );

    act(() => {
      userEvent.type(getByTestId("input"), "12345678");
    });

    expect(fnChange).toBeCalledTimes(0);
  });
});

describe("Input.Area", () => {
  it("is rendered in document body", () => {
    const { getByTestId } = render(
      <Input.Area value="" onChange={always.EMPTY_FUNC} />
    );
    expect(getByTestId("input-area")).toBeInTheDocument();
  });

  it("is required properties assign", () => {
    const className = "class_name";
    const value = "value";
    const disabled = true;
    const placeholder = "placeholder";
    const maxLength = 100;
    const minLength = 0;

    const { getByTestId } = render(
      <Input.Area
        className={className}
        value={value}
        disabled={disabled}
        placeholder={placeholder}
        maxLength={maxLength}
        minLength={minLength}
        onChange={always.EMPTY_FUNC}
      />
    );

    const node = getByTestId("input-area");
    expect(node).toHaveTextContent(value);
    expect(node).toHaveAttribute("disabled");
    expect(node).toHaveAttribute("placeholder", placeholder);
    expect(node.classList.contains(className)).toBeTruthy();
    expect(node).toHaveAttribute("maxLength", String(maxLength));
    expect(node).toHaveAttribute("minLength", String(minLength));
  });

  it("is call fn change on input", () => {
    const name = "name";
    const value = "";
    const input = "12345";
    const fnChange = jest.fn(({ target: { name, value } }) => ({
      name,
      value,
    }));

    const { getByTestId } = render(
      <Input.Area name={name} value={value} onChange={fnChange} />
    );

    act(() => {
      userEvent.type(getByTestId("input-area"), input);
    });

    expect(fnChange).toBeCalledTimes(input.length);
    expect(fnChange.mock.results[0].value).toMatchObject({
      value: input.substring(0, 1),
      name,
    });
  });

  it("is not call fn change on input if disabled", () => {
    const name = "name";
    const value = "";
    const input = "10";
    const fnChange = jest.fn();

    const { getByTestId } = render(
      <Input name={name} value={value} onChange={fnChange} disabled />
    );

    userEvent.type(getByTestId("input"), input);
    expect(fnChange).toBeCalledTimes(0);
  });
});
