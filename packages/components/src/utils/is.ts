export const isNumber = (n: unknown) => typeof n === "number";
export const isNull = (n: unknown) => n === null;
