export const EMPTY_FUNC = Object.freeze(() => null);
export const EMPTY_ARRAY = Object.freeze([]);
export const EMPTY_OBJECT = Object.freeze({});
