#### Popup content

```tsx
import React, {useCallback, useState} from "react";
import {Button, Icon} from "@package/components";

const [text, setText] = useState('');
const res = useCallback((e) => {
    console.log('res', e.target)
}, []);
<Popup
    content={
        <Button
            onClick={({target: {name, value}}) => console.log(name, value)}
            value="value"
            name="key"
            color={Button.colors.Coral}
            border={Button.borders.Coral}
            type={Button.types.Rounded}
        >
            Admin Tech
        </Button>
    }
    placement="left"
    triangleSize={13}
>
    <p style={{maxWidth: "150px", margin: '0'}}>
        Lorem
        ipsum
        dolor
        sit
        amet, consectetur
        adipisicing
        elit.Atque
        blanditiis, cum
        doloremque
        dolores
        ea
        incidunt
        laborum, laudantium
        magnam
        maxime
        nam, necessitatibus
        nesciunt
        placeat
        porro
        quia
        quod
        reiciendis
        repudiandae
        tempora
        vel!
    </p>
</Popup>
```
