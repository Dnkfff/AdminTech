import "@testing-library/jest-dom";
import * as React from "react";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Popup from "./index";

const intersectionObserverMock = () => ({
  observe: () => null,
  disconnect: () => null,
});
window.IntersectionObserver = jest
  .fn()
  .mockImplementation(intersectionObserverMock);

const content = (
  <div data-testid="content" style={{ width: "50px", height: "50px" }}>
    content
  </div>
);
const children = (
  <div data-testid="children" style={{ width: "50px", height: "50px" }}>
    children
  </div>
);

describe("Popup", () => {
  it("is rendered in document body", () => {
    const { getByTestId } = render(<Popup content={content}>{children}</Popup>);
    expect(getByTestId("content")).toBeInTheDocument();
  });

  it("is displayed on hover", async () => {
    const { getByTestId } = render(<Popup content={content}>{children}</Popup>);
    await userEvent.hover(getByTestId("content"));
    expect(getByTestId("children")).toBeInTheDocument();
  });

  it("is displayed on hover and hide on unhover", async () => {
    const { getByTestId } = render(<Popup content={content}>{children}</Popup>);
    expect(getByTestId("popup-container").style.visibility).toBe("hidden");
    await userEvent.hover(getByTestId("content"));
    expect(getByTestId("popup-container").style.visibility).toBe("visible");
  });
});
