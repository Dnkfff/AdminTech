import "@testing-library/jest-dom";
import * as React from "react";
import { fireEvent, render } from "@testing-library/react";
import Tabs from "./index";

const items = [
  {
    label: "Component 1",
    component: <div> component 1 </div>,
    key: "key-0",
  },
  {
    label: "Component 2",
    component: <div> component 2 </div>,
    key: "key-1",
  },
  {
    label: "Component 3",
    component: <div> component 3 </div>,
    key: "key-2",
  },
];

it("is rendered in document body", () => {
  const { getByTestId } = render(<Tabs items={items} key={0} />);
  expect(getByTestId("container")).toBeInTheDocument();
});

it("is default tab active", () => {
  const index = 1;
  const { getByTestId } = render(<Tabs items={items} defaultTab={index} />);

  const node = getByTestId(items[index].key);
  expect(node.parentNode).toBe(getByTestId("head"));
  expect(node.classList.contains("tab__item_active")).toBeTruthy();

  const contentNode = getByTestId("animated-content");
  expect(contentNode).toHaveTextContent(
    items[index].component.props.children.trim()
  );
});

it("is changed tab", () => {
  let index = 0;
  const { getByTestId } = render(<Tabs items={items} defaultTab={index} />);

  index += 2;
  const clickNode = getByTestId(items[index].key);
  fireEvent.click(clickNode);

  const node = getByTestId(items[index].key);
  expect(node.classList.contains("tab__item_active")).toBeTruthy();
});
