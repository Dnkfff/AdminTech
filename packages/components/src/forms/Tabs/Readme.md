```tsx
import React, {useState} from "react";
import {Icon} from "@package/components";

const [previousTab, setPreviousTab] = useState('second_item_1');

<>
    Active tab: {previousTab}
    <br/>
    <br/>
    <Tabs.Stateless
      currentTab={previousTab}
      onChange={setPreviousTab}
      items={[
          {
              label: "Users",
              key: 'first_item',
              component: <Icon view={Icon.views.CIRCLED_PLUS}/>,
          },
          {
              label: "Categories",
              key: 'second_item',
              component: <Icon view={Icon.views.RIGHT_ARROW}/>,
          },
          {
              label: "Dropdowns",
              key: 0,
              component: <Icon view={Icon.views.THREE_DOTS}/>,
          },
      ]}
    />
</>
```

```tsx
import {Icon} from "@package/components";

<Tabs 
  defaultTab={'second_item'}
  items={[
    {
      label: "Users",
      key: 'first_item',
      component: <Icon view={Icon.views.CIRCLED_PLUS}/>,
    },
    {
      label: "Categories",
      key: 'second_item',
      component: <Icon view={Icon.views.RIGHT_ARROW}/>,
    },
    {
      label: "Dropdowns",
      key: 'third_item',
      component: <Icon view={Icon.views.THREE_DOTS}/>,
    },
  ]}
/>
```
