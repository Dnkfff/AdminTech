import "@testing-library/jest-dom";
import * as React from "react";
import { render, fireEvent } from "@testing-library/react";
import Button from "./index";

describe("Button", () => {
  it("is rendered in document", () => {
    const { getByTestId } = render(
      <Button value={"value"} onClick={() => null}>
        content
      </Button>
    );
    expect(getByTestId("button")).toBeInTheDocument();
  });

  it("is accepts props style settings", () => {
    const type = Button.types.Rectangle;
    const color = Button.colors.Carnation;
    const fill = Button.fills.GullGray;
    const border = Button.borders.Picasso;
    const { getByTestId } = render(
      <Button
        value="value"
        onClick={() => null}
        type={type}
        color={color}
        fill={fill}
        border={border}
      >
        content
      </Button>
    );
    const node = getByTestId("button");
    expect(node.className).toContain(`type-${type}`);
    expect(node.className).toContain(`fill-${fill}`);
    expect(node.className).toContain(`border-${border}`);
    expect(node.className).toContain(`color-${color}`);
  });

  it("is accepts value, children, name and disable prop", () => {
    const value = "value";
    const disabled = true;
    const name = "name";
    const textContent = "content";
    const { getByTestId } = render(
      <Button
        value={value}
        onClick={() => null}
        disabled={disabled}
        name={name}
      >
        {textContent}
      </Button>
    );
    const node = getByTestId("button") as HTMLButtonElement;
    expect(node.value).toBe(value);
    expect(node.disabled).toBe(disabled);
    expect(node.name).toBe(name);
    expect(node.textContent).toBe(textContent);
  });

  it("is clicked on user press", () => {
    const fn = jest.fn();
    const { getByTestId } = render(<Button onClick={fn}>content</Button>);
    const node = getByTestId("button") as HTMLButtonElement;
    fireEvent.click(node);
    fireEvent.click(node);
    expect(fn).toHaveBeenCalledTimes(2);
  });

  it("is not clicked if disabled", () => {
    const fn = jest.fn();
    const { getByTestId } = render(
      <Button onClick={fn} disabled>
        content
      </Button>
    );
    const node = getByTestId("button") as HTMLButtonElement;
    fireEvent.click(node);
    fireEvent.click(node);
    expect(fn).toHaveBeenCalledTimes(0);
  });
});

describe("Button.Icon", () => {
  it("is rendered in document", () => {
    const { getByTestId } = render(
      <Button.Icon view={Button.Icon.views.IN} onClick={() => null} />
    );
    expect(getByTestId("button.icon")).toBeInTheDocument();
  });

  it("is render icon from sprite inside", () => {
    const view = Button.Icon.views.RIGHT_ARROW;
    const { getByTestId } = render(
      <Button.Icon view={view} onClick={() => null} />
    );
    const node = getByTestId("button.icon");
    const use = node.querySelector("svg > use") as SVGUseElement;
    expect(use.getAttribute("xlink:href")).toContain(view);
  });

  it("is accepts value, name and disable prop", () => {
    const value = "value";
    const disabled = true;
    const name = "name";
    const { getByTestId } = render(
      <Button.Icon
        view={Button.Icon.views.RIGHT_ARROW}
        value={value}
        onClick={() => null}
        disabled={disabled}
        name={name}
      />
    );
    const node = getByTestId("button.icon") as HTMLButtonElement;
    expect(node.value).toBe(value);
    expect(node.disabled).toBe(disabled);
    expect(node.name).toBe(name);
  });

  it("is clicked on user press", () => {
    const fn = jest.fn();
    const { getByTestId } = render(
      <Button.Icon view={Button.Icon.views.RIGHT_ARROW} onClick={fn} />
    );
    const node = getByTestId("button.icon") as HTMLButtonElement;
    fireEvent.click(node);
    fireEvent.click(node);
    expect(fn).toHaveBeenCalledTimes(2);
  });

  it("is not clicked if disabled", () => {
    const fn = jest.fn();
    const { getByTestId } = render(
      <Button.Icon view={Button.Icon.views.RIGHT_ARROW} onClick={fn} disabled />
    );
    const node = getByTestId("button.icon") as HTMLButtonElement;
    fireEvent.click(node);
    fireEvent.click(node);
    expect(fn).toHaveBeenCalledTimes(0);
  });
});
