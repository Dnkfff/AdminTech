import React, { FC, MouseEventHandler } from "react";
import { always } from "../../utils";
import cx from "classnames";

import styles from "./button.module.scss";

const Colors = {
  DarkBlueGrey: "dark-blue-grey",
  GullGray: "gull-gray",
  Coral: "coral",
  BigStone: "big-stone",
  HavelockBlue: "havelock-blue",
  Carnation: "carnation",
  White: "white",
  Picasso: "picasso",
  Transparent: "transparent",
} as const;

const Fills = {
  DarkBlueGrey: "dark-blue-grey",
  GullGray: "gull-gray",
  Coral: "coral",
  BigStone: "big-stone",
  HavelockBlue: "havelock-blue",
  Carnation: "carnation",
  White: "white",
  Picasso: "picasso",
  Transparent: "transparent",
} as const;

const Borders = {
  DarkBlueGrey: "dark-blue-grey",
  GullGray: "gull-gray",
  Coral: "coral",
  BigStone: "big-stone",
  HavelockBlue: "havelock-blue",
  Carnation: "carnation",
  White: "white",
  Picasso: "picasso",
  Transparent: "transparent",
} as const;

const Types = {
  Rounded: "rounded",
  Rectangle: "rectangle",
} as const;

export interface IButton {
  value?: HTMLButtonElement["value"];
  name?: HTMLButtonElement["name"];
  onClick: MouseEventHandler<HTMLButtonElement>;
  disabled?: HTMLButtonElement["disabled"];
  className?: HTMLButtonElement["className"];
  fill?: ValueOf<typeof Fills>;
  color?: ValueOf<typeof Colors>;
  border?: ValueOf<typeof Borders>;
  type?: ValueOf<typeof Types>;
  children: React.ReactNode;
}

const Button: FC<IButton> & {
  fills: typeof Fills;
  borders: typeof Borders;
  colors: typeof Colors;
  types: typeof Types;
} = ({
  onClick = always.EMPTY_FUNC,
  value,
  name,
  disabled,
  fill = Fills.Transparent,
  color = Colors.DarkBlueGrey,
  border = Borders.DarkBlueGrey,
  type = Types.Rounded,
  className,
  children = "Default text",
}) => {
  return (
    <button
      className={cx(
        styles.container,
        {
          [styles[`color-${color}`]]: color,
          [styles[`fill-${fill}`]]: fill,
          [styles[`border-${border}`]]: border,
          [styles[`type-${type}`]]: type,
        },
        className
      )}
      data-testid="button"
      type="button"
      onClick={onClick}
      value={value}
      name={name}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

Button.displayName = "Button";
Button.borders = Borders;
Button.fills = Fills;
Button.colors = Colors;
Button.types = Types;

export default Button;
