#### Button with icon content

```tsx
<Button.Icon
  view={Button.Icon.views.CIRCLED_PLUS}
  onClick={({target: {name, value}}) => console.log(name, value)}
  value="value"
  name="key"
/>
```


#### Button content

```tsx
<Button
  onClick={({target: {name, value}}) => console.log(name, value)}
  value="value"
  name="key"
  color={Button.colors.Coral}
  border={Button.borders.Coral}
  type={Button.types.Rounded}
>
  Admin Tech
</Button>
```
