#### Swiper content

```tsx
import React from "react";

const data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
const component = ({ content }) => (
  <div
    style={{
      background: "#5fa2db",
      padding: "10px",
    }}
  >
    {content}
  </div>
);

const firstActiveSlide = 0;
const step = 1;
const minComponentWidth = 150;
const spaceBetween = 10;

<Swiper
  data={data}
  component={component}
  firstActiveSlide={firstActiveSlide}
  step={step}
  minComponentWidth={minComponentWidth}
  spaceBetween={spaceBetween}
/>;
```
