import React, { FC, useCallback, useMemo, useEffect, useState } from "react";
import Button from "../Button";
import Icon from "../Icon";
import { useDimensions } from "../../hooks";
import { animated, useTransition } from "react-spring";

import styles from "./Swiper.module.scss";

export type ISwiper = {
  data: Array<any>;
  component: FC<{ content: any }>;
  firstActiveSlide?: number;
  step?: number;
  minComponentWidth?: number;
  spaceBetween?: number;
};

type IState = {
  min: number;
  max: number;
  items: JSX.Element[];
};

const initialState: IState = {
  min: 0,
  max: 0,
  items: [],
};

const MIN = 0;

const countCurrentMax = (
  activeSlide: number,
  max: number,
  slidesQuantity: number
) => {
  return activeSlide + slidesQuantity <= max
    ? activeSlide + slidesQuantity
    : (activeSlide + slidesQuantity) % max;
};

const visibleComponents = (
  allSlides: JSX.Element[],
  min: number,
  max: number
) => {
  return max > min
    ? allSlides.slice(min, max)
    : allSlides.slice(min).concat(allSlides.slice(0, max));
};

const Swiper: FC<ISwiper> = ({
  data = [],
  component: Component,
  firstActiveSlide = 0,
  step = 1,
  minComponentWidth = 150,
  spaceBetween = 10,
}) => {
  const MAX = data.length;

  const _firstActiveSlide =
    firstActiveSlide >= MIN && firstActiveSlide < MAX ? firstActiveSlide : MIN;

  const _step = step > 0 && step < MAX ? step : 1;

  const allSlides = useMemo(() => {
    return data.map((elem, i) => (
      <div
        key={i}
        style={{
          marginLeft: `${Math.floor(spaceBetween / 2)}px`,
          marginRight: `${Math.floor(spaceBetween / 2)}px`,
          minWidth: `${minComponentWidth}px`,
        }}
      >
        <Component content={elem} />
      </div>
    ));
  }, [data, spaceBetween, minComponentWidth, Component]);

  const [ref, bounds] = useDimensions();
  const slidesQuantity = Math.floor(
    bounds.width / (minComponentWidth + spaceBetween)
  );

  const [currentSlides, setCurrentSlides] = useState(initialState);

  useEffect(() => {
    setCurrentSlides({
      min: _firstActiveSlide,
      max: countCurrentMax(_firstActiveSlide, MAX, slidesQuantity),
      items: visibleComponents(
        allSlides,
        _firstActiveSlide,
        countCurrentMax(_firstActiveSlide, MAX, slidesQuantity)
      ),
    });
  }, [slidesQuantity]);

  const transitions = useTransition(currentSlides.items, {
    from: () => ({
      flex: 1,
      position: "absolute" as const,
      opacity: 0,
      transform: `rotateY(180deg)`,
    }),
    enter: () => ({
      flex: 1,
      position: "static" as const,
      opacity: 1,
      transform: `rotateY(0deg)`,
    }),
    leave: () => ({
      flex: 1,
      position: "absolute" as const,
      opacity: 0,
      transform: `rotateY(180deg)`,
    }),
  });

  const onClickLeftArrow = useCallback(() => {
    const currentMin =
      currentSlides.min - _step >= MIN
        ? currentSlides.min - _step
        : MAX + (currentSlides.min - _step);

    const currentMax =
      currentSlides.max - _step > MIN
        ? currentSlides.max - _step
        : MAX + (currentSlides.max - _step);

    setCurrentSlides({
      min: currentMin,
      max: currentMax,
      items: visibleComponents(allSlides, currentMin, currentMax),
    });
  }, [setCurrentSlides, currentSlides, _step]);

  const onClickRightArrow = useCallback(() => {
    const currentMin =
      currentSlides.min + _step < MAX
        ? currentSlides.min + _step
        : (currentSlides.min + _step) % MAX;

    const currentMax =
      currentSlides.max + _step <= MAX
        ? currentSlides.max + _step
        : (currentSlides.max + _step) % MAX;

    setCurrentSlides({
      min: currentMin,
      max: currentMax,
      items: visibleComponents(allSlides, currentMin, currentMax),
    });
  }, [setCurrentSlides, currentSlides, MAX, _step]);

  return (
    <div className={styles.container}>
      <Button.Icon
        view={Icon.views.RIGHT_ARROW}
        className={styles.arrowLeft}
        onClick={onClickLeftArrow}
      />
      <div className={styles.items} ref={ref}>
        <div className={styles.item}>
          {transitions((styles, item) => (
            <animated.div style={styles}>{item}</animated.div>
          ))}
        </div>
      </div>
      <Button.Icon view={Icon.views.RIGHT_ARROW} onClick={onClickRightArrow} />
    </div>
  );
};

export default Swiper;
