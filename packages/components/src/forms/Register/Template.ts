/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { ChangeEventHandler, MouseEventHandler } from "react";

export type ITemplate = {
  filterComponent?: (
    e: {
      name: string;
      value: any;
      onChange: ChangeEventHandler;
    } & Record<string, any>
  ) => React.ReactNode;
  isSortEnable?: boolean;
  columnsRatio: string;
  valueKey: string;
  valueComponent?: (
    e: {
      name: string | number;
      value: any;
      onClick: MouseEventHandler;
      className: string;
    } & Record<string, any>
  ) => React.ReactNode;
};
