#### Register with icon content

```tsx
import React, {useEffect, useState, useMemo, useCallback} from "react";
import {Modal, useToggle, Icon, Input} from "@package/components";

const [data, setData] = useState([]);
const [rowsValue, setRowsValue] = useState([]);
const [currentPage, setCurrentPage] = useState(1);
const [isFetching, setIsFetching] = useState(false);
const [sort, setSort] = useState({id: "DESC"});
const [filters, setFilters] = useState({id: ""});

useEffect(() => {
  fetch(`https://jsonplaceholder.typicode.com/comments?_limit=100&_page=${currentPage}`)
    .then((response) => response.json())
    .then((data1) => {
      setData([...data, ...data1]);
      setCurrentPage(prevState => prevState + 1);
    })
    .finally(() => setIsFetching(false));
}, [isFetching]);

const sortHandler = useCallback((key, value) => {
  setSort((prevSort) => {
    const state = {...prevSort, [key]: value}
    console.log('sort: ', state)
    return state
  });
}, [setSort]);

const onFilterChange = useCallback((event) => {
  setFilters((prevFilters) => {
    const state = {...prevFilters, [event.target.name]: event.target.value}
    console.log('filter: ', state)
    return state
  });
}, [setFilters]);

const onClickHandler = useCallback((event) => {
  console.log('onClickHandler', event)
}, []);

const onScrollDown = useCallback(() => {
  setIsFetching(true);
}, [setIsFetching]);
console.log(rowsValue);
const onClickRowHandler = useCallback((e) => {
  setRowsValue(e)
}, [rowsValue, setRowsValue]);


const template = useMemo(() => [
  {
    filterComponent: ({value, onChange, name}) => <Input placeholder='id' value={value} onChange={onChange}
                                                         name={name}/>,
    isSortEnable: true,
    columnsRatio: "0.6fr",
    valueKey: "id",
  },
  {
    filterComponent: ({value, onChange, name}) => <Input placeholder='name' value={value} onChange={onChange}
                                                         name={name}/>,
    isSortEnable: true,
    columnsRatio: "1fr",
    valueKey: "name",
  },
  {
    filterComponent: ({value, onChange, name}) => <Input placeholder='email' value={value} onChange={onChange}
                                                         name={name}/>,
    isSortEnable: true,
    columnsRatio: "1.8fr",
    valueKey: "email",
  },
  {
    filterComponent: ({value, onChange, name}) => <Input placeholder='body' value={value} onChange={onChange}
                                                         name={name}/>,
    isSortEnable: true,
    columnsRatio: "1.8fr",
    valueKey: "body",
  },
  {
    filterComponent: () => '',
    valueComponent: ({value, onClick, name}) => <button name={name} style={{
      border: 'none',
      background: 'none',
      cursor: 'pointer',
    }} onClick={onClick} value={value}>
      < Icon view="three-dots" className="bluey_grey"/>
    </button>,
    columnsRatio: "50px",
    valueKey: "ellipsis",
  },
  {
    filterComponent: () => '',
    valueComponent: ({value, onClick, name}) => <button name={name} style={{
      border: 'none',
      background: 'none',
      cursor: 'pointer',
    }} onClick={onClick} value={value}>
      <Icon view="circled-plus" className="bluey_grey"/>
    </button>,
    columnsRatio: "50px",
    valueKey: "settings",
  },
], []);

<div style={{height: '80vh'}}>
  <Register
    data={data}
    onScrollDown={onScrollDown}
    sortValues={sort}
    onSortChange={sortHandler}
    filterValues={filters}
    onFilterChange={onFilterChange}
    onClickHandler={onClickHandler}
    template={template}
    rowsValue={rowsValue}
    onClickRowHandler={onClickRowHandler}
  />
</div>
```
