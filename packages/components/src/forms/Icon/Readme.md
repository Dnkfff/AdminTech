#### Add new icons algorithm :
> 
> 1. Add new _*.svg*_ icon to _*components/icons*_ folder
> 2. Run following script
> ```bash
> yarn build:assets
> ```

#### Basic usage :

```tsx
import { SpriteProvider } from '@package/components';
<SpriteProvider>
    <Icon view={Icon.views.CIRCLED_PLUS}/>
</SpriteProvider>
```

#### Existing icons with demonstrating color setting :

```tsx
const Box = ({ children }) => 
<div style={{ 
    display: "grid",
    gridTemplateRows: '1fr auto',
    placeItems: 'center',
    gap: '5px',
    border: '1px dashed',
    padding: '5px'
}}>
    {children}
</div>

const iconsList = Object.entries(Icon.views).map(([key, value]) => {
    return <Box key={key}>
        <Icon view={value} className="fill_green" />
        {key}
    </Box>
});

const styles = `
    .fill_red { fill: red }
    .fill_green { fill: green }
    .fill_blue { fill: blue }
`;

<div style={{
    display: 'grid', 
    gridTemplateColumns: 'repeat(auto-fit, minmax(200px, 1fr))',
    alignItems: 'center',
    gap: '10px',
    alignItems: 'stretch'
}}>
    <style dangerouslySetInnerHTML={{ __html: styles }}/>
    {iconsList}
</div>
```
