import "@testing-library/jest-dom";
import * as React from "react";
import { render } from "@testing-library/react";
import Icon from "./index";

describe("Icon", () => {
  it("is rendered in document", () => {
    const { getByTestId } = render(<Icon view={Icon.views.IN} />);
    expect(getByTestId("container")).toBeInTheDocument();
  });

  it("is render icon", () => {
    const view = Icon.views.RIGHT_ARROW;
    const { getByTestId } = render(<Icon view={view} />);
    const node = getByTestId("container").querySelector("use") as SVGUseElement;
    expect(node.getAttribute("xlink:href")).toContain(view);
  });

  it("is accepts className prop", () => {
    const className = "value";
    const { getByTestId } = render(
      <Icon view={Icon.views.RIGHT_ARROW} className={className} />
    );
    const node = getByTestId("container");
    expect(node.classList.contains(className)).toBeTruthy();
  });
});
