```tsx
<Accordion title="Accordion">
    <div style={{ height: '250px', width: '100%', background: 'red' }} />
</Accordion>
```

```tsx
<Accordion.Stateless isCollapsed={false} onClickCollapse={console.log} title="Accordion stateless">
    <div style={{ height: '250px', width: '100%', background: 'red' }} />
</Accordion.Stateless>
```
