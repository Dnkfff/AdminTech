import "@testing-library/jest-dom";
import * as React from "react";
import { render, fireEvent } from "@testing-library/react";

import Accordion from "./index";

it("is rendered in document", () => {
  const { getByTestId } = render(
    <Accordion title="Head" isDefaultCollapsed={false}>
      content
    </Accordion>
  );
  expect(getByTestId("container")).toBeInTheDocument();
});

it("is content render in accordion", () => {
  const child = "content in accordion";
  const { getByTestId } = render(
    <Accordion title="Head" isDefaultCollapsed={false}>
      {child}
    </Accordion>
  );
  const element = getByTestId("content");
  expect(element?.textContent?.includes(child)).toBe(true);
});

it("is not collapsed by default", () => {
  const { getByTestId } = render(
    <Accordion title="Head" isDefaultCollapsed={false}>
      content in accordion
    </Accordion>
  );
  const element = getByTestId("head-container");
  expect(element.dataset.value).toBe(String(false));
});

it("is collapsed on click", () => {
  const { getByTestId } = render(
    <Accordion title="Head" isDefaultCollapsed={false}>
      content
    </Accordion>
  );
  const element = getByTestId("head-container");
  fireEvent.click(element);
  expect(element.dataset.value).toBe(String(true));
});
