import React, { FC, useLayoutEffect, useRef } from "react";
import Icon from "../Icon";
import { animated, useSpring } from "react-spring";
import styles from "./styles.module.scss";

export interface IAccordionStateless {
  isCollapsed: boolean;
  onClickCollapse: () => void;
  title: string | React.ReactNode;
  iconComponent?: React.FC<{ isCollapsed: IAccordionStateless["isCollapsed"] }>;
  titleComponent?: React.FC<{ title: IAccordionStateless["title"] }>;
  children: React.ReactNode;
}

const DefaultIconComponent: FC<{
  isCollapsed: IAccordionStateless["isCollapsed"];
}> = ({ isCollapsed }) => {
  const [styleIcon] = useSpring(() => {
    const transform = isCollapsed ? "rotate(90deg)" : "rotate(0deg)";
    return { transform };
  }, [isCollapsed]);

  return (
    <animated.div style={styleIcon}>
      <Icon view={Icon.views.RIGHT_ARROW} data-value={isCollapsed} />
    </animated.div>
  );
};

const DefaultTitleComponent: FC<{
  title: IAccordionStateless["title"];
}> = ({ title }) => <div className={styles.title}>{title}</div>;

const AccordionStateless: FC<IAccordionStateless> = ({
  isCollapsed,
  onClickCollapse,
  title,
  children,
  iconComponent: IconComponent = DefaultIconComponent,
  titleComponent: TitleComponent = DefaultTitleComponent,
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const [styleContent, animateContent] = useSpring(() => ({ height: "0px" }));

  useLayoutEffect(() => {
    if (ref.current) {
      const height = isCollapsed ? 0 : ref.current.scrollHeight;
      animateContent.start({ height: height + "px" });
    }
  }, [animateContent, ref, isCollapsed]);

  return (
    <div className={styles.container} data-testid="container">
      <button
        onClick={onClickCollapse}
        className={styles.head}
        data-value={isCollapsed}
        data-testid="head-container"
      >
        <TitleComponent title={title} />
        <IconComponent isCollapsed={isCollapsed} />
      </button>

      <animated.div
        style={styleContent}
        className={styles.content}
        ref={ref}
        data-testid="content"
      >
        {children}
      </animated.div>
    </div>
  );
};

export default AccordionStateless;
