import "@testing-library/jest-dom";
import * as React from "react";
import { render, fireEvent } from "@testing-library/react";
import Modal from "./index";

describe("Modal", () => {
  it("is rendered in document body", () => {
    const { getByTestId } = render(<Modal isVisible>content</Modal>);
    expect(getByTestId("modal").parentNode).toBe(document.body);
  });

  it("is not rendered in document if invisible", () => {
    const { queryByTestId } = render(<Modal isVisible={false}>content</Modal>);
    expect(queryByTestId("modal")).not.toBeInTheDocument();
  });

  it("is not rendered in document if invisible", () => {
    const { queryByTestId } = render(<Modal isVisible={false}>content</Modal>);
    expect(queryByTestId("modal")).not.toBeInTheDocument();
  });
});

describe("Modal.Headered", () => {
  it("is call onClose when click close button", () => {
    const fn = jest.fn();
    const { getByTestId } = render(
      <Modal.Headered isVisible onClose={fn}>
        content
      </Modal.Headered>
    );
    fireEvent.click(getByTestId("button.icon"));
    expect(fn).toBeCalledTimes(1);
  });

  it("is call onClose when keydown escape", () => {
    const fn = jest.fn();
    const { getByTestId } = render(
      <Modal.Headered isVisible onClose={fn}>
        content
      </Modal.Headered>
    );
    fireEvent.keyDown(getByTestId("modal"), {
      key: "Escape",
      code: "Escape",
      keyCode: 27,
      charCode: 27,
    });
    expect(fn).toBeCalledTimes(1);
  });
});
