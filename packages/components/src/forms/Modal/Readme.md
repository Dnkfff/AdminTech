#### Modal content

```tsx
import React from 'react';
import { useToggle } from '@package/components/hooks'

const [visibleModal, setVisibleModal] = useToggle(false);
const [visibleModalHeadered, setvisibleModalHeadered] = useToggle(false);

<div style={{ display: 'flex', justifyContent: 'space-between', width: 400 }}>
  
  <button onClick={setVisibleModal.on}>Open Modal!</button>
  
  <Modal isVisible={visibleModal}>
    <div style={{ color: 'red' }} onClick={setVisibleModal.off}>CLOSE!!!</div>
    <div>Hi!!!</div>
    <div style={{width: 400, heigth: 600, background: "green"}}> body</div>
    <div> footer</div>
  </Modal>

  <button onClick={setvisibleModalHeadered.on}>Open Modal Headered!</button>
  
  <Modal.Headered isVisible={visibleModalHeadered} title="Password Recovery" onClose={setvisibleModalHeadered.off}>
    <div>Hi!!!</div>
    <div style={{width: 400, height: 200, background: "blue"}}> body</div>
    <div> footer</div>
  </Modal.Headered>

</div>
```
