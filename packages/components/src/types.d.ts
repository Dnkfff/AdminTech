/**
 * @description global
 */

declare module "*.module.scss" {
  const content: Record<string, string>;
  export default content;
}

declare module "*.svg" {
  const content: Record<string, string>;
  export default content;
}

/**
 * @description local
 */

type ValueOf<T> = T[keyof T];
