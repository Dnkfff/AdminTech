import { useLayoutEffect } from "react";

export const useEventListener = (
  key: keyof DocumentEventMap,
  event: (this: Document, ev: DocumentEventMap[typeof key]) => any
) => {
  useLayoutEffect(() => {
    document.addEventListener(key, event);
    return () => {
      document.removeEventListener(key, event);
    };
  }, [event]);
};

export default useEventListener;
