- useToggle
```tsx
import { useToggle } from "@package/components";

const [value, onToggle] = useToggle();

<div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
    Now: {String(value)}
    <button onClick={onToggle}> Click me to toggle </button>
    <button onClick={onToggle.off}> Click me to toggle OFF always </button>
    <button onClick={onToggle.on}> Click me to toggle ON always </button>
</div>
```

- useClickOutside
```tsx
import { useClickOutside, useToggle } from "@package/components";

const [isVisible, onToggleVisible] = useToggle()

const ref = React.useRef(null)

const handler = () => { alert('You click out') }

useClickOutside(ref, handler);

<div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
    {!isVisible && <button onClick={onToggleVisible.on}>Enable preview</button> }
    {isVisible && <div ref={ref} style={{
        width: '100%',
        height: 100,
        background: "grey",
        display: 'flex',
        flexDirection: 'column',
        gap: '10px',
        justifyContent: 'center',
        alignItems: 'center'
    }}>
        {isVisible && <button onClick={onToggleVisible.off}>Disable preview</button>}

        Click out of me
    </div>
    }
</div>
```

- useKeyPress
```tsx
import { useKeyPress, useToggle } from "@package/components";

const [isVisible, onToggleVisible] = useToggle()

const PreviewComponent = () => {
    const handler = () => { alert('You enter') }
    useKeyPress('Enter', handler);
    return 'Press "Enter" key'
}


<div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
    {!isVisible && <button onClick={onToggleVisible.on}>Enable preview</button> }
    {isVisible && <div style={{
        width: '100%',
        height: 'fit-content',
        display: 'flex',
        flexDirection: 'column',
        gap: '10px',
        justifyContent: 'center',
        alignItems: 'center'
    }}>
        {isVisible && <button onClick={onToggleVisible.off} style={{ width: '100%'}}>Disable preview</button>}

        <PreviewComponent/>
    </div>
    }
</div>
```

- useModal
```tsx
import { useModal, useToggle } from "@package/components";

const [isVisible, onToggleVisible] = useToggle()

const PreviewComponent = (props) => {
    return <div style={{ padding: '10px' }}>
        { 'Hi, I\'m modal with props: ' + JSON.stringify(props, null, 2) }
    </div>
}

const [fragment, onOpen, onClose] = useModal(PreviewComponent, { modalProps: { title: 'Title' } });

<div style={{ display: 'flex', flexDirection: 'column' }}>
    <button onClick={() => onOpen({ property1: '1', property2: '2' })}>Open modal</button>
    { fragment }
</div>
```

- useDimensions
```tsx
import { useDimensions, useToggle } from "@package/components";

const [ref, bounds] = useDimensions();

<div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
    <textarea ref={ref} disabled>Resize me</textarea>
    {JSON.stringify(bounds, null, 2)}
</div>
```

- useEvent
```tsx
import { useEvent } from "@package/components";

const eventFn = useEvent(() => alert("Hello"));

<div style={{ display: 'flex', flexDirection: 'column' }}>
    <button onClick={eventFn}> Event handler doesn't recreates each time  </button>
</div>
```
