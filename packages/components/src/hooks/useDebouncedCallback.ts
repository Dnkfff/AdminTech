import React from "react";

const useDebouncedCallback = <
  F extends (...args: any[]) => void = (...args: any[]) => void
>(
  callback: F,
  delay: number,
  dependencies?: any[]
) => {
  const timeout = React.useRef<number>(null);

  // Avoid error about spreading non-iterable (undefined)
  const comboDeps = dependencies
    ? [callback, delay, ...dependencies]
    : [callback, delay];

  return React.useCallback((...args: Parameters<F>) => {
    if (timeout.current != null) {
      clearTimeout(timeout.current);
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    timeout.current = setTimeout(() => {
      callback(...args);
    }, delay);
  }, comboDeps);
};

export default useDebouncedCallback;
