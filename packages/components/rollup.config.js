import typescript from "rollup-plugin-typescript2";
import { terser } from "rollup-plugin-terser";
import postcss from "rollup-plugin-postcss";
import commonjs from "@rollup/plugin-commonjs";
import pkg from "./package.json";
import resolve from "@rollup/plugin-node-resolve";
import image from "@rollup/plugin-image";
import svg from "rollup-plugin-svg";
import ts from "typescript";

export default {
  input: "./src/index.ts",
  external: [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {}),
  ],
  output: [
    {
      file: `./${pkg.module}`,
      format: "es",
      sourcemap: true,
    },
    {
      file: `./${pkg.main}`,
      format: "cjs",
      sourcemap: true,
    },
  ],
  plugins: [
    svg(),
    resolve(),
    commonjs(),
    postcss(),
    image({ exclude: "**/*/*.svg" }),
    typescript({
      typescript: ts,
      tsconfig: "tsconfig.json",
    }),
    terser({
      output: {
        comments: false,
      },
    }),
  ],
};
