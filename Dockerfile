FROM nginx

ARG ENVIR

RUN mkdir -p /etc/nginx/conf.d/

COPY ./application/build   /var/lib/app/

COPY ./packages/components/styleguide /var/lib/styleguide

WORKDIR /var/lib/app/

COPY nginx-site.conf /etc/nginx/conf.d/app.conf

