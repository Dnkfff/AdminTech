import { context } from "../i18n";
import { useContext } from "react";

export const useI18n = () => useContext(context);
