import { useResource } from "./useResource";
import { country_language } from "src/service";

export type ICountryLanguageResource = {
  codeCountry: string;
  countryId: string;
  countryName: Record<string, string>;
  children: string[];
  childrenEntities: {
    id: number;
    short: string;
    long: Record<string, string>;
  }[];
};

export const useCountryLanguageResource = () => {
  return useResource<ICountryLanguageResource>(
    "country-language-resource",
    async () => {
      const { data } = await country_language.getCountryLanguages();
      return { data: data.message };
    },
    {
      unmountDelay: 3000,
    }
  );
};
