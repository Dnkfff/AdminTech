import React, { FC } from "react";
import { Layout } from "src/components";
import Documents from "./Documents";
import Banner from "./Banner";
const Home: FC = () => {
  return (
    <Layout
      stickyHeader
      headOptions={[
        Layout.headOptions.countryAndLanguage,
        Layout.headOptions.basket,
        Layout.headOptions.auth,
      ]}
    >
      <Banner />
      <Documents />
    </Layout>
  );
};

export default Home;
