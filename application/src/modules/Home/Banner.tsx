import React, { FC } from "react";
import { useI18n } from "../../hooks";

import styles from "./Banner.module.scss";

const Banner: FC = () => {
  const i18n = useI18n();
  return (
    <svg viewBox="0 0 1444 498" className={styles.container}>
      <defs>
        <path id="picqor5pga" d="M0 .5h1444v496H0z" />
      </defs>
      <g transform="translate(0 .5)" fill="none">
        <use fill="#122430" xlinkHref="#picqor5pga" />
        <circle
          stroke="#FFF"
          opacity=".5"
          transform="matrix(-1 0 0 1 2113 0)"
          cx="1056.5"
          cy="253"
          r="530"
        />
        <circle
          stroke="#FFF"
          opacity=".295"
          transform="matrix(-1 0 0 1 149 0)"
          cx="74.5"
          cy="715"
          r="470.5"
        />
        <circle
          stroke="#FFF"
          opacity=".295"
          transform="matrix(-1 0 0 1 2849 0)"
          cx="1424.5"
          cy="565"
          r="202"
        />
        <circle
          fill="#F5574A"
          transform="matrix(-1 0 0 1 745 0)"
          cx="349"
          cy="159"
          r="118"
        />
        <circle
          fill="#142F41"
          transform="matrix(-1 0 0 1 2090 0)"
          cx="1045"
          cy="247.5"
          r="202"
        />
        <path
          stroke="#FFF"
          strokeWidth="1.176"
          opacity=".3"
          d="M961.5 167.087v166.826"
        />
      </g>
      <text className={styles.currency} transform="translate(961 155.5)">
        <tspan x={15} y={23}>
          CHF
        </tspan>
      </text>
      <text transform="translate(961 155.5)">
        <tspan x={72} y={110} className={styles.number}>
          9
        </tspan>
      </text>
      <text className={styles.document} transform="translate(961 155.5)">
        <tspan x={15} y={139}>
          {i18n.home.per_legal}
        </tspan>
        <tspan x={15} y={169}>
          {i18n.home.document}
        </tspan>
      </text>
      <text className={styles.buy}>
        <tspan x={400} y={223} className={styles.buy}>
          {i18n.home.try_it_before}
        </tspan>
        <tspan x={499} y={289} className={styles.buy}>
          {i18n.home.you_buy_it}
        </tspan>
      </text>
      <text className={styles.create}>
        <tspan x={345} y={326}>
          {i18n.home.create_your_legal_documents_in_a_couple_of_clicks}
        </tspan>
      </text>
    </svg>
  );
};
export default Banner;
