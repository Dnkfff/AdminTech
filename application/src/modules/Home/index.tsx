import React, { lazy } from "react";

const Component = lazy(() => import("./Home"));

export default {
  path: "/",
  element: <Component />,
};
