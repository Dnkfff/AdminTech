/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { ChangeEvent, ChangeEventHandler, FC } from "react";
import { ICountryAndLanguageEntity } from "./types";
import { useFormChange, useI18n } from "src/hooks";
import type { ICountryResource } from "src/hooks/useResource/useCountriesResource";
import type { ILanguageResource } from "src/hooks/useResource/useLanguagesResource";
import { Button, Select, useEvent, Popup, Icon } from "@package/components";

import styles from "./Group.module.scss";

export const idSelector = (option: any) => option?.code;
export const valueSelector = (option: any) => option?.name;

const EN_REQUIRED_COUNTRY_CODE = "EN";

const Group: FC<{
  value: ICountryAndLanguageEntity;
  onChange: ChangeEventHandler;
  name: string;
  countriesOptions: ICountryResource[];
  languagesOptions: ILanguageResource[];
  onRemove: (index: number) => void;
  countriesHiddenSelector: (entity: any) => boolean;
}> = ({
  value,
  onChange,
  name,
  countriesOptions,
  languagesOptions,
  onRemove,
  countriesHiddenSelector,
}) => {
  const input = useFormChange<ICountryAndLanguageEntity>(value, onChange, name);
  const i18n = useI18n();

  const isCountryEntered = !!value?.codeCountry;

  const onChangeIdCountry = useEvent<ChangeEventHandler<HTMLInputElement>>(
    (event) => {
      const {
        target: { value: v, name: n },
      } = event;
      onChange({
        target: {
          name,
          value: { ...value, [n]: v, children: [EN_REQUIRED_COUNTRY_CODE] },
        },
      } as unknown as ChangeEvent<HTMLInputElement>);
    }
  );

  const onChangeChildren = useEvent<ChangeEventHandler<HTMLInputElement>>(
    (event) => {
      const {
        target: { value: v, name: n },
      } = event;

      onChange({
        target: {
          name,
          value: {
            ...value,
            [n]: Array.from(
              new Set([...Array.from(v), EN_REQUIRED_COUNTRY_CODE])
            ),
          },
        },
      } as unknown as ChangeEvent<HTMLInputElement>);
    }
  );

  const onRemoveHandler = useEvent(() => onRemove(Number(name)));

  return (
    <div className={styles.group}>
      <div className={styles.country}>
        <Select
          {...input.input("codeCountry")}
          onChange={onChangeIdCountry}
          options={countriesOptions}
          idSelector={idSelector}
          valueSelector={valueSelector}
          hiddenSelector={countriesHiddenSelector}
        />
        <Popup
          content={<Icon view={Button.Icon.views.THREE_DOTS} />}
          placement="right"
          triangleSize={10}
          className={styles.dots}
        >
          <Button
            color={Button.colors.Coral}
            border={Button.borders.Transparent}
            onClick={onRemoveHandler}
            className={styles.delete}
          >
            {i18n.common.delete}
          </Button>
        </Popup>
      </div>
      <Select.Multi
        {...input.input("children")}
        onChange={onChangeChildren}
        options={languagesOptions}
        disabled={!isCountryEntered}
        idSelector={idSelector}
        valueSelector={valueSelector}
      />
    </div>
  );
};

export default React.memo(Group);
