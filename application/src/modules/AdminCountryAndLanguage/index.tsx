import React, { lazy } from "react";

const Component = lazy(() => import("./AdminCountryAndLanguage"));

export default {
  path: "/admin-country-language",
  element: <Component />,
};
