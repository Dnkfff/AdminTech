import React, { FC, useCallback, useEffect, useMemo } from "react";
import { useStore } from "react-redux";
import {
  useForm,
  useI18n,
  useLanguagesResource,
  useCountriesResource,
  useFormArrayChange,
} from "src/hooks";
import { country_language } from "src/service";
import type { ICountryResource } from "src/hooks/useResource/useCountriesResource";
import type { ICountryAndLanguageEntity } from "./types";
import { Button } from "@package/components";
import styles from "./Body.module.scss";
import Group from "./Group";

const initialState: ICountryAndLanguageEntity[] = [];

const Body: FC<{ onContinue: () => void }> = ({ onContinue }) => {
  const i18n = useI18n();
  const { dispatch, getState } = useStore();

  const countriesResource = useCountriesResource();
  const { entities: countries, onLoad: onLoadCountries } = countriesResource;

  const languagesResource = useLanguagesResource();
  const { entities: languages, onLoad: onLoadLanguages } = languagesResource;

  const { values, onChange, onSave, onInitialize, store } = useForm<
    ICountryAndLanguageEntity[]
  >("admin-country-and-language", initialState, {
    loader: async () => {
      const { data } = await country_language.getCountryLanguages();
      return data?.message ?? [];
    },
    saver: async (toSave) => {
      const normalized = toSave.filter(({ codeCountry }) => !!codeCountry);
      const { data } = await country_language.updateCountryLanguages(
        normalized
      );
      return { data: data.message ?? [] };
    },
  });

  useEffect(() => {
    onLoadCountries();
    onLoadLanguages();
    onInitialize();
  }, []);

  const input = useFormArrayChange(values, onChange);
  const keys = Object.keys(values);

  const onAppend = useCallback(() => {
    const state = getState();
    const prevValues = useForm.selectors.getValues(state, store.name);
    dispatch(
      store.actions.setValues([
        ...prevValues,
        { codeCountry: null, children: [] },
      ])
    );
  }, [dispatch, store]);

  const onRemove = useCallback(
    (index: number) => {
      const state = getState();
      const prevValues = useForm.selectors.getValues(state, store.name);
      dispatch(
        store.actions.setValues(
          prevValues.filter((_: unknown, _index: number) => _index !== index)
        )
      );
    },
    [dispatch, store]
  );

  const selectedCountries = useMemo(
    () => (values as ICountryAndLanguageEntity[]).map((i) => i.codeCountry),
    [values]
  );

  const countriesHiddenSelector = useCallback(
    (entity: ICountryResource) => !selectedCountries.includes(entity.code),
    [selectedCountries]
  );

  return (
    <div className={styles.container}>
      {keys.map((key) => (
        <Group
          key={key}
          {...input.array(key)}
          countriesOptions={countries ?? []}
          languagesOptions={languages ?? []}
          onRemove={onRemove}
          countriesHiddenSelector={countriesHiddenSelector}
        />
      ))}

      <div className={styles.plus}>
        <Button.Icon view={Button.Icon.views.CIRCLED_PLUS} onClick={onAppend} />
      </div>

      <div className={styles.footer}>
        <Button onClick={onSave} className={styles.button}>
          {i18n.common.save}
        </Button>
        <Button onClick={onContinue} className={styles.button}>
          {i18n.common.continue}
        </Button>
      </div>
    </div>
  );
};

export default Body;
