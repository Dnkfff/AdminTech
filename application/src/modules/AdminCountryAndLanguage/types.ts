type ID = string | number;
export type ICountryAndLanguageEntity = {
  codeCountry: ID;
  children: ID[];
};
