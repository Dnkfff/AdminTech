import React, { FC, useCallback } from "react";
import { Layout } from "src/components";
import { useNavigate } from "react-router-dom";
import adminDashboard from "../AdminDashboard";
import Body from "./Body";

import styles from "./AdminCountryLanguage.module.scss";

const AdminCountryAndLanguage: FC = () => {
  const navigate = useNavigate();
  const onBack = useCallback(() => navigate(adminDashboard.path), [navigate]);
  const onContinue = useCallback(
    () => navigate(adminDashboard.path),
    [navigate]
  );

  return (
    <Layout
      onBack={onBack}
      headOptions={[Layout.headOptions.language, Layout.headOptions.profile]}
      className={styles.layout}
    >
      <Body onContinue={onContinue} />
    </Layout>
  );
};
export default AdminCountryAndLanguage;
