import React, { FC } from "react";
import { LinkedInCallback } from "react-linkedin-login-oauth2";

const callbacks = {
  path: "/callback",
  children: [
    {
      path: "/linkedin",
      element: <LinkedInCallback />,
    },
  ],
};

const CallbackProvider: FC<{ children: React.ReactNode }> = ({ children }) => {
  const pathList = callbacks.children.map(
    ({ path }) => `${callbacks.path}${path}`
  );

  if (pathList.includes(window.location.pathname)) {
    const component = callbacks.children.find(({ path }) =>
      window.location.pathname.includes(path)
    );
    return component?.element || <></>;
  }

  return <>{children}</>;
};

export default CallbackProvider;
