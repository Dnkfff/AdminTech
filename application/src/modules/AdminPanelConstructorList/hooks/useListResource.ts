import { admin_list } from "src/service";
import { useResource } from "src/hooks";
import { IList } from "../type";
import { formatDate } from "../constats";

const FRAME_SIZE = 50;

export const useListResource = () => {
  return useResource<IList>(
    "constructor-list",
    async ({ filter, sort, entries }) => {
      const _filters = { ...filter };

      _filters.updated = _filters.updated
        ? formatDate(new Date(_filters.updated), "yyyy-mm-dd")
        : undefined;

      const query = new URLSearchParams([
        ...Object.entries(_filters)
          .filter(([, value]) => !!value)
          .map(([key, value]) => ["f" + key, value]),

        ...Object.entries(sort)
          .filter(([, value]) => !!value)
          .map(([key, value]) => ["s" + key, value === "ASC" ? "" : 1]),
        ["frame", Math.floor(entries.length / FRAME_SIZE)],
      ]);

      const { data } = await admin_list.getAllTemplates("", query.toString());

      return {
        data: data.message,
        isLoadedAll: data.message.length < FRAME_SIZE,
      };
    }
  );
};
