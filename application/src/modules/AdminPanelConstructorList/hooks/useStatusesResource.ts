import { useResource } from "src/hooks";
import { admin_list } from "src/service";
import { IStatuses } from "../type";

export const useStatusesResource = () => {
  return useResource<IStatuses>("constructor-list-statuses", async () => {
    const { data } = await admin_list.getListOfTemplateStatuses();

    return { data: data.message };
  });
};
