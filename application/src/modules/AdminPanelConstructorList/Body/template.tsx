import React, { ComponentProps } from "react";
import {
  Button,
  Datepicker,
  Input,
  Register,
  Select,
} from "@package/components";
import { formatDate, LINKS, PUBLISH } from "../constats";
import { ITranslationValue } from "src/i18n";
import { LanguagePopup, SettingsPopup } from "../Compotent";
import { IList } from "../type";
import cx from "classnames";

import styles from "./Body.module.scss";

export const idSelector = (option: any) => option?.id;

export const valueStatuses = (option: any) => option?.name.EN;
export const valueTags = (option: any) => option?.nameTag;
export const valueSelector = (option: any) => option?.name;

export const templateItems = (
  i18n: ITranslationValue
): ComponentProps<typeof Register>["template"] => {
  return [
    {
      valueComponent: ({ value, className, languageId }) => (
        <p className={className}>
          {(value as IList).category.name?.[languageId]
            ? (value as IList).category.name?.[languageId]
            : (value as IList).category.name?.EN}
        </p>
      ),
      filterComponent: ({
        value,
        onChange,
        name,
        categoryEntities,
        isCountrySelected,
      }) => (
        <Select
          options={categoryEntities}
          placeholder={i18n.adminPanelConstructorList.category_label}
          name={name}
          value={value}
          onChange={onChange}
          idSelector={idSelector}
          valueSelector={valueStatuses}
          disabled={!isCountrySelected}
        />
      ),
      isSortEnable: true,
      columnsRatio: "1fr",
      valueKey: "categoryId",
    },
    {
      valueComponent: ({ value, className }) => (
        <p className={className}>{(value as IList).documentName}</p>
      ),
      filterComponent: ({ value, onChange, name }) => (
        <Input
          value={value}
          name={name}
          placeholder={i18n.adminPanelConstructorList.name_label}
          onChange={onChange}
        />
      ),
      isSortEnable: true,
      columnsRatio: "1fr",
      valueKey: "name",
    },
    {
      valueComponent: ({ value, className }) => (
        <span className={cx(className, styles.datepicker__item)}>
          {formatDate(new Date((value as IList).updated), "dd.mm.yyyy")}
        </span>
      ),
      filterComponent: ({ value, onChange, name }) => (
        <Datepicker
          className={styles.datepicker}
          placeholder={i18n.adminPanelConstructorList.start_date_label}
          title="Start date"
          name={name}
          value={value}
          dateFormat="DD.MM.YYYY"
          onChange={onChange}
        />
      ),
      isSortEnable: true,
      columnsRatio: ".8fr",
      valueKey: "updated",
    },
    {
      valueComponent: ({ value, className, languageId }) => (
        <p className={className}>
          {(value as IList).country.name?.[languageId]
            ? (value as IList).country.name?.[languageId]
            : (value as IList).country.name?.EN}
        </p>
      ),
      filterComponent: ({ value, onChange, name, countryEntities }) => (
        <Select
          options={countryEntities}
          placeholder={i18n.adminPanelConstructorList.country_label}
          value={value}
          name={name}
          idSelector={idSelector}
          valueSelector={valueSelector}
          onChange={onChange}
        />
      ),
      isSortEnable: true,
      columnsRatio: ".8fr",
      valueKey: "countryId",
    },
    {
      valueComponent: ({ value, className }) => (
        <p className={className}> {(value as IList).language.code} </p>
      ),
      filterComponent: ({ value, onChange, name, languagesEntities }) => (
        <LanguagePopup
          value={value}
          onChange={onChange}
          name={name}
          languagesEntities={languagesEntities}
        />
      ),
      isSortEnable: false,
      columnsRatio: "60px",
      valueKey: "langId",
    },
    {
      valueComponent: ({ value, className, languageId }) => (
        <span className={className}>
          {(value as IList).status.name?.[languageId]
            ? (value as IList).status.name?.[languageId]
            : (value as IList).status.name?.EN}
        </span>
      ),
      filterComponent: ({ value, onChange, name, statusEntities }) => (
        <Select
          options={statusEntities}
          placeholder={i18n.adminPanelConstructorList.status_label}
          value={value}
          name={name}
          idSelector={idSelector}
          valueSelector={valueStatuses}
          onChange={onChange}
        />
      ),
      isSortEnable: true,
      columnsRatio: ".7fr",
      valueKey: "statusId",
    },
    {
      valueComponent: ({ value, className }) => (
        <>
          {value.tags
            ? (value as IList).tags.map((tag) => (
                <span className={cx(className, styles.tag_name)} key={tag.id}>
                  {tag.name},
                </span>
              ))
            : ""}
        </>
      ),
      filterComponent: ({
        value,
        onChange,
        name,
        tagsEntities,
        isCountrySelected,
      }) => (
        <Select
          options={tagsEntities}
          placeholder={i18n.adminPanelConstructorList.name_tag_label}
          name={name}
          value={value}
          onChange={onChange}
          idSelector={idSelector}
          valueSelector={valueTags}
          disabled={!isCountrySelected}
        />
      ),
      isSortEnable: true,
      columnsRatio: "1fr",
      valueKey: "tagId",
    },
    {
      valueComponent: ({ value, className }) => (
        <p className={cx(className, styles.downloads_item)}>
          {(value as IList).downloads}
        </p>
      ),
      filterComponent: () => (
        <p className={styles.downloads}>
          {i18n.adminPanelConstructorList.downloads_label}
        </p>
      ),
      isSortEnable: true,
      columnsRatio: "150px",
      valueKey: "downloads",
    },
    {
      isSortEnable: false,
      valueComponent: ({ value, onClick }) => (
        <SettingsPopup
          value={value}
          name={PUBLISH}
          onClick={onClick}
          i18n={i18n}
        />
      ),
      columnsRatio: "50px",
      valueKey: "PUBLISH",
    },
    {
      isSortEnable: false,
      valueComponent: ({ onClick }) => (
        <div className={styles.link}>
          <Button.Icon
            view={Button.Icon.views.LINK}
            onClick={onClick}
            name={LINKS}
          />
        </div>
      ),
      columnsRatio: "50px",
      valueKey: "link",
    },
  ];
};
