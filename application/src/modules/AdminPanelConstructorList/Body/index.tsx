import React, {
  ChangeEventHandler,
  FC,
  useCallback,
  useEffect,
  useMemo,
} from "react";
import { Register, useModal } from "@package/components";
import { useSelector, useStore } from "react-redux";
import { templateItems } from "./template";
import {
  resourceSelectors,
  useCountriesResource,
  useI18n,
  useLanguagesResource,
} from "src/hooks";
import { ILinks, IList } from "../type";
import Header from "../Header";
import Footer from "../Footer";
import { useListResource } from "../hooks/useListResource";
import { LinksPopup } from "../Compotent";
import { useStatusesResource } from "../hooks/useStatusesResource";
import { useCategoriesResource, useTagsResource } from "../../AdminPanel/hooks";
import * as services from "src/service";
import { settings } from "src/store";
import { LINKS, PUBLISH } from "../constats";

import styles from "./Body.module.scss";

const DELETE = "delete";
const COPY = "copy";

const Body: FC = () => {
  const i18n = useI18n();
  const { dispatch, getState } = useStore();

  const languageId = useSelector(settings.selectors.getLanguage);

  const countriesResource = useCountriesResource();
  const languagesResource = useLanguagesResource();
  const listResource = useListResource();
  const statusResource = useStatusesResource();
  const categoriesResource = useCategoriesResource();
  const tagsResource = useTagsResource();

  const { entities: countries, onLoad: onLoadCountries } = countriesResource;
  const { entities: languages, onLoad: onLoadLanguages } = languagesResource;
  const { entities: statuses, onLoad: onLoadStatuses } = statusResource;
  const {
    entities: categories,
    onFilter: onFilterCategories,
    onReloadDebounced: onReloadDebouncedCategories,
  } = categoriesResource;
  const {
    entities: tags,
    onFilter: onFilterTags,
    onReloadDebounced: onReloadDebouncedTags,
  } = tagsResource;
  const {
    entities,
    filter,
    onFilterEventHandler,
    onSortEventHandler,
    sort,
    onLoadDebounced,
    onReload,
    onResetAndReload,
    store,
  } = listResource;

  useEffect(() => {
    onLoadCountries();
    onLoadStatuses();
    onLoadLanguages();
    onReload();
  }, []);

  const template = useMemo(() => templateItems(i18n), [i18n]);

  const onLinksChnaged = useCallback(async (entity: ILinks) => {
    const { id, value } = entity;
    await services.admin_list.updateLinksToTemplate(String(id), value);

    const state = getState();
    const essence = resourceSelectors.getEntries(state, store.name) as IList[];

    const linkEntity = essence.map((item) =>
      id === item.id
        ? {
            ...item,
            links: value,
          }
        : item
    );
    dispatch(store.actions.setEntities(linkEntity));
  }, []);

  const [fragment, onOpen] = useModal(LinksPopup);

  const onClickHandler = useCallback(
    async (info: IList, name: string | undefined) => {
      switch (name) {
        case PUBLISH:
          await services.admin_list.setStatusToTemplate(
            info.id,
            info.status.id === statuses[1].id ? statuses[2].id : statuses[1].id
          );
          onReload();
          break;

        case DELETE:
          await services.admin_list.removeTemplates(info.id);
          onReload();
          break;

        case COPY:
          await services.admin_list.copyTemplates(info.id);
          onReload();
          break;

        case LINKS:
          onOpen({
            value: info,
            store: store,
            onLinksChnaged: onLinksChnaged,
          });
          break;
      }
    },
    [onOpen, statuses, onLinksChnaged]
  );

  const onRegisterFilterChanged: ChangeEventHandler = useCallback(
    (event) => {
      onFilterEventHandler(event);

      const { value: changedFieldValue, name: changedFieldKey } =
        event.target as HTMLInputElement;
      if ("countryId" === changedFieldKey) {
        onFilterCategories(changedFieldKey, changedFieldValue);
        onFilterTags(changedFieldKey, changedFieldValue);
        onReloadDebouncedCategories();
        onReloadDebouncedTags();
      }
    },
    [onFilterCategories, onFilterEventHandler, onReloadDebouncedCategories]
  );

  return (
    <div className={styles.body}>
      <Header onClear={onResetAndReload} />
      <div className={styles.register}>
        <Register
          data={entities}
          template={template}
          filterValues={filter}
          onFilterChange={onRegisterFilterChanged}
          sortValues={sort}
          onSortChange={onSortEventHandler}
          onScrollDown={onLoadDebounced}
          onClickHandler={onClickHandler}
          // custom properties for template
          languageId={languageId}
          languagesEntities={languages}
          categoryEntities={categories}
          tagsEntities={tags}
          isCountrySelected={filter.countryId}
          countryEntities={countries}
          statusEntities={statuses}
        />
        {fragment}
      </div>
      <Footer listResource={store} />
    </div>
  );
};

export default Body;
