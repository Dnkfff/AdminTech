import React, { FC, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { Layout } from "src/components";
import adminDashboard from "../AdminDashboard";
import Body from "./Body";

const ConstructorList: FC = () => {
  const navigate = useNavigate();
  const onBack = useCallback(() => navigate(adminDashboard.path), [navigate]);

  return (
    <Layout
      onBack={onBack}
      headOptions={[Layout.headOptions.language, Layout.headOptions.profile]}
      fitToWindow
    >
      <Body />
    </Layout>
  );
};

export default ConstructorList;
