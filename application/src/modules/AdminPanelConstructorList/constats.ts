export const LINKS = "LINKS";
export const PUBLISH = "PUBLISH";

const DMY = "dd.mm.yyyy";
const YMD = "yyyy-mm-dd";

export const formatDate = (date: Date, format: string) => {
  switch (format) {
    case DMY:
      const dateDmy = date.toISOString().slice(0, 10);
      return dateDmy.split("-").reverse().join(".");
    case YMD:
      return date.toISOString().slice(0, 10);
  }
};
