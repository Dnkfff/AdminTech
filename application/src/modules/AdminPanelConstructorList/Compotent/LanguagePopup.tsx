import React, { FC, memo } from "react";
import { Button, Icon, Popup } from "@package/components";

import styles from "./LanguagePopup.module.scss";

interface IHeader {
  value: any;
  onChange: any;
  name: string;
  languagesEntities: any[];
}

const LanguagePopup: FC<IHeader> = ({
  value,
  onChange,
  name,
  languagesEntities,
}) => {
  return (
    <Popup
      content={
        <Button.Icon
          view={Icon.views.EARTH}
          onClick={onChange}
          name={String(name)}
          value={value}
        />
      }
      placement="bottom"
      className={styles.button_earth}
    >
      <div className={styles.countries_options}>
        {languagesEntities.map((language) => {
          return (
            <button
              className={styles.countries_option}
              key={language.id}
              name={name}
              value={language.id}
              onClick={onChange}
            >
              {language.code}
            </button>
          );
        })}
      </div>
    </Popup>
  );
};

export default memo(LanguagePopup);
