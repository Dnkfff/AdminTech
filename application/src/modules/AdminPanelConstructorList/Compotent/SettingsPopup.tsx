import React, { FC, memo } from "react";
import { Button, Icon, Popup, Switch } from "@package/components";
import { useI18n } from "src/hooks";

import styles from "./SettingsPopup.module.scss";

const PUBLISHED_STATUS = "Published";

interface IHeader {
  value: any;
  onClick: any;
  name: string | number;
  i18n: ReturnType<typeof useI18n>;
}

const SettingsPopup: FC<IHeader> = ({ value, onClick, name, i18n }) => {
  return (
    <Popup
      className={styles.template__icon}
      content={<Icon view={Icon.views.ADMIN_LIST_SETTINGS} />}
      placement="left"
    >
      <div className={styles.popup_publish}>
        <h5 className={styles.title}>
          {i18n.adminPanelConstructorList.publish_label}
        </h5>
        <div className={styles.switch}>
          <Switch
            name={String(name)}
            value={value.status.name?.EN === PUBLISHED_STATUS}
            onChange={onClick}
          />
        </div>
        <Button
          className={styles.button}
          onClick={onClick}
          name="delete"
          color={Button.colors.DarkBlueGrey}
          border={Button.borders.DarkBlueGrey}
          type={Button.types.Rounded}
        >
          {i18n.adminPanelConstructorList.delete_label}
        </Button>
        <Button
          className={styles.button}
          onClick={onClick}
          name="copy"
          color={Button.colors.DarkBlueGrey}
          border={Button.borders.DarkBlueGrey}
          type={Button.types.Rounded}
        >
          {i18n.adminPanelConstructorList.copy_label}
        </Button>
      </div>
    </Popup>
  );
};

export default memo(SettingsPopup);
