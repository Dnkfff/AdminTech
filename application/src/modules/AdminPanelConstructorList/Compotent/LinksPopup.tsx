import React, { FC, useCallback, useState } from "react";
import { Select } from "@package/components";
import { useI18n } from "src/hooks";
import { useStore } from "react-redux";
import { ILinks, IList } from "../type";
import { selectors } from "../../../hooks/useResource/useResource";

import styles from "./LinksPopup.module.scss";

interface IHeader {
  store: any;
  value: any;
  onLinksChnaged: (entity: ILinks) => void;
}

export const idLinks = (option: any) => option?.id;
export const valueLinks = (option: any) => option?.documentName;

const LinksPopup: FC<IHeader> = ({ value, store, onLinksChnaged }) => {
  const [ref, setRef] = React.useState<HTMLDivElement | null>(null);

  const [link, setLink] = useState(value.links);

  const i18n = useI18n();

  const { getState } = useStore();
  const entities = selectors.getEntries(getState(), store.name) as IList[];

  const onChange = useCallback((item: any) => {
    setLink(item.target.value);
    onLinksChnaged({ id: value.id, value: item.target.value });
  }, []);

  return (
    <div
      className={styles.popup_links}
      ref={useCallback((e: HTMLDivElement) => setRef(e), [])}
    >
      <div className={styles.title}>
        <h3 className={styles.title_links}>
          {i18n.adminPanelConstructorList.links_label}
        </h3>
        <span className={styles.document_name}>{value.documentName}</span>
      </div>
      <Select.Multi
        boundaries={ref}
        options={entities}
        value={link}
        idSelector={idLinks}
        valueSelector={valueLinks}
        onChange={onChange}
      />
    </div>
  );
};

export default LinksPopup;
