import React, { lazy } from "react";

const Component = lazy(() => import("./AdminConstructorList"));

export default {
  path: "/admin-constructor-list",
  element: <Component />,
};
