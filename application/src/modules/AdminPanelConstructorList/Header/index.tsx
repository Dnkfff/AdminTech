import React, { FC, memo } from "react";
import { always, Button, Icon } from "@package/components";
import { useI18n } from "src/hooks";

import styles from "./Header.module.scss";

interface IHeader {
  onClear: () => void;
}

const Header: FC<IHeader> = ({ onClear }) => {
  const i18n = useI18n();
  return (
    <div className={styles.navigate_register}>
      <Button
        className={styles.clear_filter}
        onClick={onClear}
        value="value"
        name="key"
        fill={Button.colors.HavelockBlue}
        color={Button.colors.White}
        border={Button.colors.HavelockBlue}
        type={Button.types.Rounded}
      >
        {i18n.adminPanelConstructorList.clear_all_filters}
      </Button>
      <Button.Icon
        className={styles.link_constructor}
        view={Icon.views.CIRCLED_PLUS}
        onClick={always.EMPTY_FUNC}
      />
    </div>
  );
};

export default memo(Header);
