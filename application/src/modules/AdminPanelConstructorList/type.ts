export type IList = {
  id: string;
  country: {
    id: number;
    name: Record<string, string>;
  };
  category: {
    id: number;
    name: Record<string, string>;
  };
  language: {
    id: number;
    code: string;
  };
  status: {
    id: number;
    name: Record<string, string>;
  };
  tags: {
    id: number;
    name: string;
  }[];
  updated: string;
  downloads: number;
  links: string[];
  documentName: string;
  userId: string;
};

export type IStatuses = {
  id: number;
  name: Record<string, string>;
};

export type ILinks = {
  id: string;
  value: string[];
};
