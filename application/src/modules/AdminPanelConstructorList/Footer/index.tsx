import React, { FC, memo } from "react";
import { useSelector } from "react-redux";
import { selectors } from "../../../hooks/useResource/useResource";

import styles from "./Footer.module.scss";

interface IFooter {
  listResource: any;
}

const Footer: FC<IFooter> = ({ listResource }) => {
  const entities: any[] = useSelector((state) =>
    selectors.getEntries(state, listResource.name)
  );

  return (
    <div className={styles.footer}>
      <div className={styles.entities_length}>{(entities ?? []).length}</div>
    </div>
  );
};

export default memo(Footer);
