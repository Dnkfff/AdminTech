export type IUser = {
  role: string;
  id: number;
  email: string;
  name: string;
  language: string;
  countryId: number;
  roleId: number;
  type: number;
  password: string;
};

export type IUserModal = {
  id: number;
  type: number;
  name: string;
  email: string;
  password: string;
};

export type IUserRole = {
  id: number;
  short: Record<string, string>;
  countryId: number;
};
