/* eslint-disable @typescript-eslint/no-explicit-any */
import React, {
  FC,
  useEffect,
  useCallback,
  ComponentProps,
  useMemo,
} from "react";
import { useStore } from "react-redux";
import {
  Register,
  Icon,
  Input,
  Select,
  Button,
  Popup,
  useModal,
} from "@package/components";
import * as services from "src/service";
import { resourceSelectors } from "src/hooks";
import { IUser, IUserModal } from "./type";
import EditModal, {
  defaultIdSelector,
  defaultValueSelector,
} from "./UsersModal";
import {
  useUsersResource,
  useLocationFormValues,
  useUsersRolesResource,
} from "../hooks";

import styles from "./Users.module.scss";

const EDIT = "EDIT";
const DELETE = "DELETE";

const template: ComponentProps<typeof Register>["template"] = [
  {
    columnsRatio: "0.6fr",
    valueKey: "type",
    valueComponent: ({ value, className, dataForTypeUserColumn }) => (
      <p className={className}>
        {dataForTypeUserColumn.get((value as IUser).roleId)?.short.EN || ""}
      </p>
    ),
    filterComponent: ({ value, onChange, name, dataForTypeUserSelect }) => (
      <Select
        options={dataForTypeUserSelect}
        placeholder="Type"
        value={value}
        onChange={onChange}
        name={name}
        idSelector={defaultIdSelector}
        valueSelector={defaultValueSelector}
      />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "1fr",
    valueKey: "name",
    valueComponent: ({ value, className }) => (
      <p className={className}> {(value as IUser).name} </p>
    ),
    filterComponent: ({ value, onChange, name }: any) => (
      <Input placeholder="name" value={value} onChange={onChange} name={name} />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "1.8fr",
    valueKey: "email",
    valueComponent: ({ value, className }) => (
      <p className={className}> {(value as IUser).email} </p>
    ),
    filterComponent: ({ value, onChange, name }: any) => (
      <Input
        placeholder="email"
        value={value}
        onChange={onChange}
        name={name}
      />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "0.3fr",
    valueKey: "ellipsis",
    filterComponent: () => <></>,
    valueComponent: ({ className, onClick }) => (
      <Popup
        content={<Icon view={Button.Icon.views.THREE_DOTS} />}
        className={className}
      >
        <Button
          border={Button.borders.Transparent}
          color={Button.colors.HavelockBlue}
          className={styles.popup_button}
          name={EDIT}
          onClick={onClick}
        >
          Edit
        </Button>
        <Button
          border={Button.borders.Transparent}
          color={Button.colors.Coral}
          className={styles.popup_button}
          name={DELETE}
          onClick={onClick}
        >
          Delete
        </Button>
      </Popup>
    ),
    isSortEnable: false,
  },
];

const Users: FC = () => {
  const { dispatch, getState } = useStore();
  const {
    entities,
    filter,
    sort,
    store,
    onFilter,
    onFilterEventHandler,
    onSortEventHandler,
    onReload,
    onLoadDebounced,
  } = useUsersResource();

  const {
    entities: usersRoles,
    store: usersRolesStore,
    onFilter: onUserRolesFilter,
    onReload: onUsersRolesReload,
  } = useUsersRolesResource();

  const { countryId } = useLocationFormValues();

  const usersRoleMap = useMemo(
    () => new Map(usersRoles.map((item) => [item.id, item])),
    [usersRoles]
  );

  useEffect(() => {
    onFilter("countryId", countryId);
    onReload();
    onUserRolesFilter("countryId", countryId);
    onUsersRolesReload();
  }, [countryId]);

  const onEdit = useCallback(
    async (entity: IUserModal) => {
      const state = getState();
      const entities = resourceSelectors.getEntries(
        state,
        store.name
      ) as IUser[];
      const { data } = await services.admin_users.updateUser(
        String(entity.id),
        entity
      );
      const _entities = entities.map((item) =>
        String(item.id) == String(data.message.id) ? data.message : item
      );
      dispatch(store.actions.setEntities(_entities));
    },
    [store, dispatch, getState]
  );

  const onCreate = useCallback(
    async (entity: IUserModal) => {
      const { data } = await services.admin_users.createUser(countryId, entity);
      dispatch(store.actions.mergeEntities([data.message]));
    },
    [store, dispatch]
  );

  const onDelete = useCallback(
    async (entity: IUser) => {
      const state = getState();
      const entities = resourceSelectors.getEntries(
        state,
        store.name
      ) as IUser[];
      const _entities = entities.filter(
        ({ id }) => String(entity.id) !== String(id)
      );
      await services.admin_users.deleteUser(String(entity.id));
      dispatch(store.actions.setEntities(_entities));
    },
    [store, dispatch]
  );

  const [fragment, onOpen, onClose] = useModal(EditModal);

  const cellCallbackDelegate = useCallback(
    (entity: IUser, name = "") => {
      switch (name) {
        case EDIT: {
          return onOpen({
            onSave: onEdit,
            onClose,
            state: {
              id: entity.id,
              type: entity.type,
              name: entity.name,
              email: entity.email,
              password: entity.password,
              countryId: entity.countryId,
            },
            store: usersRolesStore,
          });
        }
        case DELETE:
          return onDelete(entity);
      }
    },
    [onEdit, onClose, onDelete]
  );

  const onAdd = useCallback(() => {
    return onOpen({
      onSave: onCreate,
      onClose,
      store: usersRolesStore,
    });
  }, [onCreate, onClose]);

  return (
    <div className={styles.content}>
      <div className={styles.actions}>
        <Button.Icon view={Button.Icon.views.CIRCLED_PLUS} onClick={onAdd} />
      </div>
      {fragment}
      <div className={styles.register}>
        <Register
          data={entities}
          template={template}
          filterValues={filter}
          sortValues={sort}
          onFilterChange={onFilterEventHandler}
          onSortChange={onSortEventHandler}
          onScrollDown={onLoadDebounced}
          onClickHandler={cellCallbackDelegate}
          dataForTypeUserColumn={usersRoleMap}
          dataForTypeUserSelect={usersRoles}
        />
      </div>
    </div>
  );
};

export default Users;
