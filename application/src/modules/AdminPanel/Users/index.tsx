import React, { lazy } from "react";

const Component = lazy(() => import("./Users"));

export default {
  path: "/users",
  element: <Component />,
}
