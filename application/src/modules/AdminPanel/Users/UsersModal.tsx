import React, { FC } from "react";
import { useSelector } from "react-redux";
import { Button, Input, Select } from "@package/components";
import { resourceSelectors, useForm, useFormChange, useI18n } from "src/hooks";
import { useUsersRolesResource } from "../hooks";
import { IUserModal, IUserRole } from "./type";

import styles from "./UsersModal.module.scss";

export const defaultIdSelector = (option: IUserRole) => option?.id;
export const defaultValueSelector = (option: IUserRole) => option?.short.EN;

const initialState = {
  id: -1,
  type: -1,
  name: "",
  email: "",
  password: "",
  countryId: -1,
};

type IEditModal = {
  state?: typeof initialState;
  onClose: () => void;
  onSave: (entity: IUserModal) => void;
  store: ReturnType<typeof useUsersRolesResource>["store"];
};

const EditModal: FC<IEditModal> = ({
  state = initialState,
  store: usersRolesStore,
  onSave: onSaveCallback,
  onClose,
}) => {
  const i18n = useI18n();

  const usersRolesEntities = useSelector((state) =>
    resourceSelectors.getEntries(state, usersRolesStore.name)
  );

  const ref = React.useRef(null);

  const { values, onChange, onSave } = useForm("admin-panel-user-form", state, {
    saver: async (toSave) => {
      const normalizedValue = {
        id: toSave.id,
        type: toSave.type,
        name: toSave.name,
        email: toSave.email,
        password: toSave.password,
      };
      await onSaveCallback(normalizedValue);
      onClose && onClose();
      return { data: toSave };
    },
  });

  const input = useFormChange(values, onChange);

  return (
    <>
      <div className={styles.container} ref={ref}>
        <div className={styles.form}>
          <Select
            {...input.input("type")}
            boundaries={ref.current}
            options={usersRolesEntities}
            placeholder={i18n.adminPanel.type}
            idSelector={defaultIdSelector}
            valueSelector={defaultValueSelector}
          />
          <Input
            {...input.input("name")}
            className={styles.input}
            placeholder={i18n.adminPanel.name}
          />
          <Input
            {...input.input("email")}
            className={styles.input}
            placeholder={i18n.adminPanel.email}
            type="email"
          />
          <Input
            {...input.input("password")}
            className={styles.input}
            placeholder={i18n.adminPanel.password}
          />
        </div>
        <div className={styles.footer}>
          <Button className={styles.button} onClick={onClose}>
            {i18n.common.cancel}
          </Button>
          <Button className={styles.button} onClick={onSave}>
            {i18n.common.save}
          </Button>
        </div>
      </div>
    </>
  );
};

export default EditModal;
