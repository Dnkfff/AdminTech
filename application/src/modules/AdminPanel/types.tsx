type ID = string | number;
export type IAdminPanel = {
  id: ID;
  idOptions: ID;
};
