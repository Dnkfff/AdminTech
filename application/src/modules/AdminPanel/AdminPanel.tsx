import React, { useCallback, useMemo, Suspense } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Layout } from "src/components";
import { useI18n } from "src/hooks";
import { Tabs, always } from "@package/components";
import Location from "./Location";
import { ITranslationValue } from "src/i18n";
import adminDashboard from "../AdminDashboard";
import users from "./Users";
import dropdowns from "./Dropdowns";
import categories from "./Categories";
import tags from "./Tags";

import styles from "./AdminPanel.module.scss";

const tabItems = (i18n: ITranslationValue) => {
  return [
    {
      label: i18n.adminPanel.users_label,
      key: users.path.replace("/", ""),
      component: users.element,
    },
    {
      label: i18n.adminPanel.category_label,
      key: categories.path.replace("/", ""),
      component: categories.element,
    },
    {
      label: i18n.adminPanel.dropdowns_label,
      key: dropdowns.path.replace("/", ""),
      component: dropdowns.element,
    },
    {
      label: i18n.adminPanel.tags_label,
      key: tags.path.replace("/", ""),
      component: tags.element,
    },
  ];
};

const AdminPanel = () => {
  const navigate = useNavigate();
  const onBack = useCallback(() => navigate(adminDashboard.path), [navigate]);

  const i18n = useI18n();

  const tabsTemplate = useMemo(() => tabItems(i18n), [i18n]);

  const { tab } = useParams();

  const onClickTabs = (tab: string | number) => navigate("./" + tab);

  return (
    <Layout headOptions={always.EMPTY_ARRAY as []} onBack={onBack} fitToWindow>
      <Location>
        <Suspense fallback="Loading ...">
          <div className={styles.tabs}>
            <Tabs.Stateless
              items={tabsTemplate}
              currentTab={tab}
              onChange={onClickTabs}
            />
          </div>
        </Suspense>
      </Location>
    </Layout>
  );
};

export default AdminPanel;
