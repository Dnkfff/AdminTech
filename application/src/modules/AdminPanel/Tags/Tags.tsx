import React, { ChangeEventHandler, FC, useCallback, useEffect } from "react";
import { always, Select } from "@package/components";
import { useLocationFormValues, useTagsResource } from "../hooks";
import { ITag } from "./types";
import { useForm, useFormChange, resourceSelectors } from "src/hooks";
import { useStore } from "react-redux";
import * as services from "src/service";

import styles from "./Tags.module.scss";

const PREFIX_CREATE = "Create: ";
const valueSelector = ({ nameTag }: ITag) => nameTag;
const idSelector = ({ id }: ITag) => id;
const creatableSelector = (query: string) => ({
  id: -1,
  nameTag: `${PREFIX_CREATE}${query}`,
});

const initialValues = { tags: [] as number[] };

const Tags: FC = () => {
  const { getState, dispatch } = useStore();
  const { entities, onReload, onFilter, store: tagsStore } = useTagsResource();
  const { countryId } = useLocationFormValues();

  const {
    values,
    onChange,
    store: formStore,
  } = useForm<typeof initialValues>(
    "tags-form",
    initialValues,
    always.EMPTY_OBJECT
  );

  const input = useFormChange(values, onChange);

  useEffect(() => {
    const run = async () => {
      onFilter("countryId", countryId);
      await onReload();
      const state = getState();
      const _entities = resourceSelectors.getEntries(
        state,
        tagsStore.name
      ) as ITag[];
      const update = { tags: _entities.map(({ id }) => id) };
      dispatch(formStore.actions.setValues(update));
      dispatch(formStore.actions.setValuesSnapshot(update));
    };
    run();
  }, [dispatch, countryId, getState]);

  const onRemove = useCallback(
    async (id: string) => {
      await services.admin_tags.removeTag(id);

      const state = getState();
      const _entities = resourceSelectors.getEntries(
        state,
        tagsStore.name
      ) as ITag[];
      const filteredEntities = _entities.filter(
        ({ id: e_id }) => String(id) !== String(e_id)
      );
      dispatch(tagsStore.actions.setEntities(filteredEntities));

      const update = {
        tags: filteredEntities.map(({ id }) => id),
      };
      dispatch(formStore.actions.setValues(update));
      dispatch(formStore.actions.setValuesSnapshot(update));
    },
    [countryId]
  );

  const onUpdate = useCallback(
    async (nameTag: string) => {
      const { data } = await services.admin_tags.createTag(countryId, {
        nameTag,
      });
      dispatch(tagsStore.actions.mergeEntities([data.message]));
      const state = getState();
      const _entities = resourceSelectors.getEntries(
        state,
        tagsStore.name
      ) as ITag[];
      const update = { tags: _entities.map(({ id }) => id) };
      dispatch(formStore.actions.setValues(update));
      dispatch(formStore.actions.setValuesSnapshot(update));
    },
    [countryId]
  );

  const onChangeHandler: ChangeEventHandler<HTMLInputElement> = useCallback(
    (event) => {
      const { value, id, title } = event.target;
      if (value.includes(id)) {
        const title_to_save = title?.replace(PREFIX_CREATE, "");
        onUpdate(title_to_save);
      } else {
        onRemove(id);
      }
    },
    [onUpdate, onRemove]
  );

  return (
    <>
      <div className={styles.tags}>
        <Select.Multi
          {...input.input("tags")}
          onChange={onChangeHandler}
          options={entities}
          valueSelector={valueSelector}
          idSelector={idSelector}
          creatable
          creatableSelector={creatableSelector}
        />
      </div>
    </>
  );
};

export default Tags;
