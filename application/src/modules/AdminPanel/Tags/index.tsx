import React, { lazy } from "react";

const Component = lazy(() => import("./Tags"));

export default {
  path: "/tags",
  element: <Component />,
};
