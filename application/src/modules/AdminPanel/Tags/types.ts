export type ITag = {
  id: number;
  countryId: number;
  nameTag: string;
};

export type ITagCategories = {
  id: number;
  name_tag: string;
  country_id: number;
};
