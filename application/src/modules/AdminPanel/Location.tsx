import React, { FC, useEffect, ChangeEvent, useMemo, useCallback } from "react";
import { Select } from "@package/components";
import {
  useCountriesResource,
  useI18n,
  useFormChange,
  useLanguagesResource,
  useCountryLanguageResource,
} from "src/hooks";
import { useLocationForm } from "./hooks";

import styles from "./Location.module.scss";

const countryIdSelector = (option: any) => option?.countryId;
const languageIdSelector = (option: any) => option?.code;
const languageValueSelector = (option: any) => option?.name;

const Location: FC<{ children: React.ReactNode }> = ({ children }) => {
  const i18n = useI18n();

  const countriesResource = useCountriesResource();
  const { entities: countries, onReloadLazy: onReloadLazyCountries } =
    countriesResource;

  const countryLanguageResource = useCountryLanguageResource();
  const {
    onReload: onReloadCountryLanguage,
    store: countryLanguageStore,
    entities: countryLanguages,
  } = countryLanguageResource;

  const languagesResource = useLanguagesResource();
  const { entities: languages, onReloadLazy: onReloadLazyLanguages } =
    languagesResource;

  const { values, onChange, onInitialize } =
    useLocationForm(countryLanguageStore);

  useEffect(() => {
    const f = async () => {
      await onReloadLazyCountries();
      await onReloadLazyLanguages();
      await onReloadCountryLanguage();
      await onInitialize();
    };
    f();
  }, []);

  const countriesMap = useMemo(
    () => new Map(countries.map((item) => [item.code, item])),
    [countries]
  );

  const countriesLanguageMap = useMemo(
    () => new Map(countryLanguages.map((item) => [item.countryId, item])),
    [countryLanguages]
  );

  const countryValueSelector = useCallback(
    ({ codeCountry }: any) => countriesMap.get(codeCountry)?.name || "",
    [countriesMap]
  );

  const { countryId } = values;

  const filteredLanguages = useMemo(() => {
    const children = countriesLanguageMap.get(countryId)?.children;
    return languages.filter(({ code }) => children?.includes(code));
  }, [countriesLanguageMap, countryId, languages]);

  const input = useFormChange(values, onChange);

  const onCountryChange = ({
    target: { value, name },
  }: {
    target: { value: string; name: string };
  }) => {
    onChange({
      target: {
        name,
        value: {
          countryId: value,
          languageId: countriesLanguageMap.get(value)?.children[0],
        },
      },
    } as unknown as ChangeEvent<HTMLInputElement>);
  };

  return (
    <>
      <div className={styles.location}>
        <div className={styles.item}>
          <Select
            options={countryLanguages}
            placeholder={i18n.common.select}
            {...input.input("countryId")}
            onChange={onCountryChange}
            idSelector={countryIdSelector}
            valueSelector={countryValueSelector}
          />
        </div>

        <div className={styles.item}>
          <Select
            options={filteredLanguages}
            placeholder={i18n.common.select}
            {...input.input("languageId")}
            idSelector={languageIdSelector}
            valueSelector={languageValueSelector}
          />
        </div>
      </div>
      {countryId && children}
    </>
  );
};

export default Location;
