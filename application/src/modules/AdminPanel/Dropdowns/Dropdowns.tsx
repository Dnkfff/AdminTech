import React, { ComponentProps, FC, useCallback, useEffect } from "react";
import {
  Register,
  Input,
  useModal,
  Button,
  Icon,
  Popup,
} from "@package/components";
import { useDropdownsResource, useLocationFormValues } from "../hooks";
import { resourceSelectors } from "src/hooks";
import { IDropdown } from "./types";
import EditModal from "./EditModal";
import { useStore } from "react-redux";
import * as services from "src/service";

import styles from "./Dropdowns.module.scss";

const EDIT = "EDIT";
const DELETE = "DELETE";

const template: ComponentProps<typeof Register>["template"] = [
  {
    columnsRatio: "1fr",
    valueKey: "name",
    valueComponent: ({ value, className, languageId }) => (
      <p className={className}> {(value as IDropdown)?.name?.[languageId]} </p>
    ),
    filterComponent: ({ value, onChange, name }) => (
      <Input placeholder="Name" value={value} onChange={onChange} name={name} />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "2fr",
    valueKey: "opt",
    valueComponent: ({ value, className, languageId }) => (
      <p className={className}>{(value as IDropdown)?.options?.[languageId]}</p>
    ),
    filterComponent: ({ value, onChange, name }) => (
      <Input
        placeholder="Options"
        value={value}
        onChange={onChange}
        name={name}
      />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "60px",
    valueKey: "options",
    valueComponent: ({ className, onClick }) => (
      <Popup
        content={<Icon view={Button.Icon.views.THREE_DOTS} />}
        className={className}
      >
        <Button
          border={Button.borders.Transparent}
          color={Button.colors.HavelockBlue}
          name={EDIT}
          onClick={onClick}
          className={styles.popover_button}
        >
          Edit
        </Button>
        <Button
          border={Button.borders.Transparent}
          color={Button.colors.Coral}
          name={DELETE}
          onClick={onClick}
          className={styles.popover_button}
        >
          Delete
        </Button>
      </Popup>
    ),
  },
];

const Dropdowns: FC = () => {
  const { dispatch, getState } = useStore();
  const {
    entities,
    filter,
    onFilter,
    onFilterEventHandler,
    onSortEventHandler,
    sort,
    onReload,
    onLoadDebounced,
    store,
  } = useDropdownsResource();

  const { countryId, languageId } = useLocationFormValues();

  useEffect(() => {
    onFilter("countryId", countryId);
    onReload();
  }, [countryId]);

  const onCreate = useCallback(
    async (entity: IDropdown) => {
      const { data } = await services.admin_dropdowns.createDropdown(
        countryId,
        entity
      );
      dispatch(store.actions.mergeEntities([data.message as IDropdown]));
    },
    [store, dispatch]
  );

  const onEdit = useCallback(
    async (entity: IDropdown) => {
      const state = getState();
      const entities = resourceSelectors.getEntries(
        state,
        store.name
      ) as IDropdown[];
      const { data } = await services.admin_dropdowns.updateDropdown(
        String(entity.id),
        entity
      );
      const _entities = entities.map((item) =>
        String(item.id) == String(data.message.id) ? data.message : item
      );
      dispatch(store.actions.setEntities(_entities));
    },
    [store, dispatch, getState]
  );

  const onDelete = useCallback(
    async (entity: IDropdown) => {
      const state = getState();
      const entities = resourceSelectors.getEntries(
        state,
        store.name
      ) as IDropdown[];
      const _entities = entities.filter(
        ({ id }) => String(entity.id) !== String(id)
      );
      await services.admin_dropdowns.deleteDropdown(String(entity.id));
      dispatch(store.actions.setEntities(_entities));
    },
    [store, dispatch]
  );

  const [fragment, onOpen, onClose] = useModal(EditModal);

  const cellCallbackDelegate = useCallback(
    (entity: IDropdown, name = "") => {
      switch (name) {
        case EDIT: {
          return onOpen({
            onSave: onEdit,
            onClose,
            state: entity,
            languageId,
          });
        }
        case DELETE:
          return onDelete(entity);
      }
    },
    [onEdit, onClose, onDelete, languageId]
  );

  return (
    <div className={styles.content}>
      <div className={styles.menu}>
        <Button.Icon
          view={Button.Icon.views.CIRCLED_PLUS}
          onClick={() => onOpen({ onSave: onCreate, onClose, languageId })}
        />
      </div>
      {fragment}
      <div className={styles.register}>
        <Register
          data={entities}
          template={template}
          filterValues={filter}
          onFilterChange={onFilterEventHandler}
          sortValues={sort}
          onSortChange={onSortEventHandler}
          onScrollDown={onLoadDebounced}
          onClickHandler={cellCallbackDelegate}
          languageId={languageId}
        />
      </div>
    </div>
  );
};

export default Dropdowns;
