export type IDropdown = {
  countryId: number;
  id: number;
  name: Record<string, string>;
  options: Record<string, string>;
};

export type IDropdownCreate = {
  countryId: number;
  id: number;
  name: string;
  options: Record<string, string>;
};
