import React, {
  ChangeEvent,
  ChangeEventHandler,
  FC,
  MouseEventHandler,
  useCallback,
  useMemo,
} from "react";
import { useForm, useFormChange } from "../../../hooks";
import { Button, Icon, Input, useEvent } from "@package/components";
import styles from "./EditModal.module.scss";
import { useStore } from "react-redux";
import { IDropdown, IDropdownCreate } from "./types";

type IOptions = {
  value: IDropdownCreate["options"];
  onChange: ChangeEventHandler;
  name: string;
};

const GET_UNIQUE_ID = () => "option_" + Math.random() * 100000000000000000;

const Options: FC<IOptions> = ({ value, onChange, name }) => {
  const keys = Object.keys(value || {});

  const input = useFormChange(value, onChange, name);

  const onRemove: MouseEventHandler<HTMLButtonElement> = useEvent((event) => {
    const { value: id } = event.target as HTMLButtonElement;
    const options = { ...value };
    delete options[id];
    onChange({
      target: { value: options, name },
    } as unknown as ChangeEvent<HTMLInputElement>);
  });

  return (
    <>
      {keys.map((key) => {
        return (
          <span className={styles.wrapper} key={key}>
            <Input
              {...input.input(key)}
              className={styles.input}
              placeholder="Option"
            />
            <Button.Icon
              view={Icon.views.CROSS}
              onClick={onRemove}
              value={key}
              className={styles.icon}
            />
          </span>
        );
      })}
    </>
  );
};

const getInitialState = (languageId: string): IDropdown => ({
  countryId: -1,
  id: -1,
  name: { [languageId]: "" } as Record<string, string>,
  options: { [languageId]: "" } as Record<string, string>,
});

type IEditModal = {
  state?: IDropdown;
  onClose: () => void;
  onSave: (entity: IDropdown) => void;
  languageId: string;
};

const EditModal: FC<IEditModal> = ({
  onSave: onSaveCallback,
  languageId,
  state: _state = getInitialState(languageId),
  onClose,
}) => {
  const { dispatch, getState } = useStore();

  const state: IDropdownCreate = useMemo(
    () => ({
      id: _state?.id,
      name: _state?.name?.[languageId] ?? "",
      options: Object.fromEntries(
        (_state?.options?.[languageId] ?? "")
          .split(",")
          .map((_v, index) => ["option_" + index, _v])
      ),
      countryId: _state.countryId,
    }),
    [_state, languageId]
  );

  const { values, onChange, onSave, store } = useForm<IDropdownCreate>(
    "admin-panel-form",
    state,
    {
      saver: async (toSave) => {
        const options = Object.values(toSave.options).join(", ");
        const normalizedValue: IDropdown = {
          countryId: toSave.countryId,
          id: toSave.id,
          name: { ..._state.name, [languageId]: toSave.name },
          options: { ..._state.options, [languageId]: options },
        };
        await onSaveCallback(normalizedValue);
        onClose && onClose();
        return Promise.resolve({ data: toSave });
      },
    }
  );

  const input = useFormChange(values, onChange);

  const onAppend = useCallback(() => {
    const state = getState();
    const prevValues = useForm.selectors.getValues(state, store.name);
    dispatch(
      store.actions.setValues({
        ...prevValues,
        options: {
          ...prevValues.options,
          [GET_UNIQUE_ID()]: "",
        },
      })
    );
  }, [dispatch, store]);

  return (
    <>
      <div className={styles.container}>
        <Input
          {...input.input("name")}
          className={styles.input}
          placeholder="Name"
        />

        <Options {...input.input("options")} />

        <Button.Icon
          view={Icon.views.CIRCLED_PLUS}
          onClick={onAppend}
          className={styles.plus_icon}
        />

        <div className={styles.footer}>
          <Button className={styles.button} onClick={onClose}>
            Cancel
          </Button>
          <Button className={styles.button} onClick={onSave}>
            Save
          </Button>
        </div>
      </div>
    </>
  );
};

export default EditModal;
