import React, { lazy } from "react";

const Component = lazy(() => import("./Dropdowns"));

export default {
  path: "/dropdowns",
  element: <Component />,
};
