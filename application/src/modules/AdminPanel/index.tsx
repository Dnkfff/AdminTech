import React, { lazy } from "react";

const Component = lazy(() => import("./AdminPanel"));

export default {
  path: "/admin-panel",
  element: <Component />,
  children: [
    {
      path: "/admin-panel/:tab",
      element: <Component />,
    },
  ],
};
