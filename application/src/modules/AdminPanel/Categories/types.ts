export type ICategory = {
  countryId: number;
  id: number;
  name: Record<string, string>;
  numList: number;
  tags: {
    id: number;
    name: string;
  }[];
};

export type ICategoryModal = {
  id: number;
  name: Record<string, string>;
  numList: string;
  tagsId: number[];
};
