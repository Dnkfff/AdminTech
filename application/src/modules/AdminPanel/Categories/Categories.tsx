import React, { FC, useCallback, useEffect, ComponentProps } from "react";
import { useStore } from "react-redux";
import {
  useModal,
  Register,
  Icon,
  Input,
  Button,
  Popup,
} from "@package/components";
import * as services from "src/service";
import { resourceSelectors } from "src/hooks";
import { useCategoriesResource, useLocationFormValues } from "../hooks";
import EditModal from "./EditModal";
import { ICategory, ICategoryModal } from "./types";
import { ITagCategories } from "../Tags/types";

import styles from "./Categories.module.scss";

const EDIT = "EDIT";
const DELETE = "DELETE";

const template: ComponentProps<typeof Register>["template"] = [
  {
    columnsRatio: "0.6fr",
    valueKey: "pos",
    valueComponent: ({ value, className }) => (
      <p className={className}> {(value as ICategory).numList} </p>
    ),
    filterComponent: ({ value, onChange, name }) => (
      <Input
        placeholder="position"
        value={value}
        onChange={onChange}
        name={name}
      />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "0.8fr",
    valueKey: "name",
    valueComponent: ({ value, className }) => (
      <p className={className}> {(value as ICategory).name.EN} </p>
    ),
    filterComponent: ({ value, onChange, name }) => (
      <Input placeholder="name" value={value} onChange={onChange} name={name} />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "2fr",
    valueKey: "tag",
    valueComponent: ({ value, className }) => (
      <>
        {value.tags
          ? (value as ICategory).tags.map((tag) => (
              <span className={className} key={tag.id}>
                {tag.name}
              </span>
            ))
          : ""}
      </>
    ),
    filterComponent: ({ value, onChange, name }) => (
      <Input
        placeholder="default tags"
        value={value}
        onChange={onChange}
        name={name}
      />
    ),
    isSortEnable: true,
  },
  {
    columnsRatio: "0.5fr",
    valueKey: "ellipsis",
    filterComponent: () => <></>,
    valueComponent: ({ value, className, onClick }) => (
      <Popup
        content={<Icon view={Button.Icon.views.THREE_DOTS} />}
        className={className}
      >
        <Button
          border={Button.borders.Transparent}
          color={Button.colors.HavelockBlue}
          className={styles.popup_button}
          name={EDIT}
          onClick={onClick}
        >
          Edit
        </Button>
        <Button
          border={Button.borders.Transparent}
          color={Button.colors.Coral}
          className={styles.popup_button}
          name={DELETE}
          onClick={onClick}
        >
          Delete
        </Button>
      </Popup>
    ),
    isSortEnable: false,
  },
];

const Categories: FC = () => {
  const { dispatch, getState } = useStore();
  const {
    entities,
    filter,
    onFilter,
    onFilterEventHandler,
    onSortEventHandler,
    sort,
    onReload,
    onLoadDebounced,
    store,
  } = useCategoriesResource();

  const { countryId } = useLocationFormValues();

  useEffect(() => {
    onFilter("countryId", countryId);
    onReload();
  }, [countryId]);

  const onEdit = useCallback(
    async (entity: ICategoryModal) => {
      const state = getState();
      const entities = resourceSelectors.getEntries(
        state,
        store.name
      ) as ICategory[];
      const { data } = await services.admin_categories.updateCategory(
        String(entity.id),
        entity
      );

      const _entities = entities.map((item) =>
        String(item.id) == String(data.message.id)
          ? {
              ...data.message,
              tags: data.message.tags.map((tag: ITagCategories) => ({
                ...tag,
                name: tag.name_tag,
              })),
            }
          : item
      );
      dispatch(store.actions.setEntities(_entities));
    },
    [store, dispatch, getState]
  );

  const onCreate = useCallback(
    async (entity: ICategoryModal) => {
      const { data } = await services.admin_categories.createCategory(
        countryId,
        entity
      );

      dispatch(
        store.actions.mergeEntities([
          {
            ...data.message,
            tags: data.message.tags.map((tag: ITagCategories) => ({
              ...tag,
              name: tag.name_tag,
            })),
          },
        ])
      );
    },
    [store, dispatch]
  );

  const onDelete = useCallback(
    async (entity: ICategory) => {
      const state = getState();
      const entities: ICategory[] = resourceSelectors.getEntries(
        state,
        store.name
      );
      const _entities = entities.filter(
        ({ id }) => String(entity.id) !== String(id)
      );
      await services.admin_categories.deleteCategory(String(entity.id));
      dispatch(store.actions.setEntities(_entities));
    },
    [store, dispatch]
  );

  const [fragment, onOpen, onClose] = useModal(EditModal);

  const onClick = useCallback(
    (data: ICategory, name: string | undefined) => {
      switch (name) {
        case EDIT:
          return onOpen({
            state: {
              countryId: Number(countryId),
              id: data.id,
              pos: String(data.numList),
              name: data.name.EN,
              tag: data.tags ? (data.tags.map((tag) => tag.id) as never[]) : [],
            },
            onSave: onEdit,
            onClose,
          });
        case DELETE:
          return onDelete(data);
      }
    },
    [onOpen, onDelete, onEdit, onClose]
  );

  return (
    <div className={styles.content}>
      <div className={styles.actions}>
        <Button.Icon
          view={Button.Icon.views.CIRCLED_PLUS}
          onClick={() => onOpen({ onSave: onCreate, onClose })}
        />
      </div>
      {fragment}
      <div className={styles.register}>
        <Register
          data={entities}
          template={template}
          filterValues={filter}
          onFilterChange={onFilterEventHandler}
          sortValues={sort}
          onSortChange={onSortEventHandler}
          onScrollDown={onLoadDebounced}
          onClickHandler={onClick}
        />
      </div>
    </div>
  );
};

export default Categories;
