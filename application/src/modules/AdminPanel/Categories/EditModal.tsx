import React, { FC, useEffect } from "react";
import { Button, Input, Select } from "@package/components";
import { useForm, useFormChange, useI18n } from "src/hooks";
import { useTagsResource, useLocationFormValues } from "../hooks";
import { ICategoryModal } from "./types";
import { ITag } from "../Tags/types";

import styles from "./EditModal.module.scss";

const defaultTagsValueSelector = (option: ITag) => option.nameTag;
const defaultTagsIdSelector = (option: ITag) => option.id;

const initialState = {
  countryId: -1,
  id: -1,
  pos: "",
  name: "",
  tag: [],
};

type IEditModal = {
  state?: typeof initialState;
  onClose: () => void;
  onSave: (entity: ICategoryModal) => void;
};

const EditModal: FC<IEditModal> = ({
  state = initialState,
  onClose,
  onSave: onSaveCallback,
}) => {
  const i18n = useI18n();

  const { entities: tags, onFilter, onReload } = useTagsResource();

  const { countryId } = useLocationFormValues();

  const ref = React.useRef(null);
  
  useEffect(() => {
    onFilter("countryId", countryId);
    onReload();
  }, [countryId]);

  const { values, onChange, onSave } = useForm("admin-panel-form", state, {
    saver: async (toSave) => {
      const normalizedValue = {
        name: { EN: toSave.name, FR: toSave.name, DE: toSave.name },
        numList: toSave.pos,
        tagsId: toSave.tag,
        id: toSave.id,
      };
      await onSaveCallback(normalizedValue);
      onClose && onClose();
      return Promise.resolve({ data: toSave });
    },
  });

  const input = useFormChange(values, onChange);

  return (
    <>
      <div className={styles.container} ref={ref}>
        <div className={styles.form}>
          <Input
            {...input.input("pos")}
            className={styles.input}
            placeholder={i18n.adminPanel.position_label}
          />
          <Input
            {...input.input("name")}
            className={styles.input}
            placeholder={i18n.adminPanel.name_label}
          />
          <div className={styles.select}>
            <Select.Multi
              {...input.input("tag")}
              boundaries={ref.current}
              options={tags}
              placeholder={i18n.common.select}
              valueSelector={defaultTagsValueSelector}
              idSelector={defaultTagsIdSelector}
            />
          </div>
        </div>
        <div className={styles.actions}>
          <Button className={styles.item} onClick={onClose}>
            {i18n.common.cancel}
          </Button>
          <Button className={styles.item} onClick={onSave}>
            {i18n.common.save}
          </Button>
        </div>
      </div>
    </>
  );
};

export default EditModal;
