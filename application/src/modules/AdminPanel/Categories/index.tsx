import React, { lazy } from "react";

const Component = lazy(() => import("./Categories"));

export default {
  path: "/categories",
  element: <Component />,
};
