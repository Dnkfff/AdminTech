import { useResource } from "src/hooks";
import { admin_users } from "src/service";
import { IUserRole } from "../Users/type";

const FRAME_SIZE = 50;

export const useUsersRolesResource = () => {
  return useResource<IUserRole>("users-roles-resource", async ({ filter }) => {
    const { data } = await admin_users.getRoles(filter.countryId);

    return {
      data: data.message as IUserRole[],
      isLoadedAll: data.message.length < FRAME_SIZE,
    };
  });
};
