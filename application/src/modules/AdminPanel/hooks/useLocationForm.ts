import {
  useCountryLanguageResource,
  resourceSelectors,
  useFormStore,
} from "src/hooks";
import { useSelector, useStore } from "react-redux";

type IAdminPanelCountryAndLangObj = {
  countryId: string;
  languageId: string;
};

const initialState = {
  countryId: "",
  languageId: "",
};

export const locationFormStore = useFormStore.getFormStore(
  "admin-panel-location-form",
  initialState
);

export const useLocationFormValues = () =>
  useSelector<unknown, IAdminPanelCountryAndLangObj>((state) =>
    useFormStore.selectors.getValues(state, locationFormStore.name)
  );

export const useLocationForm = (
  countryLanguageStore?: ReturnType<typeof useCountryLanguageResource>["store"]
) => {
  const { getState } = useStore();
  return useFormStore<typeof locationFormStore, IAdminPanelCountryAndLangObj>(
    locationFormStore,
    {
      loader: async () => {
        const state = getState();
        const entity = resourceSelectors.getEntries(
          state,
          countryLanguageStore?.name ?? ""
        );

        return Promise.resolve({
          countryId: entity[0].countryId,
          languageId: entity[0].children[0],
        });
      },
    }
  );
};
