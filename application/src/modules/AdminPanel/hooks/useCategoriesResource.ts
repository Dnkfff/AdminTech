import { useResource } from "src/hooks";
import { admin_categories } from "src/service";
import { ICategory } from "../Categories/types";

const FRAME_SIZE = 50;

export const useCategoriesResource = () => {
  return useResource<ICategory>(
    "categories-resource",
    async ({ filter, sort, entries }) => {
      const _filters = { ...filter };
      delete _filters.countryId;

      const query = new URLSearchParams([
        ...Object.entries(_filters)
          .filter(([, value]) => !!value)
          .map(([key, value]) => ["f" + key, value]),

        ...Object.entries(sort)
          .filter(([, value]) => !!value)
          .map(([key, value]) => ["s" + key, value === "ASC" ? 0 : 1]),

        ["frame", Math.floor(entries.length / FRAME_SIZE)],
      ]);

      const { data } = await admin_categories.getCategories(
        filter.countryId,
        query.toString()
      );

      return {
        data: data.message as ICategory[],
        isLoadedAll: data.message.length < FRAME_SIZE,
      };
    }
  );
};
