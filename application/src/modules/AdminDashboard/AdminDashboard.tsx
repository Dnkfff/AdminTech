import React, { FC } from "react";
import { useI18n } from "src/hooks";
import { Layout } from "src/components";
import styles from "./AdminDashboard.module.scss";
import { always, Button } from "@package/components";
import adminCountryAndLanguageConfig from "src/modules/AdminCountryAndLanguage";
import adminPanelPanelConfig from "src/modules/AdminPanel";
import adminPanelPanelDropdownsConfig from "src/modules/AdminPanel/Dropdowns";
import adminPanelConstructorList from "src/modules/AdminPanelConstructorList";

import { Link } from "react-router-dom";

type IAdminDashboard = { type?: "IAdminDashboard" };

const AdminDashboard: FC<IAdminDashboard> = () => {
  const i18n = useI18n();

  return (
    <Layout>
      <h1 className={styles.title}>{i18n.adminDashboard.admin_dashboard}</h1>
      <div className={styles.content_container}>
        <div className={styles.content}>
          <Link to={adminCountryAndLanguageConfig.path} className={styles.link}>
            <Button
              className={styles.button}
              fill={Button.fills.HavelockBlue}
              type={Button.types.Rectangle}
              border={Button.borders.Transparent}
              color={Button.colors.White}
              onClick={always.EMPTY_FUNC}
            >
              {i18n.adminDashboard.countries_and_languages}
            </Button>
          </Link>
          <Link to={adminPanelConstructorList.path} className={styles.link}>
            <Button
              className={styles.button}
              fill={Button.fills.HavelockBlue}
              type={Button.types.Rectangle}
              border={Button.borders.Transparent}
              color={Button.colors.White}
              onClick={always.EMPTY_FUNC}
            >
              {i18n.adminDashboard.constructor}
            </Button>
          </Link>
          <Link
            to={
              adminPanelPanelConfig.path + adminPanelPanelDropdownsConfig.path
            }
            className={styles.link}
          >
            <Button
              className={styles.button}
              fill={Button.fills.HavelockBlue}
              type={Button.types.Rectangle}
              border={Button.borders.Transparent}
              color={Button.colors.White}
              onClick={always.EMPTY_FUNC}
            >
              {i18n.adminDashboard.system_configurations}
            </Button>
          </Link>
        </div>
      </div>
    </Layout>
  );
};

export default AdminDashboard;
