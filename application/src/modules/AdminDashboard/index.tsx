import React, { lazy } from "react";

const Component = lazy(() => import("./AdminDashboard"));

export default {
  path: "/admin-dashboard",
  element: <Component />,
};
