import React, { lazy } from "react";

const Component = lazy(() => import("./TermsAndConditions"));

export default {
  path: "/terms-and-conditions",
  element: <Component />,
};
