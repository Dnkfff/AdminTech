import React, { FC } from "react";
import { Layout, TermsAndConditions as T_AND_C } from "src/components";

const TermsAndConditions: FC = () => {
  return (
    <Layout
      stickyHeader
      headOptions={[Layout.headOptions.basket, Layout.headOptions.profile]}
    >
      <T_AND_C />
    </Layout>
  );
};

export default TermsAndConditions;
