import React, { lazy } from "react";

const Component = lazy(() => import("./Documents"));

export default {
  path: "/documents",
  element: <Component />,
}
