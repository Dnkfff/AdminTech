import React, { FC, useCallback } from "react";
import { useModal, Modal as ModalComponent } from "@package/components";
import DeleteDocumentModal from "./DeleteDocumentModal";
import { useI18n } from "src/hooks";

const Documents: FC = () => {
  const i18n = useI18n();

  const [fragment, onOpen, onClose] = useModal(DeleteDocumentModal, {
    modalProps: { title: i18n.profile.delete_document },
    modalComponent: ModalComponent.Headered,
  });

  const onAdd = useCallback(() => {
    return onOpen({
      onClose,
    });
  }, [onClose]);

  return (
    <>
      <p>Documents</p>
      <button onClick={onAdd}>DELETE</button>
      {fragment}
    </>
  );
};

export default Documents;
