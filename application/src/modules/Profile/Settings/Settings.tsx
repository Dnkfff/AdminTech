import React, { FC, useCallback } from "react";
import { useI18n } from "src/hooks/useI18n";
import {
  useChangePasswordModal,
  useDeleteAccountModal,
  PasswordChangedSuccessModal,
} from "./hooks";

import styles from "./Settings.module.scss";

const Settings: FC = () => {
  const i18n = useI18n();

  const [fragmentChangePwd, onOpenChangePwd, onCloseChangePwd] =
    useChangePasswordModal();
  const [fragmentDeleteAcc, onOpenDeleteAcc, onCloseDeleteAcc] =
    useDeleteAccountModal();
  const [fragmentPwdChanged, onOpenPwdChanged, onClosePwdChanged] =
    PasswordChangedSuccessModal();

  const openPasswordChangedSuccessModalHandler = useCallback(() => {
    onCloseChangePwd();
    onOpenPwdChanged({
      onClose: onClosePwdChanged,
    });
  }, [onCloseChangePwd, onOpenPwdChanged, onClosePwdChanged]);

  const openChangePasswordHandler = useCallback(() => {
    onOpenChangePwd({
      onClose: onCloseChangePwd,
      onSaved: openPasswordChangedSuccessModalHandler,
    });
  }, [
    onOpenChangePwd,
    onCloseChangePwd,
    openPasswordChangedSuccessModalHandler,
  ]);

  const openDeleteAccountHandler = useCallback(() => {
    onOpenDeleteAcc({
      onClose: onCloseDeleteAcc,
    });
  }, [onOpenDeleteAcc, onCloseDeleteAcc]);

  return (
    <div className={styles.container}>
      <article className={styles.card}>
        <div className={styles.content}>
          <h2 className={styles.title}>{i18n.profile.change_password}</h2>
          <p className={styles.password}>*******</p>
        </div>
        <div className={styles.action}>
          <button className={styles.button} onClick={openChangePasswordHandler}>
            {i18n.common.change}
          </button>
        </div>
      </article>
      <article className={styles.card}>
        <div className={styles.content}>
          <h2 className={styles.title}>{i18n.profile.delete_account}</h2>
          <p className={styles.text}>{i18n.profile.delete_account_message}</p>
        </div>
        <div className={styles.action}>
          <button className={styles.button} onClick={openDeleteAccountHandler}>
            {i18n.profile.delete_my_account}
          </button>
        </div>
      </article>
      {fragmentChangePwd}
      {fragmentDeleteAcc}
      {fragmentPwdChanged}
    </div>
  );
};

export default Settings;
