import React, { FC, useCallback } from "react";
import { useI18n } from "src/hooks";
import { Button } from "@package/components";
import * as service from "src/service";
import { ls } from "src/utils";
import cx from "classnames";

import styles from "./index.module.scss";

const DeleteAccountModal: FC<{
  onClose: () => void;
}> = ({ onClose }) => {
  const i18n = useI18n();

  const onDeleteAccount = useCallback(async () => {
    const response = await service.profile_settings.delete_account();

    if (response.response.ok) {
      ls.token.remove();
    }
  }, []);

  return (
    <div className={styles.frame}>
      <div className={styles.container}>
        <p className={styles.text}>{i18n.profile.delete_profile_message}</p>

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.cancel}
          </Button>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onDeleteAccount}
          >
            {i18n.common.delete}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default DeleteAccountModal;
