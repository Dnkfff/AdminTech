import { useI18n } from "src/hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "../PasswordChangedSuccessModal";

export const PasswordChangedSuccessModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.profile.password_changed },
    modalComponent: ModalComponent.Headered,
  });
};
