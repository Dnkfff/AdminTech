import { useI18n } from "src/hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "../ChangePasswordModal";

export const useChangePasswordModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.profile.change_password },
    modalComponent: ModalComponent.Headered,
  });
}
