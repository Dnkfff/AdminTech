import { useI18n } from "src/hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "../DeleteAccountModal";

export const useDeleteAccountModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.profile.delete_profile },
    modalComponent: ModalComponent.Headered,
  });
}
