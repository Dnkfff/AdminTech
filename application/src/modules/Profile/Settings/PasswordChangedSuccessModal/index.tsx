import React, { FC } from "react";
import { useI18n } from "src/hooks";
import { Button } from "@package/components";
import cx from "classnames";

import styles from "./index.module.scss";

const PasswordChangedSuccessModal: FC<{
  onClose: () => void;
}> = ({ onClose }) => {
  const i18n = useI18n();

  return (
    <div className={styles.frame}>
      <div className={styles.container}>
        <p className={styles.text}>{i18n.profile.password_changed_message}</p>

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.close}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default PasswordChangedSuccessModal;
