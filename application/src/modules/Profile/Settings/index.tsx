import React, { lazy } from "react";

const Component = lazy(() => import("./Settings"));

export default {
  path: "/settings",
  element: <Component />,
};
