import React, { FC } from "react";
import { useForm, useFormChange, useI18n } from "src/hooks";
import { Button } from "@package/components";
import InputField from "src/components/Header/Auth/SignIn/components/InputField";
import * as yup from "yup";
import { validation as validationUtils } from "src/utils";
import * as service from "src/service";
import cx from "classnames";

import styles from "./index.module.scss";

type IChangePassword = {
  current_password: string;
  new_password: string;
  new_password_1: string;
};

const initialValues: IChangePassword = {
  current_password: "",
  new_password: "",
  new_password_1: "",
};

const validationSchema = yup.object().shape({
  current_password: yup
    .string()
    .label("Enter Current Password")
    .matches(new RegExp("^.{8,30}$"), { message: "Incorrect password" })
    .required(),
  new_password: yup
    .string()
    .label("Enter new password")
    .matches(new RegExp("^.{8,30}$"), { message: "Incorrect password" })
    .required(),
  new_password_1: yup
    .string()
    .label("Repeat new password")
    .oneOf([yup.ref("new_password"), null], "The passwords don't match")
    .required(),
});

const ChangePasswordModal: FC<{
  onClose: () => void;
  onSaved: () => void;
}> = ({ onClose, onSaved }) => {
  const i18n = useI18n();

  const form = useForm<IChangePassword>("change-password-form", initialValues, {
    saver: async (data) => {
      const validationResult = await validationUtils.normalizedValidation(
        validationSchema,
        data
      );

      if (validationResult.success) {
        const send_value = {
          password: validationResult.value.new_password,
        };

        const response = await service.profile_settings.change_password(
          send_value
        );

        if (!response.response.ok || response.data.error) {
          return {
            data,
            validation: { response_error: response?.data?.message || "" },
          };
        }

        onSaved();
        return { data };
      }

      return { data, validation: validationResult.value };
    },
  });

  const { values, validation, onChange, onSave } = form;

  const input = useFormChange(values, onChange);

  return (
    <div className={styles.frame}>
      <form className={styles.form}>
        <InputField
          title={i18n.profile.enter_current_password}
          required
          error={validation.current_password}
          {...input.input("current_password" as keyof IChangePassword)}
          type="password"
        />
        <InputField
          title={i18n.profile.enter_new_password}
          required
          error={validation.new_password}
          {...input.input("new_password" as keyof IChangePassword)}
          type="password"
        />
        <InputField
          title={i18n.profile.repeat_new_password}
          required
          error={validation.new_password_1}
          {...input.input("new_password_1" as keyof IChangePassword)}
          type="password"
        />

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.cancel}
          </Button>
          <Button
            border={Button.borders.GullGray}
            color={Button.colors.GullGray}
            className={styles.button}
            onClick={onSave}
          >
            {i18n.common.submit}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default ChangePasswordModal;
