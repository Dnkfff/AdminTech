import React, { FC, Suspense } from "react";
import { Route, Routes, Navigate, Outlet } from "react-router-dom";
import { useSelector } from "react-redux";
import { authorization } from "src/store";
import { Layout } from "src/components";
import Aside from "./Aside";
import settings from "./Settings";
import subscriptions from "./Subscriptions";
import documents from "./Documents";
import { WithAuth } from "src/components/Providers/AuthProvider";
import home from "../Home";

import styles from "./Profile.module.scss";

const Profile: FC = () => {
  // const isAuthorized = useSelector(authorization.selectors.getIsAuthorized);

  // if (!isAuthorized) {
  //   return <Navigate to={home.path} replace />;
  // }

  return (
    <Layout
      stickyHeader
      headOptions={[
        Layout.headOptions.basket,
        Layout.headOptions.countryAndLanguage,
      ]}
    >
      <div className={styles.container}>
        <Aside />
        <div className={styles.content}>
          <Suspense fallback={"ooooops"}>
            <Routes>
              <Route path={settings.path} element={settings.element}></Route>
              <Route
                path={subscriptions.path}
                element={subscriptions.element}
              ></Route>
              <Route path={documents.path} element={documents.element}></Route>
            </Routes>
          </Suspense>
        </div>
      </div>
    </Layout>
  );
};

export default WithAuth(Profile);
