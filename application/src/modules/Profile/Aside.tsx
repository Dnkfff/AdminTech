import React, { FC } from "react";
import { Icon } from "@package/components";
import { NavLink } from "react-router-dom";
import { useI18n } from "src/hooks/useI18n";
import { ls } from "src/utils";
import cx from "classnames";
import settings from "./Settings";
import subscriptions from "./Subscriptions";
import documents from "./Documents";

import styles from "./Aside.module.scss";

const classNameSelector = ({ isActive }: { isActive: boolean }) =>
  cx(styles.link, { [styles.active]: isActive });

const Aside: FC = () => {
  const i18n = useI18n();

  return (
    <aside className={styles.aside}>
      <div className={styles.container}>
        <ul className={styles.list}>
          <li className={styles.item}>
            <NavLink
              to={`${settings.path.replace("/", "")}`}
              className={classNameSelector}
            >
              <Icon view={Icon.views.SETTINGS} className={styles.icon} />
              {i18n.profile.settings_label}
            </NavLink>
          </li>
          <li className={styles.item}>
            <NavLink
              to={subscriptions.path.replace("/", "")}
              className={classNameSelector}
            >
              <Icon view={Icon.views.SUBSCRIPTIONS} className={styles.icon} />
              {i18n.profile.subscriptions_label}
            </NavLink>
          </li>
          <li className={styles.item}>
            <NavLink
              to={documents.path.replace("/", "")}
              className={classNameSelector}
            >
              <Icon view={Icon.views.DOCUMENTS} className={styles.icon} />
              {i18n.profile.documents_label}
            </NavLink>
          </li>
          <li className={styles.item}>
            <button onClick={ls.token.remove} className={styles.link}>
              <Icon view={Icon.views.LOGOUT} className={styles.icon} />
              {i18n.profile.logout_label}
            </button>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default Aside;
