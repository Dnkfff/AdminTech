import React, { lazy } from "react";

const Component = lazy(() => import("./Profile"));

export default {
  path: "profile/*",
  element: <Component />,
};
