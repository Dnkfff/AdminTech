import { useResource } from "src/hooks";
import { profile_subscriptions } from "src/service";

type IStatuses = {
    id: number;
    errors: boolean;
  };

type IProps = {
    skip: number;
    loadingQuantity: number;
  };

export const useSubscriptionResource = (props: IProps) => {
  return useResource<IStatuses>("constructor-list-statuses", async ({ filter, sort, entries }) => {

    const loadingQuantity = entries.length
        ? entries.length
        : props.loadingQuantity;
    
    const { data } = await profile_subscriptions.getSubscription();

    return { data: data.message };
  });
};
