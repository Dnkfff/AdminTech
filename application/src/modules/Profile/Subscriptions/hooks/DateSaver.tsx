import React, { useState } from "react";
import { useI18n } from "src/hooks/useI18n";

export interface title {
    title: string;
    date: Date;
}

const now =  new Date(); // Wed Aug 10 2022 15:02:46 GMT+0300 (за східноєвропейським літнім часом)

const i18n = useI18n();


const [titleString, date] = useState({
    title: i18n.profile.next_payment,
    date: now.toUTCString(), // Wed, 10 Aug 2022 12:02:46 GMT
});
