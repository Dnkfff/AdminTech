import React, { FC } from "react";
import { useI18n } from "src/hooks";
import { Button, Icon } from "@package/components";

import styles from "./index.module.scss";

interface ISubscriptionCancelled {
  date: string;
  onClose: () => void;
}

const SubscriptionCancelled: FC<ISubscriptionCancelled> = ({
  date,
  onClose,
}) => {
  const i18n = useI18n();

  return (
    <div className={styles.subscription_cancelled}>
      <div className={styles.close_button}>
        <Button.Icon
          view={Icon.views.CLOSE}
          className={styles.close}
          onClick={onClose}
        />
      </div>
      <div className={styles.title}>Your subscription is cancelled</div>
      <div className={styles.body}>
        {i18n.profile.create_documents_until_label}
        <br />
        <span className={styles.date}>{date}</span>
      </div>
      <div className={styles.footer}>
        <Button
          onClick={onClose}
          border={Button.borders.HavelockBlue}
          color={Button.colors.HavelockBlue}
        >
          {i18n.profile.ok_label}
        </Button>
      </div>
    </div>
  );
};

export default SubscriptionCancelled;
