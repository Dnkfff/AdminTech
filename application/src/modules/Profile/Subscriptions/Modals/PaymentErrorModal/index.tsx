import React, { FC } from "react";
import { Button, Icon } from "@package/components";
import { useI18n } from "src/hooks";

import styles from "./index.module.scss";

type IPaymentError = {
  onClose: () => void;
};

const PaymentErrorModal: FC<IPaymentError> = ({ onClose }) => {
  const i18n = useI18n();

  return (
    <div className={styles.payment_failure}>
      <div className={styles.close_button}>
        <Button.Icon
          view={Icon.views.CLOSE}
          className={styles.close}
          onClick={onClose}
        />
      </div>
      <div className={styles.circle}>
        <Icon className={styles.error} view={Icon.views.CLOSE} />
      </div>
      <div className={styles.title}> {i18n.profile.payment_error_label}</div>
      <div className={styles.body}>
        {i18n.profile.payment_unsuccessful_label}
        <br />
        {i18n.profile.card_not_charged_label}
      </div>
      <div className={styles.footer}>
        <Button
          onClick={onClose}
          border={Button.borders.HavelockBlue}
          color={Button.colors.HavelockBlue}
        >
          {i18n.profile.ok_label}
        </Button>
      </div>
    </div>
  );
};

export default PaymentErrorModal;
