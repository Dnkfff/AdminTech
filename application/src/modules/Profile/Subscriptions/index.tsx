import React, { lazy } from "react";

const Component = lazy(() => import("./Subscriptions"));

export default {
  path: "/subscriptions",
  element: <Component />,
};
