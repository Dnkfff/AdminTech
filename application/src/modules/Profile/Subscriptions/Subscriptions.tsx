import React, { FC, useState, useEffect } from "react";
import ReactDOM from "react-dom";
import styles from "./styles.module.scss"
import { useI18n } from "src/hooks/useI18n";
import { Button, always, Modal, useModal, useToggle } from '@package/components';

import { useSubscriptionResource } from "./hooks/useSubscriptionResource";



const Subscriptions: FC = () => {
  const i18n = useI18n();

  //const [isModalVisible, setIsModalVisible] = useState(false)

  const [visibleModal, setVisibleModal] = useToggle();


  const statusResource = useSubscriptionResource({
    skip: 1,
    loadingQuantity: 0,
  });

  const { entities: statuses, onLoad: onLoadStatuses } = statusResource;

  useEffect(() => {
    onLoadStatuses();
  }, []);

  const PreviewComponent = (props: any) => {
    return (
      <div className={styles.Modal_footer} style={{ padding: '10px' }}>
        { i18n.profile.activated } 
      <button className={styles.Close_btn} >{i18n.profile.subscribe_close}</button> 
    </div>

    );
  }

  const [fragment, onOpen, onClose] = useModal(PreviewComponent, { modalProps: { title: i18n.profile.thank } });


  return (
      <div className = {styles.block}>
        <h2 className = {styles.h2_DoNotMiss}>{i18n.profile.do_not_miss}</h2>
        <div className = {styles.container}>
          <h3 className = {styles.h3_monthlySubscription}>{i18n.profile.monthly_subscription}</h3>        
          <p className = {styles.content}> <span className={styles.get}> {i18n.profile.get_unlimited} <span className={styles.money_equivalent}> {i18n.profile.money_equivalent}</span> {i18n.profile.month}</span>
          <div className = {styles.subscribe_wrapper} >
          <Button className = {styles.subscribe} onClick={() => onOpen({ property1: '1', property2: '2' })}>{i18n.profile.subscribe_button}</Button>
          { fragment }
          </div>
          </p>
        </div>

        <div className = {styles.container2}>
          <h3 className = {styles.h3_asubscription}>{i18n.profile.annual_subscription}</h3>
          <p className = {styles.content}><span className={styles.span_save33}>{i18n.profile.save}</span> <span className={styles.span_saveAnd}>{i18n.profile.save_and}</span></p>

          <div className = {styles.AnnualSubscription}>
          <Button
              className = {styles.subscribe2}
              fill = {Button.fills.HavelockBlue}
              type = {Button.types.Rectangle}
              border = {Button.borders.Transparent}
              color = {Button.colors.White}
              onClick={() => onOpen({ property1: '1', property2: '2' })}
            >{i18n.profile.subscribe_button}
          </Button>
          </div>
        </div>
      </div>
  );
};

export default Subscriptions;
