import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type IInitialState = {
  isAuthorized: boolean;
  user: Record<string, any>;
};

const initialState: IInitialState = {
  isAuthorized: false,
  user: {},
};

const settings = createSlice({
  name: "authorization",
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<IInitialState["user"]>) {
      return { ...state, user: action.payload };
    },
    setIsAuthorized(
      state,
      action: PayloadAction<IInitialState["isAuthorized"]>
    ) {
      return { ...state, isAuthorized: action.payload };
    },
  },
});

export default settings;
