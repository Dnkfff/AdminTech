/* eslint-disable @typescript-eslint/no-explicit-any */
import store from "./store";
import { createSelector } from "@reduxjs/toolkit";

const self = (state: Record<string, any>) => state?.[store.name];

export const getLanguage = createSelector(self, ({ language }) => language);
export const getCountryId = createSelector(self, ({ countryId }) => countryId);
