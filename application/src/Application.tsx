import React, { Suspense } from "react";
import * as modules from "./modules";
import { HashRouter, useRoutes } from "react-router-dom";

const modules_list = Object.values(modules);
const Routes = () => useRoutes(modules_list);

function Application() {
  return (
    <Suspense fallback={"ooooops"}>
      <HashRouter>
        <Routes />
      </HashRouter>
    </Suspense>
  );
}

export default Application;
