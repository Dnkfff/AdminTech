import React, { FC, useCallback } from "react";
import { useForm, useFormChange, useI18n } from "src/hooks";
import { Button } from "@package/components";
import InputField from "../components/InputField";
import cx from "classnames";
import * as yup from "yup";
import { validation as validationUtils } from "src/utils";
import GoogleLogin from "../components/Google";
import LinkedInLogin from "../components/LinkedIn";
import * as service from "src/service";

import styles from "./index.module.scss";

type IRegister = {
  email: string;
  password: string;
  password_1: string;
  name: string;
  surName: string;
  isTerms: boolean;
};

const initialValues: IRegister = {
  email: "",
  password: "",
  password_1: "",
  name: "",
  surName: "",
  isTerms: true,
};

const validationSchema = yup.object().shape({
  name: yup.string().label("Name").min(3).max(30).required(),
  surName: yup.string().label("Surname").min(3).max(30).required(),
  email: yup.string().label("Email").email().required(),
  password: yup
    .string()
    .label("Password")
    .matches(new RegExp("^.{8,30}$"), { message: "Not valid password" })
    .required(),
  password_1: yup
    .string()
    .label("Repeat password")
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required(),
  isTerms: yup.boolean(),
});

const SignUpModal: FC<{
  onClose: () => void;
  onSaved: (email: string) => void;
  onOpenTermsAndConditions: () => void;
}> = ({ onClose, onSaved, onOpenTermsAndConditions }) => {
  const i18n = useI18n();

  const form = useForm<IRegister>("sign-up-form", initialValues, {
    saver: async (data) => {
      const validationResult = await validationUtils.normalizedValidation(
        validationSchema,
        data
      );

      if (validationResult.success) {
        const send_value = { ...validationResult.value };
        delete send_value.password_1;

        const response = await service.auth.registration(send_value);

        if (!response.response.ok || response.data.error) {
          return {
            data,
            validation: { response_error: response?.data?.message || "" },
          };
        }

        onSaved(validationResult.value.email);
        return { data };
      }

      return { data, validation: validationResult.value };
    },
  });

  const onClickGoogle = useCallback(
    async ({ access_token }: Record<string, unknown>) => {
      const response = await service.auth.registration_google(
        access_token as string
      );

      if (response.response.ok) {
        onClose();
      }
    },
    [onClose]
  );

  const { values, validation, onChange, onSave } = form;

  const input = useFormChange(values, onChange);

  return (
    <div className={styles.frame}>
      <form className={styles.form}>
        <div className={styles.row}>
          <InputField
            title={i18n.common.name}
            required
            error={validation.name}
            {...input.input("name" as keyof IRegister)}
          />
          <InputField
            title={i18n.common.surname}
            required
            error={validation.surName}
            {...input.input("surName" as keyof IRegister)}
          />
        </div>
        <InputField
          title={i18n.common.email}
          required
          error={validation.email}
          {...input.input("email" as keyof IRegister)}
          type="email"
        />
        <InputField
          title={i18n.common.password}
          required
          error={validation.password}
          {...input.input("password" as keyof IRegister)}
          type="password"
        />
        <InputField
          title={i18n.common.repeat_password}
          required
          error={validation.password_1}
          {...input.input("password_1" as keyof IRegister)}
          type="password"
        />

        <div className={styles.terms}>
          <input
            type="checkbox"
            id="terms"
            checked={values?.isTerms}
            readOnly
            className={styles.checkbox}
          />
          <label className={styles.terms_text}>
            <p className={styles.text}>{i18n.common.accept}</p>
            <button
              className={cx(styles.text, styles.terms_and_conditions)}
              onClick={onOpenTermsAndConditions}
              type="button"
            >
              {i18n.common.terms_and_conditions}
            </button>
          </label>
        </div>

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.cancel}
          </Button>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onSave}
          >
            {i18n.common.submit}
          </Button>
        </div>

        <div className={styles.divider}>{i18n.common.or_sign_up_with}</div>

        <div className={cx(styles.row, styles.social_bar)}>
          <GoogleLogin onSuccess={onClickGoogle} text={i18n.common.google} />
          <LinkedInLogin onSuccess={console.log} text={i18n.common.linked_in} />
        </div>
      </form>
    </div>
  );
};

export default SignUpModal;
