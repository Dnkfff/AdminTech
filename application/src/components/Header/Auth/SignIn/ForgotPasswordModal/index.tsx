import React, { FC } from "react";
import { useForm, useFormChange, useI18n } from "src/hooks";
import { Button, useKeyPress } from "@package/components";
import InputField from "../components/InputField";
import cx from "classnames";
import * as yup from "yup";
import { validation as validationUtils } from "src/utils";

import styles from "./index.module.scss";

type IForgotPassword = {
  email: string;
};

const initialValues: IForgotPassword = {
  email: "",
};

const validationSchema = yup.object().shape({
  email: yup.string().label("Email").email().required(),
});

const ForgotPasswordModal: FC<{
  onClose: () => void;
  onSend: (email: string) => void;
}> = ({ onClose, onSend }) => {
  const i18n = useI18n();

  const form = useForm<IForgotPassword>("forgot-password-form", initialValues, {
    saver: async (data) => {
      const validationResult = await validationUtils.normalizedValidation(
        validationSchema,
        data
      );
      if (validationResult.success) {
        console.log("send it -> ", validationResult.value);
        onSend(validationResult.value.email);
        return { data };
      }
      return { data, validation: validationResult.value };
    },
  });

  const { values, validation, onChange, onSave } = form;

  const input = useFormChange(values, onChange);

  useKeyPress("Enter", onSave);

  return (
    <div className={styles.frame}>
      <form className={styles.form}>
        <p className={styles.text}>{i18n.common.forgot_password_description}</p>

        <InputField
          title={i18n.common.email}
          error={validation.email}
          {...input.input("email" as keyof IForgotPassword)}
          type="email"
        />

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.cancel}
          </Button>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onSave}
          >
            {i18n.common.send}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default ForgotPasswordModal;
