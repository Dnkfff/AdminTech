import { useI18n } from "../../../../hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "./SignInModal";

export const useSignInModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.common.sign_in },
    modalComponent: ModalComponent.Headered,
  });
};
