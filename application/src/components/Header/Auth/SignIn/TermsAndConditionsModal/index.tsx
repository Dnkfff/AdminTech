import React, { FC } from "react";
import { TermsAndConditions } from "src/components";
import styles from "./style.module.scss";

interface ITermsAndConditionsModal {}

const TermsAndConditionsModal: FC<ITermsAndConditionsModal> = () => {
  return (
    <section className={styles.section}>
      <TermsAndConditions />
    </section>
  );
};

export default TermsAndConditionsModal;
