import React, { FC, useCallback } from "react";
import { useForm, useFormChange, useI18n } from "src/hooks";
import { Button } from "@package/components";
import InputField from "../components/InputField";
import cx from "classnames";
import * as yup from "yup";
import { validation as validationUtils } from "src/utils";
import GoogleLogin from "../components/Google";
import LinkedInLogin from "../components/LinkedIn";
import * as service from "src/service";

import styles from "./index.module.scss";

type IRegister = {
  email: string;
  password: string;
};

const initialValues: IRegister = {
  email: "",
  password: "",
};

const validationSchema = yup.object().shape({
  email: yup.string().label("Email").email().required(),
  password: yup
    .string()
    .label("Password")
    .matches(new RegExp("^.{8,30}$"), { message: "Not valid password" })
    .required(),
});

const SignUpModal: FC<{
  onClose: () => void;
  onContinue: () => void;
  onOpenSignUp: () => void;
  onOpenForgotPassword: () => void;
}> = ({ onClose, onContinue, onOpenSignUp, onOpenForgotPassword }) => {
  const i18n = useI18n();

  const form = useForm<IRegister>("sign-in-form", initialValues, {
    saver: async (data) => {
      const validationResult = await validationUtils.normalizedValidation(
        validationSchema,
        data
      );

      if (validationResult.success) {
        const response = await service.auth.login(validationResult.value);

        if (response.data.error) {
          return {
            data,
            validation: { response_error: response.data.message },
          };
        }

        onContinue();
        return { data };
      }

      return { data, validation: validationResult.value };
    },
  });

  const onClickGoogle = useCallback(
    async ({ access_token }: Record<string, unknown>) => {
      const response = await service.auth.login_google(access_token as string);

      if (response.response.ok) {
        onClose();
      }
    },
    [onClose]
  );

  const { values, validation, onChange, onSave } = form;

  const input = useFormChange(values, onChange);

  return (
    <div className={styles.frame}>
      <form className={styles.form}>
        <InputField
          title={i18n.common.email}
          error={validation.email}
          {...input.input("email" as keyof IRegister)}
          type="email"
        />
        <InputField
          title={i18n.common.password}
          error={validation.password}
          {...input.input("password" as keyof IRegister)}
          type="password"
        />

        <div className={styles.row}>
          <button
            onClick={onOpenForgotPassword}
            className={cx(styles.action_button, styles.forgot_password)}
          >
            {i18n.common.forgot_password}
          </button>
        </div>

        <div className={cx(styles.row, styles.sign_up)}>
          <p>{i18n.common.new_in_application}</p>
          <button onClick={onOpenSignUp} className={styles.action_button}>
            {i18n.common.sign_up_now}
          </button>
        </div>

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.cancel}
          </Button>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onSave}
          >
            {i18n.common.continue}
          </Button>
        </div>

        <div className={styles.divider}>{i18n.common.or}</div>

        <div className={styles.social_bar}>
          <GoogleLogin onSuccess={onClickGoogle} text={i18n.common.google} />
          <LinkedInLogin onSuccess={console.log} text={i18n.common.linked_in} />
        </div>
      </form>
    </div>
  );
};

export default SignUpModal;
