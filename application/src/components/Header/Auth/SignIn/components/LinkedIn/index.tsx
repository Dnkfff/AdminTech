import React, { FC } from "react";
import { useLinkedIn } from "react-linkedin-login-oauth2";
import { env } from "src/utils";
import { Button, Icon } from "@package/components";
import styles from "./styles.module.scss";

interface IGoogleLogin {
  onSuccess: (param: any) => void;
  text: string;
}

const LinkedInLogin: FC<IGoogleLogin> = ({ onSuccess, text }) => {
  const { linkedInLogin } = useLinkedIn({
    onSuccess,
    clientId: env.LINKED_IN_CLIENT_ID as string,
    redirectUri: `${window.location.origin}/callback/linkedin`,
  });

  return (
    <Button
      onClick={linkedInLogin}
      className={styles.button}
      fill={Button.fills.HavelockBlue}
      border={Button.borders.HavelockBlue}
    >
      <div className={styles.container}>
        <Icon view={Icon.views.LINKED_IN} />
        <p className={styles.text}>{text}</p>
      </div>
    </Button>
  );
};

export default LinkedInLogin;
