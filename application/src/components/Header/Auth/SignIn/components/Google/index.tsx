import React, { ComponentProps, FC, useCallback } from "react";
import { GoogleOAuthProvider, useGoogleLogin } from "@react-oauth/google";
import { env } from "src/utils";
import { Button, Icon } from "@package/components";
import styles from "./styles.module.scss";

interface IGoogleLogin {
  onSuccess: (param: any) => void;
  text: string;
}

const GoogleLogin: FC<IGoogleLogin> = ({ onSuccess, text }) => {
  const onGoogleLogin = useGoogleLogin({ onSuccess });
  return (
    <Button
      onClick={useCallback(() => onGoogleLogin(), [onGoogleLogin])}
      className={styles.button}
      fill={Button.fills.Coral}
      border={Button.borders.Coral}
    >
      <div className={styles.container}>
        <Icon view={Icon.views.GOOGLE} />
        <p className={styles.text}>{text}</p>
      </div>
    </Button>
  );
};

const withGoogleOAuth = (Component: typeof GoogleLogin) => {
  const WrappedComponent = (props: ComponentProps<typeof Component>) => {
    return (
      <GoogleOAuthProvider clientId={env.GOOGLE_CLIENT_ID as string}>
        <Component {...props} />
      </GoogleOAuthProvider>
    );
  };
  WrappedComponent.displayName = `withGoogleOAuthProvider(${Component.name})`;
  return WrappedComponent;
};

export default withGoogleOAuth(GoogleLogin);
