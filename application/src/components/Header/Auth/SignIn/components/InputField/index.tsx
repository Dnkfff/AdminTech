import React, { ComponentProps, useId } from "react";
import { Input } from "@package/components";
import cx from "classnames";

import styles from "./InputField.module.scss";

const InputField = ({
  title,
  required,
  error,
  ...rest
}: { title: string; required?: boolean; error?: string } & ComponentProps<
  typeof Input
>) => {
  const id = useId();
  return (
    <div className={styles.field}>
      <label
        className={cx(styles.label, { [styles.required]: required })}
        htmlFor={id}
      >
        {title}
      </label>
      <Input
        id={id}
        className={cx(styles.input, { [styles.input__error]: error })}
        {...rest}
      />
      <p className={styles.error}>{error}</p>
    </div>
  );
};

export default InputField;
