import React, { FC } from "react";
import styles from "./styles.module.scss";
import { Button } from "@package/components";
import { useI18n } from "../../../../../hooks";

interface IConfirmRegistration {
  email: string;
  onClose: () => void;
}

const ConfirmRegistration: FC<IConfirmRegistration> = ({ email, onClose }) => {
  const i18n = useI18n();
  return (
    <div className={styles.frame}>
      <p className={styles.text}>
        To confirm registration, follow the link sent to you by mail
        <span className={styles.email}> {email}</span>
      </p>
      <Button onClick={onClose}>{i18n.common.ok}</Button>
    </div>
  );
};

export default ConfirmRegistration;
