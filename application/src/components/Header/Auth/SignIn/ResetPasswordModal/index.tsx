import React, { FC } from "react";
import { useForm, useFormChange, useI18n } from "src/hooks";
import { Button, useKeyPress } from "@package/components";
import InputField from "../components/InputField";
import cx from "classnames";
import * as yup from "yup";
import { validation as validationUtils } from "src/utils";

import styles from "./index.module.scss";

type IResetPassword = {
  password: string;
  confirmPassword: string;
};

const initialValues: IResetPassword = {
  password: "",
  confirmPassword: "",
};

const validationSchema = yup.object().shape({
  password: yup
    .string()
    .label("New password")
    .matches(new RegExp("^.{8,30}$"), { message: "Not valid password" })
    .required(),
  confirmPassword: yup
    .string()
    .label("Repeat new password")
    .oneOf([yup.ref("password"), null], "The passwords don’t match")
    .required(),
});

const ResetPasswordModal: FC<{
  onClose: () => void;
  onSaved: () => void;
}> = ({ onClose, onSaved }) => {
  const i18n = useI18n();

  const form = useForm<IResetPassword>("reset-password-form", initialValues, {
    saver: async (data) => {
      const validationResult = await validationUtils.normalizedValidation(
        validationSchema,
        data
      );
      if (validationResult.success) {
        console.log("send it -> ", validationResult.value);
        onSaved();
        return { data };
      }
      return { data, validation: validationResult.value };
    },
  });

  const { values, validation, onChange, onSave } = form;

  const input = useFormChange(values, onChange);

  useKeyPress("Enter", onSave);

  return (
    <div className={styles.frame}>
      <form className={styles.form}>
        <InputField
          title={i18n.common.new_password}
          required
          error={validation.password}
          {...input.input("password" as keyof IResetPassword)}
          type="password"
        />

        <InputField
          title={i18n.common.repeat_new_password}
          required
          error={validation.confirmPassword}
          {...input.input("confirmPassword" as keyof IResetPassword)}
          type="password"
        />

        <div className={cx(styles.row, styles.buttons_bar)}>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onClose}
          >
            {i18n.common.cancel}
          </Button>
          <Button
            border={Button.borders.HavelockBlue}
            color={Button.colors.HavelockBlue}
            className={styles.button}
            onClick={onSave}
          >
            {i18n.common.save}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default ResetPasswordModal;
