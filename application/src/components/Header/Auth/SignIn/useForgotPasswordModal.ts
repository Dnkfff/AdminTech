import { useI18n } from "../../../../hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "./ForgotPasswordModal";

export const useForgotPasswordModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.common.forgot_your_password },
    modalComponent: ModalComponent.Headered,
  });
};
