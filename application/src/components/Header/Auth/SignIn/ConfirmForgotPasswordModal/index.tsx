import React, { FC } from "react";
import styles from "./styles.module.scss";
import { useI18n } from "../../../../../hooks";

interface IConfirmForgotPassword {
  email: string;
}

const ConfirmForgotPassword: FC<IConfirmForgotPassword> = ({ email }) => {
  const i18n = useI18n();
  return (
    <div className={styles.frame}>
      <p className={styles.text}>
        {i18n.common.confirm_forgot_password_description}
        <span className={styles.email}> {email}</span>
      </p>
    </div>
  );
};

export default ConfirmForgotPassword;
