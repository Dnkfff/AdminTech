import { useModal } from "@package/components";
import ConfirmRegistrationModal from "./ConfirmRegistrationModal";

export const useConfirmRegistrationModal = () =>
  useModal(ConfirmRegistrationModal);
