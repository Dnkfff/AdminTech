import React, { FC } from "react";
import { Button } from "@package/components";
import { useI18n } from "../../../../../hooks";
import styles from "./styles.module.scss";

interface IConfirmResetPassword {
  onClose: () => void;
}

const ConfirmResetPassword: FC<IConfirmResetPassword> = ({ onClose }) => {
  const i18n = useI18n();
  return (
    <div className={styles.frame}>
      <p className={styles.text}>
        {i18n.common.reset_password_description}
      </p>
      <Button
        border={Button.borders.HavelockBlue}
        color={Button.colors.HavelockBlue}
        className={styles.button}
        onClick={onClose}
      >
        {i18n.common.close}
      </Button>
    </div>
  );
};

export default ConfirmResetPassword;
