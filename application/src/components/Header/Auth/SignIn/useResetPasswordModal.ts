import { useI18n } from "../../../../hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "./ResetPasswordModal";

export const useResetPasswordModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.common.reset_password },
    modalComponent: ModalComponent.Headered,
  });
};
