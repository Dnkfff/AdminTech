import { useI18n } from "../../../../hooks";
import { useModal, Modal as ModalComponent } from "@package/components";
import Modal from "./SignUpModal";

export const useSignUpModal = () => {
  const i18n = useI18n();

  return useModal(Modal, {
    modalProps: { title: i18n.common.sign_up },
    modalComponent: ModalComponent.Headered,
  });
};
