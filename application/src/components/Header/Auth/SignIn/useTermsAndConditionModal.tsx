import { useModal } from "@package/components";
import Modal from "./TermsAndConditionsModal";

export const useTermsAndConditionsModal = () => {
  return useModal(Modal);
};
