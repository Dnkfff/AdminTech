import React, { FC, useCallback } from "react";
import { useI18n } from "src/hooks";
import { Button } from "@package/components";
import { useSignUpModal } from "./useSignUpModal";
import { useConfirmRegistrationModal } from "./useConfirmRegistrationModal";
import { useSignInModal } from "./useSignInModal";
import { useForgotPasswordModal } from "./useForgotPasswordModal";
import { useConfirmForgotPasswordModal } from "./useConfirmForgotPasswordModal";
import { useResetPasswordModal } from "./useResetPasswordModal";
import { useConfirmResetPasswordModal } from "./useConfirmResetPasswordModal";

import styles from "./styles.module.scss";
import { useTermsAndConditionsModal } from "./useTermsAndConditionModal";

const SignUp: FC = () => {
  const i18n = useI18n();

  const [fragmentSU, onOpenSignUp, onCloseSignUp] = useSignUpModal();
  const [fragmentSI, onOpenSignIn, onCloseSignIn] = useSignInModal();
  const [fragmentTAC, onOpenTermsAndConditions] = useTermsAndConditionsModal();
  const [fragmentCR, onOpenConfirmRegistration, onCloseConfirmRegistration] =
    useConfirmRegistrationModal();
  const [fragmentFP, onOpenForgotPassword, onCloseForgotPassword] =
    useForgotPasswordModal();
  const [fragmentCFP, onOpenConfirmForgotPassword] =
    useConfirmForgotPasswordModal();
  const [fragmentRP, onOpenResetPassword, onCloseResetPassword] =
    useResetPasswordModal();
  const [fragmentCRP, onOpenConfirmResetPassword, onCloseConfirmResetPassword] =
    useConfirmResetPasswordModal();

  const openConfirmRegistrationHandler = useCallback(
    (email: string) => {
      onCloseSignUp();
      onOpenConfirmRegistration({ email, onClose: onCloseConfirmRegistration });
    },
    [onCloseSignUp, onOpenConfirmRegistration, onCloseConfirmRegistration]
  );

  const openTermsAndConditionsHandler = useCallback(() => {
    onCloseSignUp();
    onOpenTermsAndConditions({});
  }, [onOpenTermsAndConditions, onCloseSignUp]);

  const openSignUpHandler = useCallback(() => {
    onCloseSignIn();
    onOpenSignUp({
      onClose: onCloseSignUp,
      onSaved: openConfirmRegistrationHandler,
      onOpenTermsAndConditions: openTermsAndConditionsHandler,
    });
  }, [
    onOpenSignUp,
    onCloseSignUp,
    openConfirmRegistrationHandler,
    onCloseSignIn,
    openTermsAndConditionsHandler,
  ]);

  const openConfirmForgotPasswordHandler = useCallback(
    (email: string) => {
      onCloseForgotPassword();
      onOpenConfirmForgotPassword({ email });
    },
    [onCloseForgotPassword, onOpenConfirmForgotPassword]
  );

  const openForgotPasswordHandler = useCallback(() => {
    onCloseSignIn();
    onOpenForgotPassword({
      onClose: onCloseForgotPassword,
      onSend: openConfirmForgotPasswordHandler,
    });
    // onOpenResetPassword({
    //   onClose: onCloseResetPassword,
    //   onSaved: openConfirmResetPasswordHandler,
    // });
  }, [
    onCloseSignIn,
    onOpenForgotPassword,
    onCloseForgotPassword,
    openConfirmForgotPasswordHandler,
  ]);

  const openSignInHandler = useCallback(() => {
    onOpenSignIn({
      onClose: onCloseSignIn,
      onContinue: onCloseSignIn,
      onOpenSignUp: openSignUpHandler,
      onOpenForgotPassword: openForgotPasswordHandler,
    });
  }, [
    onOpenSignIn,
    openSignUpHandler,
    onCloseSignIn,
    openForgotPasswordHandler,
  ]);

  const closeConfirmResetPasswordHandler = useCallback(() => {
    onCloseConfirmResetPassword();
    openSignInHandler();
  }, [onCloseConfirmResetPassword, openSignInHandler]);

  const openConfirmResetPasswordHandler = useCallback(() => {
    onCloseResetPassword();
    onOpenConfirmResetPassword({ onClose: closeConfirmResetPasswordHandler });
  }, [
    onCloseResetPassword,
    onOpenConfirmResetPassword,
    closeConfirmResetPasswordHandler,
  ]);

  return (
    <>
      {fragmentSU}
      {fragmentSI}
      {fragmentCR}
      {fragmentTAC}
      {fragmentFP}
      {fragmentCFP}
      {fragmentRP}
      {fragmentCRP}
      <Button
        color={Button.colors.White}
        border={Button.borders.Coral}
        fill={Button.fills.Coral}
        onClick={openSignInHandler}
        className={styles.sign_in}
      >
        {i18n.common.sign_in}
      </Button>
    </>
  );
};

export default SignUp;
