import { useModal } from "@package/components";
import ConfirmForgotPasswordModal from "./ConfirmForgotPasswordModal";

export const useConfirmForgotPasswordModal = () =>
  useModal(ConfirmForgotPasswordModal);
