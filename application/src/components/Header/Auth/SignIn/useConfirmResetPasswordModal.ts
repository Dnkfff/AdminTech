import { useModal } from "@package/components";
import ConfirmResetPasswordModal from "./ConfirmResetPasswordModal";

export const useConfirmResetPasswordModal = () =>
  useModal(ConfirmResetPasswordModal);
