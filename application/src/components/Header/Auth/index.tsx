import React, { FC } from "react";
import { Link, generatePath } from "react-router-dom";
import SignIn from "./SignIn";
import { authorization } from "src/store";
import { useSelector } from "react-redux";
import profile from "src/modules/Profile";
import settings from "src/modules/Profile/Settings";

import styles from "./styles.module.scss";

const generatedPath = generatePath(profile.path, { "*": settings.path });

const AuthorizedCircle: FC<{ name: string; surname: string }> = ({
  surname,
  name,
}) => {
  return (
    <Link to={generatedPath} className={styles.authorized}>
      {name?.[0]?.toUpperCase() || ""}.{surname?.[0]?.toUpperCase() || ""}
    </Link>
  );
};

const Auth: FC = () => {
  const isAuthorized = useSelector(authorization.selectors.getIsAuthorized);
  const authorizedUser = useSelector(authorization.selectors.getUser);

  return (
    <>
      {isAuthorized ? (
        <AuthorizedCircle
          name={authorizedUser.name}
          surname={authorizedUser.surname}
        />
      ) : (
        <SignIn />
      )}
    </>
  );
};

export default Auth;
