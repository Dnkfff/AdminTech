import React from "react";
import styles from "./index.module.scss";
import { Button, Icon } from "@package/components";
import { useNavigate } from "react-router-dom";
import Badge from "../Badge";

const Basket = () => {
  const navigate = useNavigate();
  // TODO: SET valid home profile handler
  const onClickBasket = () => navigate("/home");

  return (
    <Badge count={2}>
      <Button.Icon
        view={Icon.views.BASKET}
        onClick={onClickBasket}
        className={styles.icon}
      />
    </Badge>
  );
};

export default Basket;
