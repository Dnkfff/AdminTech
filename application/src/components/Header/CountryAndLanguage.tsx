import React, { useEffect } from "react";
import styles from "./CountryAndLanguage.module.scss";
import { ITranslationKey, ITranslations } from "../../i18n";
import { useSelector, useStore } from "react-redux";
import { settings } from "src/store";
import { useCountryLanguageResource, resourceSelectors } from "../../hooks";
import * as service from "src/service";
import { ICountryLanguageResource } from "../../hooks/useResource/useCountryLanguageResource";
import { Icon } from "@package/components";

const currentLanguage = window.navigator.language.toUpperCase();

const CountryAndLanguage = () => {
  const { getState, dispatch } = useStore();
  const countryId = useSelector(settings.selectors.getCountryId);
  const languageId = useSelector(settings.selectors.getLanguage);

  const { entities, onLoad, store } = useCountryLanguageResource();

  useEffect(() => {
    const state = getState();

    const isLoaded = resourceSelectors.getIsLoaded(state, store.name);

    const f = async () => {
      const loaderCountryLanguage = onLoad();

      const { countryCode: currentCodeCountry } =
        await service.foreign_services.getLocationInfo();

      await loaderCountryLanguage;

      const entries: ICountryLanguageResource[] = resourceSelectors.getEntries(
        getState(),
        store.name
      );

      const currentCountry = entries.find(
        ({ codeCountry }) => codeCountry === currentCodeCountry
      );

      const currentCountryLanguages = currentCountry?.childrenEntities?.find(
        ({ short }) => short === currentLanguage
      );

      const pickedCodeCountry =
        currentCountry?.countryId ?? entries?.[0]?.countryId;
      const pickedLanguage = (currentCountryLanguages?.short ??
        entries[0].childrenEntities[0].short) as keyof ITranslations;

      dispatch(settings.actions.setCountryId(Number(pickedCodeCountry)));
      dispatch(settings.actions.setLanguage(pickedLanguage));
    };
    !isLoaded && f();
  }, [dispatch]);

  const countriesOption = entities.find(
    ({ countryId: cid }) => cid === countryId
  );
  const countriesOptions = entities.filter(
    ({ countryId: cid }) => cid !== countryId
  );

  const languagesOption = (countriesOption?.childrenEntities || []).find(
    ({ short }) => String(short) === String(languageId)
  );

  const languagesOptions = (countriesOption?.childrenEntities || []).filter(
    ({ short }) => String(short) !== String(languageId)
  );

  const onSelectCountry = (event: unknown) => {
    const { value } = (event as { target: { value: string } }).target;
    const numberValue = Number(value);
    dispatch(settings.actions.setCountryId(numberValue));

    const country = entities.find(
      ({ countryId }) => String(countryId) === String(value)
    );

    dispatch(
      settings.actions.setLanguage(
        (country?.children?.includes(currentLanguage)
          ? currentLanguage
          : country?.children?.[0]) as keyof ITranslations
      )
    );
  };

  const onSelectLanguage = (event: unknown) => {
    const { value } = (event as { target: { value: ITranslationKey } }).target;
    dispatch(settings.actions.setLanguage(value));
  };

  return (
    <>
      <div className={styles.countries}>
        <button className={styles.selected} type="button">
          <Icon view={Icon.views.GLOBE} />
          <p className={styles.text}>{countriesOption?.countryName.EN}</p>
        </button>
        <div className={styles.options}>
          {countriesOptions.map(({ countryName, countryId }) => (
            <button
              className={styles.option}
              key={countryId}
              value={countryId}
              onClick={onSelectCountry}
            >
              {countryName.EN}
            </button>
          ))}
        </div>
      </div>

      <div
        className={styles.languages}
        aria-disabled={languagesOptions.length === 0}
      >
        <button className={styles.selected} type="button">
          <p className={styles.text}>{languagesOption?.short}</p>
        </button>
        <div className={styles.options}>
          {languagesOptions.map(({ short }) => (
            <button
              className={styles.option}
              key={short}
              value={short}
              onClick={onSelectLanguage}
            >
              {short}
            </button>
          ))}
        </div>
      </div>
    </>
  );
};

export default CountryAndLanguage;
