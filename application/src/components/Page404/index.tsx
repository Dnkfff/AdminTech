import styles from "./Page404.module.scss";
import React, { FC } from "react";
import { always, Button, Icon } from "@package/components";
import { Link } from "react-router-dom";
import { Home } from "../../modules";
import { useI18n } from "../../hooks";

const Page404: FC = () => {
  const i18n = useI18n();
  return (
    <div className={styles.box}>
      <Icon view={Icon.views.PAGE_404} />
      <div className={styles.page}>{i18n.page404.page_not_found}</div>
      <Link to={Home.path} className={styles.link}>
        <Button onClick={always.EMPTY_FUNC} className={styles.btn}>
          {i18n.page404.home_page}
        </Button>
      </Link>
    </div>
  );
};

export default Page404;
