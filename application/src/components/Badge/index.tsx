import React, { FC } from "react";
import styles from "./styles.module.scss";

type IBadge = React.PropsWithChildren<{ count: number }>;

const Badge: FC<IBadge> = ({ children, count }) => {
  return (
    <div className={styles.badge}>
      <span className={styles.count}>{count}</span>
      {children}
    </div>
  );
};

export default Badge;
