import React, { FC } from "react";
import { Icon } from "@package/components";
import cx from "classnames";
import styles from "./style.module.scss";
import { useI18n } from "../../hooks";
import { Link } from "react-router-dom";
import termsAndConditionsConfig from "src/modules/TermsAndConditions";

const LinkIcon: FC<{
  href: HTMLAnchorElement["href"];
  view: typeof Icon.views[keyof typeof Icon.views];
}> = ({ view, href }) => (
  <a className={styles.text} href={href}>
    <Icon view={view} />
  </a>
);

const Footer: FC = () => {
  const i18n = useI18n();

  return (
    <section className={styles.container}>
      <div className={styles.block}>
        <div className={styles.column}>
          <Icon view={Icon.views.LOGO} />
          <span className={styles.text}>
            {i18n.application.applicationLicence}
          </span>
        </div>

        <div className={styles.column}>
          <div className={styles.social_container}>
            <LinkIcon view={Icon.views.IN} href="/" />
            <LinkIcon view={Icon.views.TWITTER} href="/" />
          </div>

          <a className={cx(styles.text, styles.blue)} href="/">
            {i18n.application.aboutUs}
          </a>
          <Link
            to={termsAndConditionsConfig.path}
            className={cx(styles.text, styles.blue)}
          >
            {i18n.application.termsAndConditions}
          </Link>
        </div>

        <div className={styles.column}>
          <address className={styles.text}>
            {i18n.application.addressStreet}
            <br />
            {i18n.application.addressPostcode}
          </address>

          <a
            className={cx(styles.text, styles.blue)}
            href="mailto:info@AdminTech.com"
          >
            info@AdminTech.com
          </a>
        </div>
      </div>
    </section>
  );
};

export default Footer;
