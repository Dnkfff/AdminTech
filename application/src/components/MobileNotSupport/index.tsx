import { FC } from "react";
import styles from "./MobilePage.module.scss";
import { Icon } from "@package/components";

const MobilePage: FC = () => {
  return (
    <>
      <div className={styles.box}>
        <div className={styles.logo}>
          <Icon view={Icon.views.MOBILE_LOGO} />
        </div>
        <div className={styles.container}>
          <span className={styles.text}>
            The mobile version is not yet available
          </span>
          <span className={styles.soon}>COMING SOON</span>
        </div>
      </div>
    </>
  );
};
export default MobilePage;
