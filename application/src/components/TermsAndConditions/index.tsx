import React, { FC } from "react";

import styles from "./TermsAndConditions.module.scss";

const TermsAndConditions: FC = () => {
  return (
    <div className={styles.body}>
      <section className={styles.container}>
        <h1 className={styles.title}>Terms &amp; conditions</h1>

        <article>
          <h2 className={styles.subtitle}>
            AdminTech – General Terms and Conditions
          </h2>
          <p className={styles.content}>
            AdminTech is an online B2B service for generation and administration
            of business documents and official forms for Swiss fiduciaries
            consulting Swiss companies and individuals (“Services”).
          </p>
          <p className={styles.content}>
            The Services are provided online by automated algorithms and are
            available through our website (“Site”) or applications (“Apps”).
          </p>
          <p className={styles.content}>
            The Services are operated by AdminTech Sàrl, Chemin Jean-Baptiste
            Vandelle 3A, 1290 Versoix, Switzerland.
          </p>
        </article>
        <ol className={styles.list}>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>
              Agreement to Terms and Conditions
            </h2>
            <p className={styles.content}>
              By accepting the Terms &amp; Conditions on our Site or Apps, and
              once we have activated your User Profile, you (“User” or “you”)
              enter into a binding legal contract with AdminTech Sàrl
              (“Provider” or “we”) under the following conditions for the
              provision of AdminTech Services.
            </p>
            <p className={styles.content}>
              You agree that by accessing the Site or Apps of the Provider, you
              have read, understood, and agree to be bound by all of these Terms
              and Conditions. If you do not agree with all of these Terms and
              Conditions, then you are expressly prohibited from using the
              Services and you must discontinue use immediately. Supplemental
              terms and conditions or documents that may be posted on our
              website or applications from time to time are hereby expressly
              incorporated herein by reference. We reserve the right, in our
              sole discretion, to make changes or modifications to these Terms
              and Conditions at any time and for any reason. We will alert you
              about any changes by updating the last updated date of these Terms
              and Conditions, and you waive any right to receive specific notice
              of each such change.{" "}
            </p>
            <p className={styles.content}>
              It is your responsibility to periodically review these Terms and
              Conditions to stay informed of updates. You will be subject to,
              and will be deemed to have been made aware of and to have
              accepted, the changes in any revised Terms and Conditions by your
              continued use of the Site after the date such revised Terms and
              Conditions are posted.
            </p>
            <p className={styles.content}>
              The present Terms and Conditions remain valid throughout the use
              of the Services by the User, and remain binding after the
              termination of the User Profile for the maximum statutory
              limitation period permitted by Swiss law.{" "}
            </p>
            <p className={styles.content}>
              The Provider may transfer this agreement and the Services to any
              third party without prior notice to the User.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>
              Registration and access to Services
            </h2>
            <p className={styles.content}>
              The access to Services is subject to a registration of the User.
              The User shall be required to provide correct identification
              information for the registration. We reserve the right to remove,
              reclaim, or change a username you select if we determine, in our
              sole discretion, that such username is inappropriate, obscene,
              belongs to a different entity that you unjustly impersonate or
              otherwise objectionable.
            </p>
            <p className={styles.content}>
              We reserve the right to request additional authentication upon
              registration or later on in order to ensure the confidentiality of
              data and prevent the unauthorized access to the User Profile.
            </p>
            <p className={styles.content}>
              Except for limited free trials and promotions as indicated on our
              Site or Apps or any other correspondence with the User, the access
              to Services, as well as User Content or Generated Content, or
              parts thereof, is subject to a fixed or periodic fee as indicated
              in our Site or Apps (“Subscription Fee”). The Subscription Fee is
              due in advance prior to the access to the Services or parts
              thereof, and is payable in the currency and according to the terms
              indicated in our Site or Apps.
            </p>
            <p className={styles.content}>
              The User must be a company registered in Switzerland or having its
              registered branch in Switzerland, as well as to any individual
              residing in Switzerland. The information and Services provided on
              our Site or Apps is not intended for distribution to or use by any
              person or entity in any jurisdiction or country where such
              distribution or use would be contrary to law or regulation or
              which would subject us to any registration requirement within such
              jurisdiction or country. Accordingly, those persons who choose to
              access the Services from other locations do so on their own
              initiative and are solely responsible for compliance with local
              laws, if and to the extent local laws are applicable.
            </p>
            <p className={styles.content}>
              If you access the Site or Apps from the European Union, Asia, or
              any other region of the world with laws or other requirements
              governing personal data collection, use, or disclosure that differ
              from applicable laws in the Switzerland, then through your
              continued use of the Services, you are transferring your data to
              the Switzerland, and you expressly consent to have your data
              transferred to and processed in Switzerland.
            </p>
            <p className={styles.content}>
              The User may grant the access to his User Profile to his employees
              or sub-contractors, and is fully and personally liable for the use
              of the Services by them. Such persons shall likewise be referred
              to as the “User”. The User may not grant access to his User
              Profile to his clients without prior authorization from the
              Provider.
            </p>
            <p className={styles.content}>
              Any person accessing the User Profile must be at least 18 years
              old. By agreeing to the present Terms and Conditions the User
              hereby warrants that he is at least 18 years old.
            </p>
            <p className={styles.content}>
              The Provider may suspend the access to Services without prior
              notice in case of breach (or suspicion of breach) of the present
              Terms and Conditions or a violation of any law or judgment, as
              well as in case of any administrative or judicial requirement to
              suspend such access. The Provided may likewise suspend the access
              to Services for any other reason related to confidentiality,
              security, reputation of the Provider, etc.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>User Profile and Password</h2>
            <p className={styles.content}>
              The Provider is not responsible for monitoring of any suspicious
              activity or suspicion of data breach on the User Profile. It is
              the sole responsibility of the User to ensure the security of
              log-in credentials (“Credentials”). The User shall abstain from
              communicating such Credentials to any unauthorized person. The
              User shall ensure the regular modification of his Password and
              shall signal any suspicious activity discovered in relation to his
              User Profile.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>User Content</h2>
            <p className={styles.content}>
              The User explicitly represents and warrants that he has the
              requisite capacity and rights to any information or document
              uploaded or otherwise added to our Site or Apps by the User (“User
              Content”).
            </p>
            <p className={styles.content}>
              The User is solely liable for any inaccuracies in the provided
              User Content and it is the User’s responsibility to ensure the
              exactness of the provided User Content.
            </p>
            <p className={styles.content}>
              The Provider is not responsible for the storing of the User
              Content. For any stored User Content the Provider shall ensure its
              confidentiality. The User Content remains the exclusive property
              of the User, but may be used by the Provider for statistical
              big-data anonymized analysis.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>Generated Content</h2>
            <p className={styles.content}>
              The Services produce documents or official forms on the basis of
              the User Content (“Generated Content”). The Generated Content is
              the exclusive property of the User, but may be used by the
              Provider for statistical big-data anonymized analysis.
            </p>
            <p className={styles.content}>
              Once the Generated Content is downloaded by the User, the Provider
              is not responsible for the storing of the Generated Content. It is
              the User’s sole responsibility to carefully review the Generated
              Content before any use, and the Provider bears no liability for
              the incorrectness of the Generated Content or its non-compliance
              with the applicable legal or contractual requirements.
            </p>
            <p className={styles.content}>
              In case of breach of the present Terms and Conditions, the
              Provider may suspend the access of the User to the Generated
              Content or delete it without prior notice.{" "}
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>Protected Content</h2>
            <p className={styles.content}>
              Unless otherwise indicated, the Services are our proprietary
              property and all source code, databases, functionality, software,
              website designs, audio, video, text, photographs, and graphics on
              our websites or applications and the trademarks, service marks,
              and logos contained therein (collectively, the “Protected
              Content”) are owned or controlled by us or licensed to us, and are
              protected by copyright and trademark laws and various other
              intellectual property rights and unfair competition laws of
              Switzerland, foreign jurisdictions, and international conventions.{" "}
            </p>
            <p className={styles.content}>
              The Protected Content is provided on the Site or Apps “as is” for
              your information and personal use only. Except as expressly
              provided in these Terms and Conditions, no part of our Site or
              Apps and no Protected Content may be copied, reproduced,
              aggregated, republished, uploaded, posted, publicly displayed,
              encoded, translated, transmitted, distributed, sold, licensed, or
              otherwise exploited for any commercial purpose whatsoever, without
              our express prior written permission.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>Prohibited activity</h2>
            <p className={styles.content}>
              You may not access or use the Services for any purpose other than
              that for which we make the Services available. The Services may
              not be used in connection with any commercial endeavours except
              those that are specifically endorsed or approved by us.
            </p>
            <p className={styles.content}>
              <strong className={styles.strong}>
                As a user of the Services, you agree not to:
              </strong>
            </p>
            <ol className={styles.sublist}>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  systematically retrieve data or other content from the
                  Services to create or compile, directly or indirectly, a
                  collection, compilation, database, or directory without
                  written permission from us.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  make any unauthorized use of the Services, including
                  collecting usernames and/or email addresses of users by
                  electronic or other means for the purpose of sending
                  unsolicited email, or creating user accounts by automated
                  means or under false pretences.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  use the Services to advertise or offer to sell goods and
                  services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  circumvent, disable, or otherwise interfere with
                  security-related features of the Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  engage in unauthorized framing of or linking to the Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  trick, defraud, or mislead us and other users, especially in
                  any attempt to learn sensitive account information such as
                  user passwords;
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  make improper use of our support services or submit false
                  reports of abuse or misconduct.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  engage in any automated use of the system, such as using
                  scripts to send comments or messages, or using any data
                  mining, robots, or similar data gathering and extraction
                  tools.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  interfere with, disrupt, or create an undue burden on the
                  Services or the networks or services connected to the
                  Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  attempt to impersonate another user or person or use the
                  username of another user.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  sell or otherwise transfer your profile.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  use any information obtained from the Services in order to
                  harass, abuse, or harm another person.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  use the Services as part of any effort to compete with us or
                  otherwise use the Services and/or the Content for any
                  revenue-generating endeavour or commercial enterprise.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  decipher, decompile, disassemble, or reverse engineer any of
                  the software comprising or in any way making up a part of the
                  Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  attempt to bypass any measures of the Services designed to
                  prevent or restrict access to the Services, or any portion of
                  the Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  harass, annoy, intimidate, or threaten any of our employees or
                  agents engaged in providing any portion of the Services to
                  you.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  delete the copyright or other proprietary rights notice from
                  any Content.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  copy or adapt the Services’ software, including but not
                  limited to Flash, PHP, HTML, JavaScript, or other code.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  upload or transmit (or attempt to upload or to transmit)
                  viruses, Trojan horses, or other material, including excessive
                  use of capital letters and spamming (continuous posting of
                  repetitive text), that interferes with any party’s
                  uninterrupted use and enjoyment of the Services or modifies,
                  impairs, disrupts, alters, or interferes with the use,
                  features, functions, operation, or maintenance of the
                  Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  upload or transmit (or attempt to upload or to transmit) any
                  material that acts as a passive or active information
                  collection or transmission mechanism, including without
                  limitation, clear graphics interchange formats (“gifs”), 1×1
                  pixels, web bugs, cookies, or other similar devices (sometimes
                  referred to as “spyware” or “passive collection mechanisms” or
                  “pcms”).
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  except as may be the result of standard search engine or
                  Internet browser usage, use, launch, develop, or distribute
                  any automated system, including without limitation, any
                  spider, robot, cheat utility, scraper, or offline reader that
                  accesses the Services, or using or launching any unauthorized
                  script or other software.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  disparage, tarnish, or otherwise harm, in our opinion, us
                  and/or the Services.
                </p>
              </li>
              <li className={styles.subitem}>
                <p className={styles.content}>
                  use the Services in a manner inconsistent with any applicable
                  laws or regulations.
                </p>
              </li>
            </ol>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>
              Governing law and dispute resolution
            </h2>
            <p className={styles.content}>
              These Terms and Conditions and your use of the Site are governed
              by and construed in accordance with the laws of Switzerland. Any
              legal action in relation to the Services, the present Terms and
              Conditions, Site, Apps, User Content, Generated Content or
              Protected Content shall be brought exclusively before the courts
              of Geneva, Switzerland, with the exception of appeal to the Swiss
              Federal Court.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>Disclaimer</h2>
            <p className={styles.content}>
              The Site and Apps are provided on an “as-is” and “as-available”
              basis. You agree that your use of the our Services will be at your
              sole risk. To the fullest extent permitted by law, we disclaim all
              warranties, express or implied, in connection with the site and
              your use thereof, including, without limitation, the implied
              warranties of merchantability, fitness for a particular purpose,
              and non-infringement.{" "}
            </p>
            <p className={styles.content}>
              In no event will we or our directors, employees or agents be
              liable to you or any third party for any direct, indirect,
              consequential, exemplary, incidental, special, or punitive
              damages, including lost profit, lost revenue, loss of data, or
              other damages arising from your use of the site, even if we have
              been advised of the possibility of such damages.
            </p>
            <p className={styles.content}>
              You agree that we shall have no liability to you for any loss or
              corruption of any such Generated Content or User Content, and you
              hereby waive any right of action against us arising from any such
              loss or corruption of such data.
            </p>
          </li>
          <li className={styles.item}>
            <h2 className={styles.subtitle}>Miscellaneous</h2>
            <p className={styles.content}>
              These Terms and Conditions and any policies or operating rules
              posted by us on the Site or Apps constitute the entire agreement
              and understanding between you and us. Our failure to exercise or
              enforce any right or provision of these Terms and Conditions shall
              not operate as a waiver of such right or provision.{" "}
            </p>
            <p className={styles.content}>
              These Terms and Conditions operate to the fullest extent
              permissible by law. We may assign any or all of our rights and
              obligations to others at any time. We shall not be responsible or
              liable for any loss, damage, delay, or failure to act caused by
              any cause beyond our reasonable control.{" "}
            </p>
            <p className={styles.content}>
              If any provision or part of a provision of these Terms and
              Conditions is determined to be unlawful, void, or unenforceable,
              that provision or part of the provision is deemed severable from
              these Terms and Conditions and does not affect the validity and
              enforceability of any remaining provisions.{" "}
            </p>
            <p className={styles.content}>
              There is no joint venture, partnership, employment or agency
              relationship created between you and us as a result of these Terms
              and Conditions or use of the Services. You agree that these Terms
              and Conditions will not be construed against us by virtue of
              having drafted them.
            </p>
            <p className={styles.content}>
              You hereby waive any and all defences you may have based on the
              electronic form of these Terms and Conditions and the lack of
              signing by the parties hereto to execute these Terms and
              Conditions.
            </p>
          </li>
        </ol>
      </section>
    </div>
  );
};

export default TermsAndConditions;
