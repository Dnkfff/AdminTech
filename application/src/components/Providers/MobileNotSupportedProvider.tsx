import React from "react";
import MobileNotSupport from "../MobileNotSupport";

type IMobileNotSupportedProvider = { children: React.ReactNode };

const isPhoneRegEx =
  /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i;

const MobileNotSupportedProvider: React.FC<IMobileNotSupportedProvider> = ({
  children,
}) => {
  if (isPhoneRegEx.test(navigator.userAgent)) {
    return <MobileNotSupport />;
  }
  return <>{children}</>;
};

export default MobileNotSupportedProvider;
