import React, { FC, useCallback, useEffect } from "react";
import { ls } from "src/utils";
import * as service from "src/service";
import { useDispatch, batch, useSelector } from "react-redux";
import { authorization } from "src/store";
import Page404 from "src/components/Page404";

interface IAuthProvider {
  children: React.ReactNode;
}

const AuthProvider: FC<IAuthProvider> = ({ children }) => {
  const dispatch = useDispatch();

  const onConfirmToken = useCallback(
    async (token: string) => {
      const { response, data } = await service.auth.check_token(token);
      if (response.ok) {
        return batch(() => {
          dispatch(
            authorization.actions.setIsAuthorized(data.message.validation)
          );
          dispatch(authorization.actions.setUser(data.message.user));
        });
      }
      batch(() => {
        dispatch(authorization.actions.setIsAuthorized(false));
        dispatch(authorization.actions.setUser({}));
      });
      ls.token.remove();
    },
    [dispatch]
  );

  useEffect(() => {
    const token = ls.token.get();
    if (token) {
      onConfirmToken(token);
    }

    return ls.token.listen((token) => {
      if (token) {
        return onConfirmToken(token);
      }
      batch(() => {
        dispatch(authorization.actions.setIsAuthorized(false));
        dispatch(authorization.actions.setUser({}));
      });
      ls.token.remove();
    });
  }, []);

  return <>{children}</>;
};

export const WithAuth = (Component: FC) => {
  const WrappedComponent = (props: React.ComponentProps<typeof Component>) => {
    const isAuthorized = useSelector(authorization.selectors.getIsAuthorized);

    return isAuthorized ? <Component {...props} /> : <Page404 />;
  };
  WrappedComponent.displayName = `withAuth`;

  return WrappedComponent;
};

export default AuthProvider;
