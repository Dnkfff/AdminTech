import React from "react";
import ReactDOM from "react-dom/client";
import Application from "./Application";
import { Provider as Redux } from "react-redux";
import { Provider as I18n } from "./i18n";
import { SpriteProvider } from "@package/components";
import CallbackProvider from "./modules/Callbacks";
import { AuthProvider, MobileNotSupportedProvider } from "src/components";

import store from "./store";

import "./styles/global.scss";

const rootElement = document.getElementById("root") as Element;

const root = ReactDOM.createRoot(rootElement);

root.render(
  // <React.StrictMode>
  <SpriteProvider>
    <MobileNotSupportedProvider>
      <Redux store={store}>
        <I18n>
          <CallbackProvider>
            <AuthProvider>
              <Application />
            </AuthProvider>
          </CallbackProvider>
        </I18n>
      </Redux>
    </MobileNotSupportedProvider>
  </SpriteProvider>
  // </React.StrictMode>
);
