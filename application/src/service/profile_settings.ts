import { put, remove } from "./api";

export const change_password = (body: Record<string, string>) =>
  put({ url: "/changePassword", body });

export const delete_account = () =>
  Promise.resolve({
    response: new Response(),
    data: {
      message: {
        error: false,
        message: {
          deleted: true,
        },
      },
      error: false,
    },
  });
