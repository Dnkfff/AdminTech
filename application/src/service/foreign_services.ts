export const getLocationInfo = async () =>
  fetch("http://ip-api.com/json")
    .then((response) => response.json())
    .catch(() => ({ countryCode: null }));
