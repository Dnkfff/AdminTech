import { post } from "./api";
import { ls } from "src/utils";

const insert_token_ls = (data: any) => {
  if (data.response.ok && data.data.message.token) {
    ls.token.set(data.data.message.token);
  }
  return data;
};

export const login = (body: Record<string, any>) =>
  post({ url: "/login", body }).then(insert_token_ls);

export const login_google = (token: string) =>
  post({ url: "/login/google", body: { token } }).then(insert_token_ls);

export const registration = (body: Record<string, any>) =>
  post({ url: "/registration", body });

export const registration_google = (token: string) =>
  post({ url: "/registration/google", body: { token } }).then(insert_token_ls);

export const password_recovery = (body: Record<string, any>) =>
  post({ url: "/recovery", body });

export const check_token = (token: string) =>
  post({ url: "/checkToken", body: { token } });
