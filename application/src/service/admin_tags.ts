import { get, remove, post } from "./api";

export const getTags = (countryId: string, query: string) =>
  get({ url: "/tags/all/" + countryId + "?" + query });

export const createTag = (countryId: string, body: any) =>
  post({ url: "/tags/add/" + countryId, body });

export const removeTag = (id: string) => remove({ url: "/tags/remove/" + id });
