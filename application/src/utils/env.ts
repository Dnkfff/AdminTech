export const SERVICE_URL = process.env.REACT_APP_SERVICE;
export const GOOGLE_CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID;
export const LINKED_IN_CLIENT_ID = process.env.REACT_APP_LINKED_IN_CLIENT_ID;
