import adminPanelConstructorList from "../../modules/AdminPanelConstructorList/i18n/en.json";
import adminCountryAndLanguage from "../../modules/AdminCountryAndLanguage/i18n/en.json";
import adminDashboard from "../../modules/AdminDashboard/i18n/en.json";
import adminPanel from "../../modules/AdminPanel/i18n/en.json";
import home from "../../modules/Home/i18n/en.json";
import profile from "../../modules/Profile/i18n/en.json";
import page404 from "../../components/Page404/i18n/en.json";

const common = {
  ok: "Ok",
  save: "Save",
  continue: "Continue",
  cancel: "Cancel",
  delete: "Delete",
  back: "Back",
  edit: "Edit",
  select: "Select",
  submit: "Submit",
  sign_in: "Sign in",
  sign_up: "Sign up",
  name: "Name",
  surname: "Surname",
  email: "Email",
  password: "Password",
  repeat_password: "Repeat password",
  accept: "Accept",
  terms_and_conditions: "terms & conditions",
  or_sign_up_with: "or Sign up with",
  or: "or",
  new_in_application: "New to AdminTech?",
  sign_up_now: "Sign up now",
  forgot_password: "Forgot password",
  forgot_your_password: "Forgot your password?",
  send: "Send",
  forgot_password_description:
    "Please enter the email address used to register.",
  confirm_forgot_password_description:
    "Password recovery email has been sent to",
  reset_password: "Reset password",
  repeat_new_password: "Repeat new password",
  new_password: "New password",
  close: "Close",
  reset_password_description: "Your password has been successfully restored!",
  google: "Google",
  linked_in: "LinkedIn",
  change: "Change",
};

const application = {
  applicationLicence: "© 2021 AdminTech Sàrl. All rights reserved",
  aboutUs: "About us",
  termsAndConditions: "Terms and conditions",
  addressStreet: "ch. Jean‐Baptiste Vandelle 3A",
  addressPostcode: "1290 Versoix",
};

const translations = {
  adminPanel,
  adminDashboard,
  adminCountryAndLanguage,
  adminPanelConstructorList,
  common,
  application,
  home,
  profile,
  page404,
};

export type ITranslations = typeof translations;
export default translations;
